/******************************************************************
*
*    CyberUPnP for Java
*
*    Copyright (C) Satoshi Konno 2002
*
*    File: ActionListener.java
*
*    Revision;
*
*    01/16/03
*        - first revision.
*
******************************************************************/

package com.fiberhome.remoteime.cybergarage.unnp.control;

import com.fiberhome.remoteime.cybergarage.unnp.Action;

public interface ActionListener
{
    public boolean actionControlReceived(Action action);
}
