/******************************************************************
*
*    CyberUPnP for Java
*
*    Copyright (C) Satoshi Konno 2002
*
*    File: SearchListener.java
*
*    Revision;
*
*    11/18/02b
*        - first revision.
*
******************************************************************/

package com.fiberhome.remoteime.cybergarage.unnp.device;

import com.fiberhome.remoteime.cybergarage.unnp.ssdp.SSDPPacket;

public interface SearchListener
{
    public void deviceSearchReceived(SSDPPacket ssdpPacket);
}
