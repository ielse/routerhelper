/******************************************************************
*
*    CyberUPnP for Java
*
*    Copyright (C) Satoshi Konno 2002
*
*    File: DeviceNotifyListener.java
*
*    Revision;
*
*    11/18/02
*        - first revision.
*
******************************************************************/

package com.fiberhome.remoteime.cybergarage.unnp.device;

import com.fiberhome.remoteime.cybergarage.unnp.ssdp.SSDPPacket;

public interface NotifyListener
{
    public void deviceNotifyReceived(SSDPPacket ssdpPacket);
}
