/******************************************************************
*
*    CyberUPnP for Java
*
*    Copyright (C) Satoshi Konno 2002
*
*    File: SSDPSearchResponse.java
*
*    Revision;
*
*    01/14/03
*        - first revision.
*
******************************************************************/

package com.fiberhome.remoteime.cybergarage.unnp.ssdp;

import com.fiberhome.remoteime.cybergarage.http.HTTP;
import com.fiberhome.remoteime.cybergarage.http.HTTPStatus;
import com.fiberhome.remoteime.cybergarage.unnp.Device;
import com.fiberhome.remoteime.cybergarage.unnp.UPnP;

public class SSDPSearchResponse extends SSDPResponse
{
    ////////////////////////////////////////////////
    //    Constructor
    ////////////////////////////////////////////////

    public SSDPSearchResponse()
    {
        setStatusCode(HTTPStatus.OK);
        setCacheControl(Device.DEFAULT_LEASE_TIME);
        setHeader(HTTP.SERVER, UPnP.getServerName());
        setHeader(HTTP.EXT, "");
    }
}
