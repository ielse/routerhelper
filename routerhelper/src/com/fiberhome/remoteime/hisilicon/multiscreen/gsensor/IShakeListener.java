package com.fiberhome.remoteime.hisilicon.multiscreen.gsensor;

public interface IShakeListener
{
    void shake(int times, int level);
}
