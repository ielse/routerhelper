package com.fiberhome.remoteime.hisilicon.multiscreen.mybox;

import com.fiberhome.remoteime.cybergarage.unnp.Device;

public interface IOriginalDeviceListListener
{
    void deviceAdd(Device device);

    void deviceRemoved(Device device);
}
