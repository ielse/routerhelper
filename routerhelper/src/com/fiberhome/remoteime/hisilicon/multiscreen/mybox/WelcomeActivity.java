package com.fiberhome.remoteime.hisilicon.multiscreen.mybox;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.fiberhome.opticalbox.R;
import com.fiberhome.remoteime.hisilicon.dlna.dmc.gui.activity.AppPreference;
import com.fiberhome.remoteime.hisilicon.multiscreen.protocol.utils.ServiceUtil;

public class WelcomeActivity extends Activity
{
    /**
     * CN:1秒后跳转
     */
    private final long SPLASH_LENGTH = 1000;

    private Handler myHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        initView();
        initData();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        myHandler.postDelayed(splash, SPLASH_LENGTH);
    };

    @Override
    protected void onPause()
    {
        super.onPause();
        myHandler.removeCallbacks(splash);
    };

    private void initView()
    {
        // CN:允许布局被状态栏遮盖，防止退出全屏后重新布局。
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        setContentView(R.layout.mybox_welcome);
    }

    private void initData()
    {
        ServiceUtil.startMultiScreenControlService(this);
    }

    private Runnable splash = new Runnable()
    {
        public void run()
        {
            showStatusBar();
            startTheActivity();
        }
    };

    /**
     * Show status bar of system.<br>
     * CN:在欢迎页面提前显示系统状态栏，防止设备发现页面UI抖动。
     */
    private void showStatusBar()
    {
        getWindow().setFlags(~WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    /**
     * Show guide activity if this application runs at first time, otherwise
     * start home activity.<br>
     * CN:判断程序是第几次运行，如果是第一次运行则显示引导页面，否则启动首页。
     */
    private void startTheActivity()
    {
        if (AppPreference.isAppFirstUse())
        {
            Intent intent = new Intent(WelcomeActivity.this, GuideActivity.class);
            startActivity(intent);
            finish();
        }
        else
        {
            Intent intent = new Intent(WelcomeActivity.this, DeviceDiscoveryActivity.class);
            startActivity(intent);
            finish();
        }
    }

}
