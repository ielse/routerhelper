package com.fiberhome.remoteime.hisilicon.multiscreen.upnputils;

public interface UpnpDeviceListHandler
{
    void updateDeviceList();
}
