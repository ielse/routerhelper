package com.fiberhome.remoteime.hisilicon.dlna.dmc.processor.interfaces;

public interface SwipeCallback {
	void swipeToNext();
	void swipeToPrevious();
	void onTap();
}
