package com.fiberhome.opticalbox.view;

import java.util.Arrays;
import java.util.Random;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.utils.AnimUtil;

public class HealthCheckingView extends FrameLayout {

	private ImageView imgScan;
	private TextView txProgress, txPercent;
	private View layResult;
	private ImageView imgWifi, imgResult;

	private Random random = new Random();
	private Handler handler = new Handler();
	private int[] tmpProgress = new int[10];
	private int currTmp = 0;
	private boolean useAnimProgress = true;

	public HealthCheckingView(Context context) {
		this(context, null);
	}

	public HealthCheckingView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public HealthCheckingView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		LayoutInflater.from(context).inflate(R.layout.ob_view_health_checking, this);
		imgScan = (ImageView) findViewById(R.id.img_scan);
		txProgress = (TextView) findViewById(R.id.tx_progress);
		txPercent = (TextView) findViewById(R.id.tx_percent);
		layResult = findViewById(R.id.lay_result);
		imgWifi = (ImageView) findViewById(R.id.img_wifi);
		imgResult = (ImageView) findViewById(R.id.img_result);
	}

	public void restart() {
		imgScan.setVisibility(View.VISIBLE);
		imgScan.clearAnimation();
		imgScan.setAnimation(AnimUtil.load(getContext(), R.anim.ob_rotate_forever));
		txProgress.setVisibility(View.VISIBLE);
		txPercent.setVisibility(View.VISIBLE);
		layResult.setVisibility(View.GONE);
		setProgress(0);
	}

	public void setProgress(int progress) {
		if (0 <= progress && 100 >= progress) {
			int lastProgress = Integer.valueOf(txProgress.getText().toString());
			if (useAnimProgress && progress > lastProgress) {
				setProgressByAnim(progress);
			} else {
				txProgress.setText(String.valueOf(progress));
			}
		}
	}

	public void setProgressByAnim(int progress) {
		int lastProgress = Integer.valueOf(txProgress.getText().toString());
		if (0 <= progress && 100 >= progress) {
			for (int i = 0; i < tmpProgress.length - 1; i++) {
				tmpProgress[i] = lastProgress + random.nextInt(progress - lastProgress);
			}
			tmpProgress[tmpProgress.length - 1] = progress;
			Arrays.sort(tmpProgress);
			handler.removeCallbacks(runAnimProgress);
			currTmp = 0;
			handler.post(runAnimProgress);
		}
	}

	private Runnable runAnimProgress = new Runnable() {
		@Override
		public void run() {
			if (currTmp < tmpProgress.length) {
				txProgress.setText(String.valueOf(tmpProgress[currTmp++]));
				handler.postDelayed(runAnimProgress, 35);
			}

		}
	};

	public void finish(boolean resultRight) {
		imgScan.clearAnimation();
		imgScan.setVisibility(View.GONE);
		txProgress.setVisibility(View.GONE);
		txPercent.setVisibility(View.GONE);
		layResult.setVisibility(View.VISIBLE);
		imgWifi.setImageResource(resultRight ? R.drawable.ob_check_result_wifi_right : R.drawable.ob_check_result_wifi_wrong);
		imgResult.setImageResource(resultRight ? R.drawable.ob_check_result_right : R.drawable.ob_check_result_wrong);
	}
}
