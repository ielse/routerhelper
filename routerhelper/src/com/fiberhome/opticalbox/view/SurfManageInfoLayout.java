package com.fiberhome.opticalbox.view;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.ui.SurfManageActivity;
import com.fiberhome.opticalbox.ui.SurfManageActivity.SurfManageInfo;
import com.fiberhome.opticalbox.utils.LogUtil;

/**
 * @author wangpeng
 * 
 */
public class SurfManageInfoLayout extends FrameLayout {

	private FrameLayout layContent;
	private View layManage;
	private View viewContent;

	private boolean isAnim = false;
	private int currentX;
	private Handler handler = new Handler();
	private int layManageWidth;

	/* menu */
	private View layDel;

	/* content */
	private TextView txName, txMac;
	private SurfManageInfo sci;

	public SurfManageInfoLayout(Context context) {
		this(context, null);
	}

	public SurfManageInfoLayout(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public SurfManageInfoLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		LayoutInflater.from(getContext()).inflate(R.layout.ob_lay_list_content_manage, this);
		layManage = findViewById(R.id.lay_manage);
		
		layManageWidth = calcViewMeasure(layManage);

		layDel = findViewById(R.id.lay_del);

		layContent = (FrameLayout) findViewById(R.id.lay_content);
		
		viewContent = LayoutInflater.from(getContext()).inflate(R.layout.ob_list_content_device_enable, null);
		layContent.addView(viewContent);

		/* init content */
		txName = (TextView) findViewById(R.id.tx_name);
		txMac = (TextView) findViewById(R.id.tx_mac);

		setOnClickListener(new OnClickListener() {
			@Override public void onClick(View v) {
				LogUtil.e("surf manege onClick");
				if (sci.isMenuOpen) {
					closeMenuByAnim();
				} else {
					openMenuByAnim();
				}
			}
		});
	}
	
	/** 测量控件的尺寸 */
	public static int calcViewMeasure(View view) {
		int width = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
		int expandSpec = View.MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, View.MeasureSpec.AT_MOST);
		view.measure(width, expandSpec);
		return view.	getMeasuredWidth();
	}

	public void setSurfManageInfo(SurfManageInfo surfManageInfo) {
		this.sci = surfManageInfo;

		String name = getContext().getString(R.string.ob_device_unknown);
		if (!TextUtils.isEmpty(surfManageInfo.getNickName())) {
			name = surfManageInfo.getNickName();
		} else {
			if (!TextUtils.isEmpty(surfManageInfo.HostName)) {
				name = surfManageInfo.HostName;
			}
		}
		txName.setText(name);

		txMac.setText(surfManageInfo.MAC);
		if (surfManageInfo.isMenuOpen) {
			openMenu();
		} else {
			closeMenu();
		}
	}

	public View getLayContent() {
		return layContent;
	}

	private void setMenuClickListener(boolean is2Set) {
		layDel.setOnClickListener(is2Set ? onMenuDelClickListener : onOpenSlefClickListener);
	}

	/* menu click */
	private OnClickListener onMenuDelClickListener = new OnClickListener() {
		@Override public void onClick(View view) {
			if (getContext() instanceof SurfManageActivity) {
				if (sci != null) {
					closeMenu();
					((SurfManageActivity) getContext()).setDeviceDisable(sci);
				}
			}
		}
	};
	/* menu click */
	private OnClickListener onOpenSlefClickListener = new OnClickListener() {
		@Override public void onClick(View v) {
			sci.isMenuOpen = true;
			openMenuByAnim();
		}
	};

	private Runnable animOpenMenu = new Runnable() {
		@Override public void run() {
			currentX += 30;
			if (currentX > layManageWidth) {
				layContent.scrollTo(layManageWidth, 0);
				isAnim = false;
				sci.isMenuOpen = true;
				setMenuClickListener(sci.isMenuOpen);
			} else {
				layContent.scrollTo(currentX, 0);
				postDelayed(animOpenMenu, 20);
			}

		}
	};

	private Runnable animCloseMenu = new Runnable() {
		@Override public void run() {
			currentX -= 30;
			if (currentX < 0) {
				layContent.scrollTo(0, 0);
				isAnim = false;
				sci.isMenuOpen = false;
				setMenuClickListener(sci.isMenuOpen);
			} else {
				layContent.scrollTo(currentX, 0);
				postDelayed(animCloseMenu, 20);
			}

		}
	};

	private void openMenuByAnim() {
		if (!isAnim) {
			isAnim = true;
			currentX = 0;
			handler.post(animOpenMenu);
		}
	}

	public void openMenu() {
		sci.isMenuOpen = true;
		layContent.scrollTo(layManageWidth, 0);
		setMenuClickListener(true);
	}

	private void closeMenuByAnim() {
		if (!isAnim) {
			isAnim = true;
			currentX = layManageWidth;
			handler.post(animCloseMenu);
		}
	}

	public void closeMenu() {
		sci.isMenuOpen = false;
		layContent.scrollTo(0, 0);
		setMenuClickListener(false);
	}

}
