package com.fiberhome.opticalbox.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.utils.BaseUtil;

/**
 * @description 标题栏
 * @author wangpeng
 * @date 2014-12-20
 */
public class TitleView extends FrameLayout {

	private TextView txTitle;
	private LinearLayout layLeft, layRight;
	private View layContent;

	private static int menuId = Integer.MAX_VALUE / 2;

	public TitleView(Context context) {
		this(context, null);
	}

	public TitleView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public TitleView(final Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		LayoutInflater.from(context).inflate(R.layout.ob_view_title, this);
		txTitle = (TextView) findViewById(R.id.tx_title);
		layLeft = (LinearLayout) findViewById(R.id.layout_left);
		layRight = (LinearLayout) findViewById(R.id.layout_right);
		layContent = findViewById(R.id.layout_content);
	}

	public void setTitle(int textId) {
		txTitle.setText(textId);
	}

	public void setTitle(String text) {
		txTitle.setText(text);
	}

	public void setTitleColor(int color) {
		txTitle.setTextColor(color);
	}

	public void setBackground(int resId) {
		layContent.setBackgroundResource(resId);
	}
	
	public void setBackgroundColor(int color) {
		layContent.setBackgroundColor(color);
	}

	public View addLeftDrawableMenu(Context context, int drawableId, int width, int height, OnClickListener onMenuClickListener) {
		View view = createDrawableMenu(context, drawableId, width, height, onMenuClickListener);
		LinearLayout.LayoutParams LP = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
		int left = BaseUtil.dip(getContext(), 15);
		int right = BaseUtil.dip(getContext(), 20);
		view.setPadding(left, 0, right, 0);
		view.setLayoutParams(LP);
		layLeft.addView(view);
		setIdTagForMenu(view);
		return view;
	}

	public View addRightDrawableMenu(Context context, int drawableId, int width, int height, OnClickListener onMenuClickListener) {
		View view = createDrawableMenu(context, drawableId, width, height, onMenuClickListener);
		LinearLayout.LayoutParams LP = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
		int left = BaseUtil.dip(getContext(), 20);
		int right = BaseUtil.dip(getContext(), 10);
		view.setPadding(left, 0, right, 0);
		view.setLayoutParams(LP);
		layRight.addView(view);
		setIdTagForMenu(view);
		return view;
	}

	// public void addRightTextMenu(Context context, int textId, int width, int
	// height, int margins,
	// OnClickListener onMenuClickListener) {
	// Button button = createTextMenu(context, textId, width, height, margins,
	// onMenuClickListener);
	// layRight.addView(button);
	// }

	public View addLeftMenu(Context context, View view, OnClickListener onMenuClickListener) {
		view.setOnClickListener(onMenuClickListener);
		layLeft.addView(view);
		setIdTagForMenu(view);
		return view;
	}

	public FrameLayout createDrawableMenu(Context context, int drawableId, int width, int height, OnClickListener onMenuClickListener) {
		FrameLayout layF = (FrameLayout) LayoutInflater.from(getContext()).inflate(R.layout.ob_view_title_part_drawable_menu, null);
		ImageView imageView = (ImageView) layF.findViewById(R.id.ob_imageView);
		imageView.setBackgroundResource(drawableId);
		FrameLayout.LayoutParams btnLP = new FrameLayout.LayoutParams(BaseUtil.dip(context, width), BaseUtil.dip(context, height));
		imageView.setLayoutParams(btnLP);
		btnLP.gravity = Gravity.CENTER_VERTICAL;
		layF.setOnClickListener(onMenuClickListener);
		return layF;
	}

	public void removeAllMenu() {
		removeAllLeftMenu();
		removeAllRightMenu();
	}

	public void removeAllLeftMenu() {
		layLeft.removeAllViewsInLayout();
	}

	public void removeAllRightMenu() {
		layRight.removeAllViewsInLayout();
	}

	private void setIdTagForMenu(View view) {
		view.setId(menuId);
		menuId++;
	}
}
