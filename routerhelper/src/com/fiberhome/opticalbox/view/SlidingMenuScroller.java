package com.fiberhome.opticalbox.view;

import java.lang.reflect.Field;

import android.content.Context;
import android.view.animation.Interpolator;
import android.widget.HorizontalScrollView;
import android.widget.OverScroller;

/**
 * @description 侧拉菜单滑动速度控制器
 * @author wangpeng
 * @date 2014-12-20
 */
public class SlidingMenuScroller extends OverScroller {
	private int mScrollDuration = SlidingMenu.ANIM_TIME; // 滑动速度

	/**
	 * 设置速度速度
	 * 
	 * @param duration
	 */
	public void setScrollDuration(int duration) {
		this.mScrollDuration = duration;
	}

	public SlidingMenuScroller(Context context) {
		this(context, null);
	}

	public SlidingMenuScroller(Context context, Interpolator interpolator) {
		super(context, interpolator);
	}

	@Override
	public void startScroll(int startX, int startY, int dx, int dy) {
		startScroll(startX, startY, dx, dy, mScrollDuration);
	}

	public void initViewPagerScroll(HorizontalScrollView horizontalScrollView) {
		try {
			Field mScroller = HorizontalScrollView.class.getDeclaredField("mScroller");
			mScroller.setAccessible(true);
			mScroller.set(horizontalScrollView, this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
