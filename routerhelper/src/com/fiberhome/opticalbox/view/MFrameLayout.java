package com.fiberhome.opticalbox.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

/**
 * @description 截断slidingMenu展开时，右侧内容事件
 * @author wangpeng
 * @date 2014-12-22
 */
public class MFrameLayout extends FrameLayout {

	private boolean intercept = false;

	public MFrameLayout(Context context) {
		this(context, null);
	}

	public MFrameLayout(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public MFrameLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		return intercept ? true : super.onInterceptTouchEvent(ev);
	}

	public void setIntercept(boolean intercept) {
		this.intercept = intercept;
	}

}
