package com.fiberhome.opticalbox;

public class C {

	public interface app {
		String ipAddress = "^((25[0-5]|2[0-4]\\d|[01]?\\d\\d?)($|(?!\\.$)\\.)){4}$";
		String speedTestAddress = "http://hg.fiberhome.com:8087/RouterHelper/routerhelper.apk";
		int speedTestTime = 20;

		String keybordServerIpAddress = "192.168.8.253";
		String versionAddress = "http://hg.fiberhome.com:8087/RouterHelper/route.json";

		String productType_MR828 = "MR828";
		String productType_MR810 = "MR810";
	}

	public interface sp {
		String selectedTv = "selected_TV";
	}

	public interface bundle {
		String startHour = "start_hour";
		String startMin = "start_min";
		String endHour = "end_hour";
		String endMin = "end_min";
		String dayOfWeek = "day_of_week";
		String mac = "mac";
		String hostName = "host_name";

		String imagePosition = "image_position";
		String videoPosition = "video_position";
	}
}
