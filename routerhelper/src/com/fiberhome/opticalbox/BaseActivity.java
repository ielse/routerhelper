package com.fiberhome.opticalbox;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.net.NetManager;
import com.fiberhome.opticalbox.net.NetManager.TcpErrorListener;
import com.fiberhome.opticalbox.net.NetManager.TcpListener;
import com.fiberhome.opticalbox.net.bean.Parameter;
import com.fiberhome.opticalbox.utils.AnimUtil;
import com.fiberhome.opticalbox.utils.BaseUtil;
import com.fiberhome.opticalbox.utils.DialogUtil;
import com.fiberhome.opticalbox.utils.NetworkUtil;
import com.fiberhome.opticalbox.view.TitleView;
import com.google.gson.Gson;

/**
 * @description 所有活动应该继承到基类<br>
 *              提供了活动常用的对象和方法
 * @author wangpeng
 * @date 2014-12-20
 */
public abstract class BaseActivity extends FragmentActivity implements TcpListener, TcpErrorListener {
	/** 布局加载器 */
	public LayoutInflater inflater;
	/** 碎片管器器 */
	public FragmentManager fragmentManager;
	/** 对话框构造器 */
	public DialogUtil dialogUtil;
	/** Json格式字符串与对象装换工具 */
	public Gson gson;

	public NetManager netManager;
	public List<TcpErrorListener> childTcpErrorListenerList = new ArrayList<TcpErrorListener>();
	public List<TcpListener> childTcpListenerList = new ArrayList<TcpListener>();

	/** 标题栏 活动通用控件 */
	public TitleView titleView;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActivityCollector.addActivity(this);

		inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		fragmentManager = getSupportFragmentManager();

		dialogUtil = DialogUtil.init(this);
		gson = new Gson();

		netManager = new NetManager(this, this, this);

		titleView = (TitleView) findViewById(R.id.ob_titleView);
	}

	@Override public void onReceive(String cmdType, Parameter param) {
		for (TcpListener childTcpListener : childTcpListenerList) {
			childTcpListener.onReceive(cmdType, param);
		}
	}

	@Override public void onError(String cmdType) {
		if (childTcpErrorListenerList.size() > 0) {
			for (TcpErrorListener childTcpErrorListener : childTcpErrorListenerList) {
				childTcpErrorListener.onError(cmdType);
			}
		} else {
			showToast(NetworkUtil.isDisconnected(this) ? R.string.ob_error_net : R.string.ob_error_request);
		}
	}

	public void registerChildTcpListener(TcpListener tcpListener) {
		if (tcpListener != null) {
			childTcpListenerList.add(tcpListener);
		}
	}

	public void unRegisterChildTcpListener(TcpListener tcpListener) {
		if (tcpListener != null) {
			if (childTcpListenerList.contains(tcpListener)) {
				childTcpListenerList.remove(tcpListener);
			}
		}
	}

	public void registerChildTcpErrorListener(TcpErrorListener tcpErrorListener) {
		if (tcpErrorListener != null) {
			childTcpErrorListenerList.add(tcpErrorListener);
		}
	}

	public void unRegisterChildTcpErrorListener(TcpErrorListener tcpErrorListener) {
		if (tcpErrorListener != null) {
			if (childTcpErrorListenerList.contains(tcpErrorListener)) {
				childTcpErrorListenerList.remove(tcpErrorListener);
			}
		}
	}

	@Override protected void onResume() {
		super.onResume();
	}

	@Override protected void onStop() {
		super.onStop();
	}

	@Override protected void onDestroy() {
		super.onDestroy();
		if (this instanceof TcpListener) {
			unRegisterChildTcpListener((TcpListener) this);
		}
		if (this instanceof TcpErrorListener) {
			unRegisterChildTcpErrorListener((TcpErrorListener) this);
		}
		ActivityCollector.removeActivity(this);
	}

	/**
	 * 启动新活动
	 * 
	 * @param target
	 *            目标活动类名
	 * @param params
	 *            传递参数 <b>注：格式以key，value成双成对设置</b>
	 */
	public void actionStart(Class<?> target, Object... params) {
		actionStart(target, BaseUtil.initBundle(params));
	}

	/**
	 * 启动新活动
	 * 
	 * @param target
	 *            目标活动类名
	 * @param bundle
	 *            传递参数
	 */
	public void actionStart(Class<?> target, Bundle bundle) {
		Intent intent = new Intent(this, target);
		if (bundle != null) {
			intent.putExtras(bundle);
		}
		startActivity(intent);
	}

	@Override public void startActivity(Intent intent) {
		super.startActivity(intent);
		overridePendingTransition(R.anim.ob_activity_right_enter, R.anim.ob_activity_left_exit);
	}

	@Override public void finish() {
		super.finish();
		if (ActivityCollector.size() > 1) {
			overridePendingTransition(R.anim.ob_activity_left_enter, R.anim.ob_activity_right_exit);
		}
	}

	public void saveToSharedPreferences(String key, Object value) {
		SharedPreferences sp = getSharedPreferences(getString(R.string.ob_app_name), Context.MODE_PRIVATE);
		sp.edit().putString(key, value.toString()).commit();
	}

	public String getFromSharedPreferences(String key) {
		SharedPreferences sp = getSharedPreferences(getString(R.string.ob_app_name), Context.MODE_PRIVATE);
		return sp.getString(key, null);
	}

	public void showToast(int textId) {
		showToast(getString(textId));
	}

	public void showToast(String text) {
		View view = inflater.inflate(R.layout.ob_toast_message, null);
		((TextView) view.findViewById(R.id.tx_message)).setText(text);
		Toast mToast = new Toast(getApplicationContext());
		mToast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM, 0, 100);
		mToast.setDuration(500);
		mToast.setView(view);
		mToast.show();
	}

	public void startAnimation(View view, Animation animation) {
		view.setVisibility(View.VISIBLE);
		view.clearAnimation();
		view.startAnimation(animation);
	}

	public void endAnimation(View view, Animation animation) {
		view.clearAnimation();
		view.startAnimation(animation);
		view.setVisibility(View.GONE);
	}

	public OnClickListener onBackClickListener = new OnClickListener() {
		@Override public void onClick(View view) {
			startAnimation(view, AnimUtil.init().addAlpha(1f, 0.2f, 0, 200, new LinearInterpolator(), Animation.REVERSE).create());
			finish();
		}
	};

}
