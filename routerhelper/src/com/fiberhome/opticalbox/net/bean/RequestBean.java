
package com.fiberhome.opticalbox.net.bean;

public class RequestBean {
    String RPCMethod;
    String Plugin_Name;
    String ID;
    Parameter Parameter;

    public RequestBean(String rPCMethod, String plugin_Name, String iD,
            Parameter parameter) {
        super();
        RPCMethod = rPCMethod;
        Plugin_Name = plugin_Name;
        ID = iD;
        Parameter = parameter;
    }

    public String getRPCMethod() {
        return RPCMethod;
    }

    public String getPlugin_Name() {
        return Plugin_Name;
    }

    public String getID() {
        return ID;
    }

    public Parameter getParameter() {
        return Parameter;
    }

}
