
package com.fiberhome.opticalbox.net.bean;


public class ResponseBean {
    public String Result;
    public String ID;
    public Parameter return_Parameter;

	public String getResult() {
		return Result;
	}

	public String getID() {
		return ID;
	}

	public Parameter getParameter() {
		return return_Parameter;
	}

    @Override
    public String toString() {
    	return "ResponseBean [Result=" + Result + ", ID=" + ID + ", return_Parameter=" + return_Parameter + "]";
    }

}
