package com.fiberhome.opticalbox.net.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fiberhome.opticalbox.net.bean.data.UrlInfo;

public class Parameter {

	String CmdType;
	String SequenceId;
	String FailReason;
	public String Status;

	public String USER;
	public String PASSWORD;
	public String Dest;// TraceRoute诊断时使用的目的地址
	public String WANName;// 连接名称
	public String TraceStatus;// 执行完成
	public String TraceResult;// 诊断的结果
	public String InformStatus;//
	public String InformResult;// 诊断的结果
	public String RegisterStatus;
	public String RegisterResult;
	public String RegisterProgressResult;
	public String RegisterBussNameResult;
	public String RegisterServiceResult;
	public String Temperature; // 温度
	public String Vottage; // 电压
	public String Current; // 电流
	public String TXPower; // 发送光功率
	public String RXPower; // 接收光功率
	public String PonStatus;// 注册状态
	public String NUM;// WAN连接的条数
	public String Num; // ssid下挂设备个数
	public String SERVICELIST;// 接口描述
	public String CONNECTIONTYPE;// 连接类型
	public String IPADDRESS;// IP地址
	public String PingStatus;// PING状态
	public String PingResult;// PING结果
	public String IP;// TR069 WAN连接所获取到的IP地址
	public String RegStatus;// 注册状态
	public String BussinessStatus;// 业务状态
	public String SWVersion;// 软件版本
	public String HDVerson;// 硬件版本
	public String ProductCLass;// 网关型号
	public String CPUClass;// CPU型号
	public String DEVType;// 设备型号（OUI-SN）
	public String SN;// 设备的SN号
	public String MAC;// 设备的MAC
	public String LOID;// LOID信息
	public String Info;// 下挂设备信息
	public String SYSDuration;// 系统上电时间
	public String PPPoEDuration;// PPPOE拨号成功持续时间
	public String PONDuration;// PON注册授权成功持续时间
	public String Percent;// CPU/MEM占用的百分比
	public String StartTime;// Wifi开机的时间
	public String EndTime;// Wifi关机的时间
	public String ControlCycle;// 控制周期
	public String URL;// 下载的URL地址
	public String Method;// Start 开始下载/Stop 停止下载
	public String Time;
	public String RealTimeRate;// 实时速率
	public String LAN1Status;// ON表示LAN1口已经连接；OFF表示LAN1口未连接
	public String LAN2Status;
	public String LAN3Status;
	public String LAN4Status;
	public String WAN1Status;// ON表示WAN1口已经连接，OFF表示WAN1口未连接
	public String WIFIModuleStatus;// ON表示Wifi模块开启，OFF 表示Wifi模块关闭
	public String CONNECTIONSTATUS; //
	public String ConnectionStatus;// 0，则PPPOE拨号成功；1则PPPoE拨号失败
	public String ConnectionStatus1;// 0，则PPPOE拨号成功；1则PPPoE拨号失败
	public String WANStatus;// WAN连接拨号状态
	public String DialReason;// 拨号失败原因
	public String VOIPNAME1;// 第一路的电话号码
	public String VOIPNAME2;// 第二路的电话号码
	public String Line1Status;// VOIP1的状态
	public String Line2Status;// VOIP2的状态
	public String Type;// 黑白名单类型
	public ArrayList<UrlInfo> Urls;// 黑白名单
	public String AccessRights;// 访问权限
	public String DevTimeRange;// 访问时段

	public String USERID;
	public String WifiEnable;
	public String line;// 是否上行线路接入，0为未接入，1为接入
	public String PonRegStatus;// PON口注册状态
	public String leng;// 报文长度
	public String time;// 次数
	public String CPUPercent;// CPU占用率
	public String STOREPercent;// 内存占用率
	public String VOIPRegStatus;// 语音注册状态
	public String Cardno;// 卡序列号
	public String Capability;// 设备能力
	public String UPLink;// 上行方式
	public String Card;// 是否为机卡分离网关 0 否/1 是

	public String PassWD;// 表示需要进行验证的Useradmin的帐号访问密码或Telecomadmin的帐号访问密码
	public String WANInfo;// WAN连接的信息

	/* 1.4 设置WIFI连接信息 */
	/** wifi接口的编号：从1开始 */
	public String WifiIndex;
	/** 当前读取的SSID的编号取值范围（对于单频网关为1-4，对于双频网关为1-8，0表示整体WIFI模块时，只关注“Enable“参数） */
	public String SSIDIndex;
	/** SSID名称 */
	public String SSID;
	/** SSID密码 */
	public String PWD;
	/** 加密方式OPEN=1,SHARED=2,WPA-PSK=3,WPA-PSK2=4,Mixd WPA2/WPA-PSK=5 */
	public String ENCRYPT;
	/** 目前使用的信道（0表示Auto） */
	public String Channel;
	/** 此SSID是否启用（0表示启用，1表示不启用） */
	public String Enable;
	/** 此SSID的网络功耗（百分比表示，取值0-100） */
	public String PowerLevel;

	public String WiFiCmdMode;
	
	public String Hidden;

	/* 7.28获取WLAN接口信息 */
	/** SSID-1的加密状态（0表示未启用加密，1表示启用加密） */
	public String ENCTRPY;

	/* 1.11.2 管理员密码校验 */
	/** 需要校验的用户名 */
	public String USERNAME;
	// 需要校验的密码
	// public String PASSWORD;
	/* 7.9查询WAN连接的条数 */

	/** WAN链接的名称，多条WAN连接以/分割 */
	public String Description;

	public String WLANInform;

	/* 上网向导设置 */
	/** 路由获取地址的方式，“DHCP”/“PPPoE”/ “Static” */
	public String UPLINKMODE;
	/** 路由获取地址的方式，“DHCP”/“PPPoE”/ “Static” */
	public String ADDRESSTYPE;
	/** 子网掩码 */
	public String SUBNETMASK;
	/** 网关地址 */
	public String GATEWAY;
	/** 首选DNS */
	public String DNS1;
	/** 备选DNS */
	public String DNS2;
	/** IPV6 IP地址 */
	public String IPV6_IPADDRESS;
	/** IPV6的前缀长度 */
	public String IPV6_PREFIXLENGTH;
	/** IPV6的网关地址 */
	public String IPV6_GATEWAY;
	/** IPV6的首选DNS */
	public String IPV6_DNS1;
	/** IPV6的备选DNS */
	public String IPV6_DNS2;
	/** IPV6协商前缀 */
	public String IPV6_PRIFIX;
	/** PPPoE用户名 */
	public String PPPOE_USR_NAME;
	/** PPPoE密码 */
	public String PPPOE_PASSWORD;
	/** 是否启用此WAN连接NAT功能 */
	public String NATENABLE;
	/** 是否开启QOS功能 */
	public String QOSENABLE;
	/** VLANID号；（0～4095） 如果取 NaN表示没有打开VLANID使能开关； */
	public String VLANID;
	/** VLAN的优先级 0～8 */
	public String VLANPRIORITY;

	/* led */
	/** 设置LED灯状态；取值范围”OFF”关闭，”ON”开启 */
	public String LEDStatus;
	/* 上联方式 */
	/** 上联方式，wired表示有线上联，wireless表示无线上联 */
	public String MODE;
	/** 无线工作模式：“AP”/”Client” */
	public String Mode;
	/** 当前SSID的关联状态（down表示未关联成功，up表示关联成功） */
	public String LinkStatus;

	/** 关联信息（在LinkStatus为down时有效，0表示正在关联，1表示密码错误，2表示关联超时） */
	public String LinkInfo;

	/* qos */
	/** 取值为“fair”表示各项业务公平竞争；”game”表示游戏优先；“video”表示视频优先；”web”表示上网优先 */
	public String QoSMode;
	/* 绿色上网 */
	/** 下挂设备的信息列表 */
	public List<AttachInfo> AttachInfoList;

	/** 下挂设备的信息 */
	public static class AttachInfo implements Serializable {
		private static final long serialVersionUID = 5727129809145865437L;
		/** 下挂终端的MAC地址信息 */
		public String MAC;
		/** 下挂终端的IP地址信息 */
		public String IP;
		/** 设备别名 */
		public String name;
		/** 下挂主机名 */
		public String HostName;
		/** 最近的断开连接时间，0表示目前正在下挂的设备，已经断开的设备显示断开的时间，以秒为单位 */
		public String DisconnectionTime;
	}

	public List<MACInfo> MACList;

	public static class MACInfo implements Serializable {
		private static final long serialVersionUID = 5727129809145865437L;
		/** 下挂终端的MAC地址信息 */
		public String MAC;
		/** 设备别名 */
		public String name;
		/** 是否启用Internet的访问权限（ON表示开启、OFF表示不开启） */
		public String InternetAccessRight;
		/** 开启的时间，如：9:30 */
		public String StartTime;
		/** 关闭的时间，如：21:20 */
		public String EndTime;
		/** 用七个字符0和1表示该规则是否在星期几生效，从星期一开始计算，例如：”0101010”，表示该规则在星期二、星期四、星期六生效 */
		public String DaysOfWeek;
	}

	/** 是否启用Internet的访问权限（ON表示开启、OFF表示不开启） */
	public String InternetAccessRight;
	/** 用七个字符0和1表示该规则是否在星期几生效，从星期一开始计算，例如：”0101010”，表示该规则在星期二、星期四、星期六生效 */
	public String DaysOfWeek;
	/** */
	public String mac;
	/** 别名 */
	public String name;
	public String Uprate;
	public String downrate;

	/***********/

	public List<WifiInfo> WIFILIST;

	public class WifiInfo {
		private String Standard;
		private String Mode;

		public WifiInfo(String stand, String mode) {
			super();
			Standard = stand;
			Mode = mode;
		}

		public String getStandard() {
			return Standard;
		}

		public String getMode() {
			return Mode;
		}
	}

	public List<WifiSSIDList> SSIDList;

	public static class WifiSSIDList {
		private String SSID;
		private String PowerLevel;
		private String ENCRYPT;
		private String Connected;

		public WifiSSIDList(String ssid, String power, String encr, String conn) {
			super();
			SSID = ssid;
			PowerLevel = power;
			ENCRYPT = encr;
			Connected = conn;
		}

		public String getSSID() {
			return SSID;
		}

		public String getPowerLevel() {
			return PowerLevel;
		}

		public String getENCRYPT() {
			return ENCRYPT;
		}

		public String getConnected() {
			return Connected;
		}

		public int getLevel() {
			try {
				return Integer.valueOf(PowerLevel);
			} catch (Exception e) {
				return 1;
			}
		}

		public int getMyLevel() {
			int level = getLevel();
			if (level < 40) {
				return 1;
			} else if (level < 70) {
				return 2;
			} else {
				return 3;
			}
		}

		public int getSecurityType() {
			if (ENCRYPT == null) {
				return 1;
			} else {
				try {
					return Integer.valueOf(ENCRYPT);
				} catch (Exception e) {
					return 1;
				}
			}
		}

		public boolean isSecurity() {
			// 加密方式OPEN=1,SHARED=2,WPA-PSK=3,WPA-PSK2=4,Mixd WPA2/WPA-PSK=5
			// 是这样吗？？？？？？？？？？？
			return getSecurityType() != 1;
		}
	}

	public Parameter() {
		super();
	}

	public Parameter(String cmdType, String sequenceId) {
		super();
		CmdType = cmdType;
		SequenceId = sequenceId;
	}

	public String getCmdType() {
		return CmdType;
	}

	public String getSequenceId() {
		return SequenceId;
	}

	public String getStatus() {

		return Status;
	}

	public String getFailReason() {
		return FailReason;
	}

	public String getTemperature() {
		return Temperature;
	}

	public String getVOTTAGE() {
		return Vottage;
	}

	public String getCurrent() {
		return Current;
	}

	public String getTXPower() {
		return TXPower;
	}

	public String getRXPower() {
		return RXPower;
	}

	public String getPonStatus() {
		return PonStatus;
	}

	public String getNUM() {
		return NUM;
	}

	public String getSERVICELIST() {
		return SERVICELIST;
	}

	public String getCONNECTIONTYPE() {
		return CONNECTIONTYPE;
	}

	public String getIPADDRESS() {
		return IPADDRESS;
	}

	public String getPingStatus() {
		return PingStatus;
	}

	public String getPingResult() {
		return PingResult;
	}

	public String getIP() {
		return IP;
	}

	public String getRegStatus() {
		return RegStatus;
	}

	public String getBussinessStatus() {
		return BussinessStatus;
	}

	public String getSWVersion() {
		return SWVersion;
	}

	public String getHDVerson() {
		return HDVerson;
	}

	public String getProductCLass() {
		return ProductCLass;
	}

	public String getCPUClass() {
		return CPUClass;
	}

	public String getDEVType() {
		return DEVType;
	}

	public String getSN() {
		return SN;
	}

	public String getLOID() {
		return LOID;
	}

	public String getInfo() {
		return Info;
	}

	public String getSYSDuration() {
		return SYSDuration;
	}

	public String getPPPoEDuration() {
		return PPPoEDuration;
	}

	public String getPONDuration() {
		return PONDuration;
	}

	public String getPERCENT() {
		return Percent;
	}

	public String getRealTimeRate() {
		return RealTimeRate;
	}

	public String getPercent() {
		return Percent;
	}

	public String getStartTime() {
		return StartTime;
	}

	public String getEndTime() {
		return EndTime;
	}

	public String getControlCycle() {
		return ControlCycle;
	}

	public String getURL() {
		return URL;
	}

	public String getMethod() {
		return Method;
	}

	public String getLAN1Status() {
		return LAN1Status;
	}

	public String getLAN2Status() {
		return LAN2Status;
	}

	public String getLAN3Status() {
		return LAN3Status;
	}

	public String getLAN4Status() {
		return LAN4Status;
	}

	public String getWANStatus() {
		return WAN1Status;
	}

	public String getWIFIModuleStatus() {
		return WIFIModuleStatus;
	}

	public String getVOIPNAME1() {
		return VOIPNAME1;
	}

	public String getVOIPNAME2() {
		return VOIPNAME2;
	}

	public String getLine1Status() {
		return Line1Status;
	}

	public String getLine2Status() {
		return Line2Status;
	}

	public String getType() {
		return Type;
	}

	public String getAccessRights() {
		return AccessRights;
	}

	public String getDevTimeRange() {
		return DevTimeRange;
	}

	public void setType(String type) {
		Type = type;
	}

	public ArrayList<UrlInfo> getUrls() {
		return Urls;
	}

	public String getUserName() {
		return USERNAME;
	}

	public String getPassword() {
		return PASSWORD;
	}

	/* 2015 */
	public String Result; // 物理连接诊断结果 .兼 WAN连接诊断结果;兼 路由诊断结果

	public String DeviceType; // 关联设备
	
	public String ControlWifiMode; // 防蹭网
}
