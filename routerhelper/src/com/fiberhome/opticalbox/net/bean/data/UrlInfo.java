/**
 * 
 */
package com.fiberhome.opticalbox.net.bean.data;

/**
 * @author Administrator
 * 
 */
public class UrlInfo {

	String Url;

	public UrlInfo() {

	}

	public UrlInfo(String url) {

		this.Url = url;
	}

	public String getUrl() {
		return Url;
	}

	public void setUrl(String url) {
		this.Url = url;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		// return "url"+":"+murl;
		return "UrlInfo [Url=" + Url + "]";
	}

}
