package com.fiberhome.opticalbox.net.bean.data;

public class DeviceRight {
    String mMac;
    String mName;
    String mIp;
    String mNetMod;
    String mDeviceType;
    String mOs;

    String mRight;
    
    public DeviceRight(){
        
    }

    public DeviceRight(String mac,String name,String ip,String netmod,String devicetype,String os,String right) {
        mMac = mac;
        mName = name;
        mIp=ip;
        mNetMod = netmod;
        mDeviceType = devicetype;
        mOs = os;
        mRight = right;
        
    }

    public String getmDeviceType() {
        return mDeviceType;
    }

    public void setmDeviceType(String mDeviceType) {
        this.mDeviceType = mDeviceType;
    }

    public String getmMac() {
        return mMac;
    }

    public void setmMac(String mMac) {
        this.mMac = mMac;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmIp() {
        return mIp;
    }

    public void setmIp(String mIp) {
        this.mIp = mIp;
    }

    public String getmNetMod() {
        return mNetMod;
    }

    public void setmNetMod(String mNetMod) {
        this.mNetMod = mNetMod;
    }

    public String getmRight() {
        return mRight;
    }

    public void setmRight(String mRight) {
        this.mRight = mRight;
    }
    public String getmOs() {
        return mOs;
    }

    public void setmOs(String mOs) {
        this.mOs = mOs;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub

        //"MAC":"%s","IP":"%s","NAME":"%s","NetMode":"%s","DevType":"%s","OS":"%s","InternetAccessRight":"ON"
        return "MAC"+":"+this.getmMac()+"IP"+":"+this.getmIp()+"NAME"+":"+this.getmName()+"NetMode"+this.getmNetMod()+"DevType"+":"+this.getmDeviceType()
                +"OS"+":"+this.getmOs()+"InternetAccessRight"+":"+this.getmRight();
    }
    
}
