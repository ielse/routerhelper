package com.fiberhome.opticalbox.net.bean.data;

import java.io.Serializable;

public class VersionInfo implements Serializable {

	/**
	 * @Fields: serialVersionUID
	 * @Todo: TODO
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @Fields: versionName
	 * @Todo: version name
	 */
	private String versionName;

	/**
	 * @Fields: url
	 * @Todo: url from server;
	 */
	private String url;

	/**
	 * @Fields: descriptoin
	 * @Todo: description about the new version
	 */
	private String description;

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String descriptoin) {
		this.description = descriptoin;
	}

	@Override
	public String toString() {
		return this.versionName + " " + this.url + " " + this.description;
	}

	private String result = "";

	private String desc = "";

	private String downloadUrl = "";

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

}
