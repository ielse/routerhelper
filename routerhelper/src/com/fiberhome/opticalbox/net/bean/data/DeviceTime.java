package com.fiberhome.opticalbox.net.bean.data;

public class DeviceTime {

	String mMac;
	String mStartTime;
	String mEndTime;

	public DeviceTime() {

	}

	public DeviceTime(String mac, String starttime, String endtime) {
		mMac = mac;
		mStartTime = starttime;
		mEndTime = endtime;
	}

	public String getmMac() {
		return mMac;
	}

	public void setmMac(String mMac) {
		this.mMac = mMac;
	}

	public String getmStartTime() {
		return mStartTime;
	}

	@Override
	public String toString() {
		return "MAC" + ":" + this.getmMac() + "StartTime" + ":" + this.getmStartTime() + "EndTime" + ":" + this.getmEndTime();
	}

	public void setmStartTime(String mStartTime) {
		this.mStartTime = mStartTime;
	}

	public String getmEndTime() {
		return mEndTime;
	}

	public void setmEndTime(String mEndTime) {
		this.mEndTime = mEndTime;
	}

}
