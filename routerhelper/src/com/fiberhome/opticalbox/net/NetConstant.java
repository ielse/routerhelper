package com.fiberhome.opticalbox.net;

/**
 * @author tafiagu
 * @date 2013-5-9
 */
public class NetConstant {

	public static final String UDP_RESULT_SEPARATOR = " ";

	public static final String UDP_RESULT_ASSIGN = "=";

	public static final String UNRECOGNIZE_REQUEST = "UNRECOGNIZE_REQUEST";

	public static final String RESULT_SUCCESS = "SUCCESS";

	public static final String RESULT_FAILED = "FAIL";

	/**
	 * Query WAN Rate
	 */
	public static final String QUERY_WAN_RATE_STRING = "QUERY_WAN_REALRATE";

	public static final String QUERY_WAN_RATE_FAILED = "QUERY_WAN_REALRATE_FAIL";

	public static final String QUERY_WAN_RATE_SUCCESS = "QUERY_WAN_REALRATE_SUCC";

	/**
	 * Get SN
	 */
	public static final String GET_SN_INFO_STRING = "GET_SN_INFO";

	public static final String GET_SN_INFO_FAILED = "GET_SN_INFO_FAIL";

	public static final String GET_SN_INFO_SUCCESS = "GET_SN_INFO_SUCC";

	/**
	 * Http Operate
	 */
	// public static final String HTTP_CONFIG_M_STRING =
	// "SET_HTTP_CFG URL=%1$s";

	public static final String SET_WLAN_SSID_DISABLE_STRING = "SET_WLAN_SSID_DISABLE";

	/**
	 * PON Regist State
	 */
	public static final String GET_PON_STATUS_STRING = "GET_PON_STATUS";

	public static final String GET_PON_STATUS_SUCCESS = "GET_PON_STATUS";

	public static final String GET_PON_STATUS_FAILED = "DEVICE_NOT_SUPPORT_REQ";

	/**
	 * 获取注册状�?成功后的三状态：未注册未授权，已注册未授权，已注册已授权
	 */
	public static final String RESULT_PON_STATUS_NO_REG_NO_AUTH = "PON_STATUS_NO_REG_NO_AUTH";

	public static final String RESULT_PON_STATUS_REG_NO_AUTH = "PON_STATUS_REG_NO_AUTH";

	public static final String RESULT_PON_STATUS_REG_AUTH = "PON_STATUS_REG_AUTH";

	/**
	 * PON Information State
	 */
	public static final String GET_PON_INFORMATION_STRING = "GET_PONINFORM_REQ";

	public static final String GET_PON_INFORMATION_SUCCESS = "GET_PONINFORM_REQ";

	public static final String GET_PON_INFORMATION_FAILED = "DEVICE_NOT_SUPPORT_REQ";

	/**
	 * @TEMPERATURE 表示温度
	 * @VOTTAGE 表示电压
	 * @CURRENT 表示电流
	 * @TXPOWER 表示发�?光功�? * @RXPOWE 接收光功�?
	 */

	public static final String GET_PON_INFORMATION_TEMPERATURE_ATTR = "TEMPERATURE";

	public static final String GET_PON_INFORMATION_VOTTAGE_ATTR = "VOTTAGE";

	public static final String GET_PON_INFORMATION_CURRENT_ATTR = "CURRENT";

	public static final String GET_PON_INFORMATION_TXPOWER_ATTR = "TXPOWER";

	public static final String GET_PON_INFORMATION_RXPOWE_ATTR = "RXPOWE";

	/**
	 * WAN Number Count
	 */

	public static final String QUERY_WAN_NUM_SUCCESS = "QUERY_WAN_NUM_SUCC";

	public static final String QUERY_WAN_NUM_FAILED = "QUERY_WAN_NUM_FAIL";

	/**
	 * @CONNECTIONTYPE 表示连接类型
	 * @SERVICELIST 表示接口描述
	 * @IPADDRESS 表示IP地址
	 */
	public static final String QUERY_WAN_INFO_NUM_CONNECTIONTYPE_ATTR = "CONNECTIONTYPE";

	public static final String QUERY_WAN_INFO_NUM_SERVICELIST_ATTR = "SERVICELIST";

	public static final String QUERY_WAN_INFO_NUM_IPADDRESS_ATTR = "IPADDRESS";
	/**
	 * Ping
	 */
	public static final String PING_DIAG_REQ_M_STRING = "PING_DIAG_REQ DEST=%1$s"; // 发起ping诊断

	public static final String PING_DIAG_REQ_SUCCESS = "PING_DIAG_REQ_SUCC";

	public static final String PING_DIAG_REQ_FAILED = "PING_DIAG_REQ_FAIL";

	public static final String GET_PING_RESULT_SUCCESS = "GET_PING_RESULT_SUCC";

	public static final String GET_PING_RESULT_FAILED = "GET_PING_RESULT_FAIL";

	/**
	 * 获取Ping诊断结果后的三种状�?：正在获取，不可达，完成可达
	 */
	public static final String RESULT_PING_RESULT_STATUS_REQUEST = "REQUEST";

	public static final String RESULT_PING_RESULT_STATUS_UNREACH = "UNREACH";

	public static final String RESULT_PING_RESULT_STATUS_COMPLETE = "COMPLETE";

	/**
	 * @STATUS 表示命令执行结果
	 * @RESULT 表示平均时延
	 */
	public static final String GET_PING_RESULT_STATUS_ATTR = "STATUS";

	public static final String GET_PING_RESULT_RESULT_ATTR = "RESULT";

	/**
	 * ITMS ONEKEY
	 */
	public static final String ITMS_DIAG_REQUEST_STRING = "ITMS_DIAG_REQUEST";

	public static final String ITMS_DIAG_REQUEST_SUCCESS = "ITMS_DIAG_REQUEST";

	public static final String ITMS_DIAG_REQUEST_IP_ATTR = "IP";

	public static final String ITMS_DIAG_REQUEST_REGSTATUS_ATTR = "REGSTATUS";

	public static final String ITMS_DIAG_REQUEST_ISBUSINESSDOWN_ATTR = "ISBUSINESSDOWN";

	public static final String RESULT_DIAG_REQUEST_REGSTATUS_DEFAULT = "ITMS_REG_DEFAULT";

	public static final String RESULT_DIAG_REQUEST_REGSTATUS_SUCCESS = "ITMS_REG_SUCCESS";

	public static final String RESULT_DIAG_REQUEST_REGSTATUS_FAIL = "ITMS_REG_FAIL";

	// /////// App Manager ///////// //
	/**
	 * HEARTBEAT
	 */
	public static final String OPT_SEND_HEARTBEAT_STRING = "OPT_SEND_HEARTBEAT";

	public static final String OPT_SEND_HEARTBEAT_SUCCESS = "OPT_SEND_HEARTBEAT_SUCC";

	public static final String OPT_SEND_HEARTBEAT_FAILED = "OPT_SEND_HEARTBEAT_FAIL";

	/**
	 * Validate
	 */
	public static final String OPT_TEST_USERADMIN_PASSWORD_M_STRING = "OPT_TEST_USERADMIN_PASSWORD=%1$s";

	public static final String OPT_TEST_USERADMIN_PASSWORD_SUCCESS = "OPT_TEST_USERADMIN_PASSWORD_SUCC";

	public static final String OPT_TEST_USERADMIN_PASSWORD_FAIL = "OPT_TEST_USERADMIN_PASSWORD_FAIL";

	public static final String OPT_TEST_USERADMIN_PASSWORD_FAILED = "OPT_ SEND_HEARTBEAT_FAIL";

	/**
	 * System Info
	 */
	public static final String QUERY_SYSTEM_INFO_STRING = "QUERY_SYSTEM_INFO";

	public static final String QUERY_SYSTEM_INFO_SUCCESS = "QUERY_SYSTEM_INFO_SUCC";

	public static final String QUERY_SYSTEM_INFO_FAILED = "QUERY_SYSTEM_INFO_FAIL";

	public static final String QUERY_SYSTEM_INFO_DEVTYPE_ATTR = "PRODUCTCLASS";
	public static final String QUERY_SYSTEM_INFO_CPUCLASS_ATTR = " CPUCLASS=";

	/**
	 * device update
	 */
	public static final String WG_DEVICE_UPDATE_STR = "UPGRADEALL";
	// public static final String WG_RECOVER_STR = "HG_LOCAL_REVOCERY"; // demo

	/**
	 * device recovery
	 */
	public static final String WG_RECOVER_STR = "HG_REVOCERY_PRECONFIG";
	// public static final String WG_RECOVER_STR = "HG_LOCAL_REVOCERY"; // demo

	public static final String QUERY_SYSTEM_INFO_SUCC = "QUERY_SYSTEM_INFO_SUCC";

	/**
	 * 网关开通状态
	 */
	// 设备注册状态
	public static final String REGISTER_GETSTAT = "REGISTER_GETSTAT";

	// 初始设备未注册状态
	public static final String REGISTER_DEFAULT = "REGISTER_DEFAULT";

	// 设备为已注册状态,不需要再次注册
	public static final String REGISTER_REGISTED = "REGISTER_REGISTED";

	// 注册超时失败
	public static final String REGISTER_TIMEOUT = "REGISTER_TIMEOUT";

	// 逻辑ID与密码不匹配
	public static final String REGISTER_NOMATCH_NOLIMITED = "REGISTER_NOMATCH_NOLIMITED";

	public static final String REGISTER_NOMATCH_LIMITED = "REGISTER_NOMATCH_LIMITED";

	// 密码不存在
	public static final String REGISTER_NOACCOUNT_NOLIMITED = "REGISTER_NOACCOUNT_NOLIMITED";

	public static final String REGISTER_NOACCOUNT_LIMITED = "REGISTER_NOACCOUNT_LIMITED";

	// 逻辑ID不存在
	public static final String REGISTER_NOUSER_NOLIMITED = "REGISTER_NOUSER_NOLIMITED";

	public static final String REGISTER_NOUSER_LIMITED = "REGISTER_NOUSER_LIMITED";

	// 正在注册OLT
	public static final String REGISTER_OLT_PROGRESS = "REGISTER_OLT PROGRESS";

	// 在OLT上注册失败
	public static final String REGISTER_OLT_FAIL = "REGISTER_OLT_FAIL";

	// 注册成功
	public static final String REGISTER_OK_DOWN_BUSINESS = "REGISTER_OK_DOWN_BUSINESS";

	// 注册成功,下发业务成功
	public static final String REGISTER_OK_SERVICENAME = "REGISTER_OK SERVICENAME";

	// 注册成功,下发业务成功,网关需要重启
	public static final String REGISTER_OK_NOW_REBOOT = "REGISTER_OK_NOW_REBOOT";

	// 注册成功，下发业务失败
	public static final String REGISTER_POK = "REGISTER_POK";

	// 注册失败
	public static final String REGISTER_FAIL = "REGISTER_FAIL";

	public static final String REGISTER_LOID_OK = "REGISTER_LOID_OK";

	public static final String REGISTER_ERROR = "REGISTER_ERROR";

	public static final String ERROR_TIMEOUT = "ERROR_TIMEOUT";

	public static final String DEVICE_UPDATE = "DEVICE_UPDATE";

	public static final String DEVICE_UPDATE_SUCCESS = "DEVICE_UPDATE_SUCCESS";

	public static final String GET_VOIP_REG_STATUS = "GET_VOIP_REG_STATUS";

	public static final String GET_VOIP_REG_STATUS_FAIL = "GET_VOIP_REG_STATUS_FAIL";

	public static final String GET_VOIP_REG_STATUS_SUCC = "GET_VOIP_REG_STATUS_SUCC";

	public static final String GET_SN_INFO = "GET_SN_INFO";

	public static final String GET_SN_INFO_FAIL = "GET_SN_INFO_FAIL";

	public static final String GET_SN_INFO_SUCC = "GET_SN_INFO_SUCC";

	/**
	 * json格式数据 消息状态：成功
	 */
	public static final String SUCCESS = "0";
	/**
	 * json格式数据 消息状态：失败
	 */
	public static final String FAIL = "1";

	/**
	 * CmdType 1.4 设置WIFI连接信息
	 */
	public static final String SET_WIFI_INFO = "SET_WIFI_INFO";

	/**
	 * CmdType 1.11.2 管理员密码校验
	 */
	public static final String CHECK_ADMIN_PASSWORD = "CHECK_ADMIN_PASSWORD";

	/**
	 * CmdType 7.22发起设备逻辑ID注册
	 */
	public static final String REGISTER = "REGISTER";

	/**
	 * CmdType[REGISTER] RegisterResult
	 */
	public static final String REGISTER_OLT = "REGISTER_OLT";
	public static final String REGISTER_OK = "REGISTER_OK";

	/**
	 * CmdType 7.15 查看VOIP信息
	 */
	public static final String GET_VOIP_INFO = "GET_VOIP_INFO";

	/**
	 * CmdType 7.7 获取物理接口连接状态
	 */
	public static final String SYSTEST = "SYSTEST";

	/**
	 * CmdType 7.4查询系统信息
	 */
	public static final String QUERY_SYSTEM_INFO = "QUERY_SYSTEM_INFO";

	/**
	 * CmdType 7.21发起Inform诊断
	 */
	public static final String INFORM_DIAG_REQ = "INFORM_DIAG_REQ";

	/**
	 * CmdType 7.8查询PPP拨号状态
	 */
	public static final String PPPOE_DIAG_REQ = "PPPOE_DIAG_REQ";

	/**
	 * CmdType 7.5查询CPU占用率
	 */
	public static final String QUERY_CPU_INFO = "QUERY_CPU_INFO";

	/**
	 * CmdType 7.6查询内存占用率
	 */
	public static final String QUERY_MEM_INFO = "QUERY_MEM_INFO";

	/**
	 * CmdType 7.9查询WAN连接的条数
	 */
	public static final String QUERY_WAN_NUM = "QUERY_WAN_NUM";

	/**
	 * CmdType 7.20查询Traceroute诊断结果
	 */
	public static final String GET_TRACEROUTE_RESULT = "GET_TRACEROUTE_RESULT";

	/**
	 * CmdType 7.18查询Ping诊断结果
	 */
	public static final String GET_PING_RESULT = "GET_PING_RESULT";

	/**
	 * CmdType 7.25查询PON口的注册状态
	 */
	public static final String GET_PON_STATUS = "GET_PON_STATUS";

	/**
	 * CmdType 7.24PON口状态查询
	 */
	public static final String GET_PONINFORM_REQ = "GET_PONINFORM_REQ";

	/**
	 * CmdType 7.28获取WLAN接口信息
	 */
	public static final String GET_WLAN_INTF_INFO = "GET_WLAN_INTF_INFO";

	/**
	 * CmdType 7.33获取持续时间（系统时间、PPPOE持续时间、PON持续时间）
	 */
	public static final String GET_TIME_DURATION = "GET_TIME_DURATION";

	/**
	 * CmdType 7.34设备快照合并消息
	 */
	public static final String GET_DEVICE_INFO_ALL = "GET_DEVICE_INFO_ALL";

	/**
	 * Get Quickinstall Versio GET_VERSION 无 GET_VERSION VERSION=%S
	 * VERSION=%S表示当前版本的版本号
	 */
	public static final String GET_QUICKINSTALL_VERSION_STRING = "GET_VERSION";

	/**
	 * Http Operate
	 */

	/**
	 * WAN Number Count
	 */
	public static final String QUERY_WAN_NUM_STRING = "QUERY_WAN_NUM";

	public static final String PING_DIAG_REQ_STRING = "PING_DIAG_REQ"; // 发起ping诊断

	public static final String PING_DIAG_REQ_NO_ROUTE = "PING_DIAG_REQ_NO_ROUTE"; // 表示无有效WAN连接信息

	public static final String GET_PING_RESULT_STRING = "GET_PING_RESULT"; // 获取ping诊断结果

	// /////// App Manager ///////// //

	public static final String HG_LOCAL_REVOCERY = "HG_LOCAL_REVOCERY";

	public static final String HG_COMMAND_REBOOT = "HG_COMMAND_REBOOT";

	public static final String CHECK_PASSWD_PARAM = "CHECK_PASSWD_PARAM";

	/**
	 * LED
	 */
	public static final String GET_LED_STATUS = "GET_LED_STATUS";
	public static final String SET_LED_STATUS = "SET_LED_STATUS";

	public static final String SET_ROUTE_WAN_INFO = "SET_ROUTE_WAN_INFO";

	/**
	 * 上联 设置
	 */
	public static final String GET_UPLINK_MODE = "GET_UPLINK_MODE";
	public static final String SET_UPLINK_MODE = "SET_UPLINK_MODE";
	public static final String SET_WIFI_MODE = "SET_WIFI_MODE";

	public static final String QUERY_WIFI_NUM = "QUERY_WIFI_NUM";
	public static final String GET_WIFI_SSIDLIST = "GET_WIFI_SSIDLIST";
	public static final String GET_CLIENT_WIFI_INFO = "GET_CLIENT_WIFI_INFO";
	public static final String HG_WPSBUTTON_PUSH = "HG_WPSBUTTON_PUSH";
	/**
	 * wifi模式
	 */
	// public static final String GET_WIFI_POWERLEVEL = "GET_WIFI_POWERLEVEL";
	// public static final String SET_WIFI_POWERLEVEL = "SET_WIFI_POWERLEVEL";
	/**
	 * qos模式
	 */
	public static final String GET_INTERNET_QOS = "GET_INTERNET_QOS";
	public static final String SET_INTERNET_QOS = "SET_INTERNET_QOS";
	/**
	 * 绿色上网
	 */
	public static final String GET_ATTACH_INFO = "GET_ATTACH_INFO";
	public static final String SET_ATTACH_DEVICE_RIGHT = "SET_ATTACH_DEVICE_RIGHT";
	public static final String SET_DEVICE_NAME = "SET_DEVICE_NAME";
	public static final String GET_ATTACH_DEVICE_RIGHT = "GET_ATTACH_DEVICE_RIGHT";

	public static final String GET_WIFI_INFO = "GET_WIFI_INFO";

	public static final String GET_ADMIN_PASSWORD = "GET_ADMIN_PASSWORD";

	/* 2015 */
	/* 网络诊断功能 */
	/*
	 * 网络诊断功能 网络诊断功能分为四个步骤，按照执行顺序分别是，1）物理连接诊断，2）WAN连接诊断，3）路由诊断，4）DNS诊断；
	 * 在执行步骤1）物理连接诊断的时候
	 * ，只有返回值为1或者2时才继续进行下一步诊断；在执行步骤2）WAN连接诊断的时候，只有返回值为1或者2时才继续进行下一步诊断
	 * ；在执行步骤3）路由诊断的时候，只有返回值为0时才继续进行下一步诊断
	 */
	/** 物理连接诊断 */
	public static final String GET_PHYLINK_RESULT = "GET_PHYLINK_RESULT";
	/** 1.30.2 WAN连接诊断 */
	public static final String GET_WANCONN_RESULT = "GET_WANCONN_RESULT";
	/** 1.30.3 路由诊断 */
	public static final String GET_ROUTEDIAG_RESULT = "GET_ROUTEDIAG_RESULT";
	/** 1.30.4 DNS诊断 */
	public static final String GET_DNSDIAG_RESULT = "GET_DNSDIAG_RESULT";
	/* 关联设备 */
	public static final String GET_DEVICE_TYPE = "GET_DEVICE_TYPE";
	/* 网络测速 */
	/* 接口 1.1配置测速下载地址 3.1.2满负载下载控制 3.1.3 获取WAN口接收字节数 */
	/** 1.1配置测速下载地址 */
	public static final String SET_HTTP_CFG = "SET_HTTP_CFG";
	/** 3.1.2满负载下载控制 */
	public static final String HTTP_DOWNLOAD_REQUEST = "HTTP_DOWNLOAD_REQUEST";
	/** 3.1.3 获取WAN口接收字节数 */
	public static final String GET_REALTIME_RATE = "GET_REALTIME_RATE";

	/* 防蹭网 */
	public static final String GET_WLAN_ATTACH_INFO = "GET_WLAN_ATTACH_INFO";

	/* 上网向导 */
	public static final String QUERY_WAN_INFO = "QUERY_WAN_INFO";
}
