package com.fiberhome.opticalbox.net;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Environment;
import android.text.TextUtils;

/**
 * @author wangbin
 * 
 */
public class Constants {

	public static final String USER_SHAREDPREFERENCE = "userInfo";
	public static final String SETTINGS_SHAREDPREFERENCE = "appstore_icon_settings";

	public static final String SDCARD = Environment.getExternalStorageDirectory() + "/";

	public static final String SDCARD_SOFT_ADD = Environment.getExternalStorageDirectory() + "/market/soft";

	/**
	 * 主页资源URL
	 */
	public static final String SERVER_URL_RES = "/smartHN/view/resAppView.action";
	/**
	 * 主页全部资源URL
	 */
	public static final String SERVER_URL_RES_ALL = "/smartHN/view/resAllInfo";
	/**
	 * 应用分类URL
	 */
	public static final String SERVER_URL_RES_TYPE = "/smartHN/view/resInfoView";

	/**
	 * 关键字搜索URL
	 */
	public static final String SERVER_URL_RES_SEARCH = "/smartHN/view/resByKeyView";
	/**
	 * 网关软件升级URL
	 */
	public static final String SERVER_URL_RES_UPDATE_WG = "/smartHN/phone/wgsoft";
	/**
	 * 手机应用升级URL
	 */
	public static final String SERVER_URL_RES_UPDATE_PHONE = "/smartHN/phone/appsoft";

	// public static final String USER_SERVER_URL_LOCAL =
	// "http://10.96.16.16:80/smartHN/regist/userreg.action";
	// public static final String USER_LOGIN_SERVER_URL_LOCAL =
	// "http://10.96.16.16/smartHN/phone/phonelogin";
	//
	// /**
	// * 手机app升级URL
	// */
	// public static final String UPDATE_URL_PATH =
	// "http://10.96.16.16/smartHN/upload/update/xml/gateway-gmzs.xml";
	//
	// /**
	// * 用户反馈功能URL
	// */
	// public static final String URL_PATH =
	// "http://10.96.16.16:80/smartHN/phone/reply.action";
	//
	// /**
	// * 用户反馈信息查询URL
	// */
	// public static final String URL_PATH_LIST =
	// "http://10.96.16.16:80/smartHN/phone/query.action";

	/**
	 * 最大分页
	 */
	public static final int MAX_SIZE_PER_PAGE = 10;

	/**
	 * 开始
	 */
	public static final int DOWNLOAD_START = 1;
	/**
	 * 下载中
	 */
	public static final int DOWNLOAD_DOWNLOAD = 2;
	/**
	 * 暂停
	 */
	public static final int DOWNLOAD_SPACE = 3;
	/**
	 * 继续
	 */
	public static final int DOWNLOAD_GO = 4;
	/**
	 * 取消
	 */
	public static final int DOWNLOAD_CANCEL = 5;
	/**
	 * 出错
	 */
	public static final int DOWNLOAD_ERROR = 6;
	/**
	 * 下载成功
	 */
	public static final int DOWNLOAD_SUCCESS = 7;
	/**
	 * 服务器固障
	 */
	public static final int DOWNLOAD_ERROR_SERVER = 8;

	/**
	 * 升级出错
	 */
	public static final int UPDATE_ERROR = -1;
	/**
	 * 升级开始
	 */
	public static final int UPDATE_START = 1;
	/**
	 * 升级中
	 */
	public static final int UPDATE_DOWNLOAD = 2;
	/**
	 * 升级暂停
	 */
	public static final int UPDATE_SPACE = 3;
	/**
	 * 升级继续
	 */
	public static final int UPDATE_GO = 4;
	/**
	 * 升级成功
	 */
	public static final int UPDATE_SUCCESS = 5;

	/**
	 * 升级取消
	 */
	public static final int UPDATE_CANCEL = 6;

	/**
	 * 消息开始
	 */
	public static final int FLAG_NOTIFICATION_START = 1;
	/**
	 * 消息暂停
	 */
	public static final int FLAG_NOTIFICATION_PAUSE = 2;
	/**
	 * 消息取消
	 */
	public static final int FLAG_NOTIFICATION_CANCEL = 3;
	/**
	 * 消息更新
	 */
	public static final int FLAG_NOTIFICATION_UPDATE = 4;
	/**
	 * 消息升级成功
	 */
	public static final int FLAG_NOTIFICATION_UPDATE_SUCCESS = 5;
	/**
	 * 消息提示升级失败
	 */
	public static final int FLAG_NOTIFICATION_UPDATE_ERROR = -1;

	public static final String DOWNLOAD_PROGRESS_ACTION = "fh.gateway.action.download.progress";
	public static final String DOWNLOAD_PROGRESS_PAUSE_ACTION = "fh.gateway.action.download.progress.pause";
	public static final String DOWNLOAD_PROGRESS_GO_ACTION = "fh.gateway.action.download.progress.go";
	public static final String DOWNLOAD_PROGRESS_CANCEL_ACTION = "fh.gateway.action.download.progress.cancel";

	public static final String LOADING_NEWS_ACTION = "fh.gateway.action.loading.news";
	public static final String LOADING_HOTS_ACTION = "fh.gateway.action.loading.hots";
	public static final String LOADING_ALL_ACTION = "fh.gateway.action.loading.all";

	public static final String LOADING_MANAGE_DOWNLOAD = "fh.gateway.action.loading.download";
	public static final String LOADING_MANAGE_INSTALLED = "fh.gateway.action.loading.installed";
	public static final String LOADING_MANAGE_UPDATE = "fh.gateway.action.loading.update";

	/**
	 * 手机应用
	 */
	public static final int PHONE_APP_TYPE = 1;
	/**
	 * HGSystemFirmware 网关固件
	 */
	public static final int GATEWAY_HG_TYPE = 2;
	/**
	 * HGApplication 网关应用程序
	 */
	public static final int GATEWAY_APP_TYPE = 3;
	/**
	 * HGMediaFile 网关媒体程序
	 */
	public static final int GATEWAY_MEDIA_TYPE = 4;

	/**
	 * 正在连接
	 */
	public static final int GATEWAY_STATUS_BEFORE_DOWNLOAD = -1;

	/**
	 * 下载中
	 */
	public static final int GATEWAY_STATUS_DOWNLOADING = 1;
	/**
	 * 下载完成
	 */
	public static final int GATEWAY_STATUS_DOWNLOAD_SUCCESS = 2;
	/**
	 * 下载完成验证中
	 */
	public static final int GATEWAY_STATUS_IDENTITY = 3;
	/**
	 * 验证完成安装中
	 */
	public static final int GATEWAY_STATUS_INSTALL = 4;
	/**
	 * 安装完成
	 */
	public static final int GATEWAY_STATUS_INSTALL_SUCCESS = 5;
	/**
	 * 下载失败
	 */
	public static final int GATEWAY_STATUS_DOWNLOAD_FAIL = 6;
	/**
	 * 验证失败
	 */
	public static final int GATEWAY_STATUS_IDENTIFY_FAIL = 7;
	/**
	 * 安装失败
	 */
	public static final int GATEWAY_STATUS_INSTALL_FAIL = 8;

	/**
	 * 连接超时
	 */
	public static final int GATEWAY_STATUS_TIME_OUT = 9;

	/**
	 * 同时下载线程数
	 */
	public static final int MULTITHREADNUM = 3;

	/**
	 * 网关下载应用开始
	 */
	public static final String GATEWAY_DOWNLOAD_QUERY = "OPT_DOWNLOAD NAME=%s TYPE=%s VERSION=%s URL=%s";

	/**
	 * 网关下载状态查询
	 */
	public static final String GATEWAY_DOWNLOAD_LISTENER_QUERY = "OPT_DOWNLOAD_STATUS_QUERY NAME=%s VERSION=%s TYPE=%s";

	public static final String TYPE_GATEWAY_FIRMWARE = "HGSystemFirmware";

	public static final String GATEWAY_UPLOAD_URL = "http://smarthomenet.com.cn/phone/opReport";

	public static final String MESSAGE = "message";

	public static final String STATE = "succ";

	public static final String AREA_NAME = "areaName";

	public static final String ALL_BAND = "allband";

	public static final String PROVINCE_NAME = "provinceName";

	public static final String USER_BAND = "userBand";

	/**
	 * upload speed info
	 */
	public static final String AVG_SPEED = "avgSpeed";

	public static final String MAX_SPEED = "maxSpeed";

	public static final String ALL_REALTIMESPEED = "allRealTimeSpeeds";

	public static final String ALL_AVG_REALTIMESPEED = "allAvgSpeeds";

	public static final String SHOW_SPEED = "showSpeed";

	public static final String IS_SUCCESS = "isSuccess";

	public static final String TEST_SOURCE = "testsource";

	public static final String LOCATION = "location";

	public static final String TARGET = "target";

	public static final String METHOD = "method";

	public static final String TYPE_DOWNLOAD = "SaveDownTestDateforah";

	public static final int TYPE_TARGET_DOWNLOAD = 1;

	public static final int TYPE_TARGET_WEB = 2;

	public static final int TYPE_TARGET_MEDIA_STREAM = 3;

	private static int id = 0;

	private static int optype = 0;

	private static String username;

	public static int msgNum = 1;

	private static String bgImg;

	public static String USER;

	public static String PASSWORD;

	public static String getBgImg() {
		return bgImg;
	}

	public static void setBgImg(String bgImg) {
		Constants.bgImg = bgImg;
	}

	public static int getId(Context context) {
		if (id == 0) {
			SharedPreferences sp = context.getSharedPreferences("user", Context.MODE_PRIVATE);
			id = sp.getInt("id", 0);
		}
		return id;
	}

	public static int getOptype(Context context) {
		if (optype == 0) {
			SharedPreferences sp = context.getSharedPreferences("user", Context.MODE_PRIVATE);
			optype = sp.getInt("optype", 0);
		}
		return optype;
	}

	public static String getUsername(Context context) {
		if (TextUtils.isEmpty(username)) {
			SharedPreferences sp = context.getSharedPreferences("user", Context.MODE_PRIVATE);
			username = sp.getString("username", "");
		}
		return username;
	}

	public static void setUsername(String username) {
		Constants.username = username;
	}

	public static void setId(int id) {
		Constants.id = id;
	}

	public static void setOptype(int optype) {
		Constants.optype = optype;
	}

	public static void logOut(Context context) {
		SharedPreferences sp = context.getSharedPreferences("user", Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.remove("id").remove("optype").remove("username");
		editor.commit();
		id = 0;
		optype = 0;
		username = "";
	}

}
