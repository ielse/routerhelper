package com.fiberhome.opticalbox.net.utils;

import java.util.Random;

public class SequenceIdGenerator {
	static SequenceIdGenerator generator;

	int sequenceId;

	private static final int MAX_VALUE = 1000000;

	public static SequenceIdGenerator getInstance() {
		if (generator == null) {
			generator = new SequenceIdGenerator();
		}
		return generator;
	}

	// 在1-1000000间随机生成
	private SequenceIdGenerator() {
		Random random = new Random();
		sequenceId = random.nextInt(MAX_VALUE);
	}

	public String next() {
		return Integer.toHexString(sequenceId++);
	}
}
