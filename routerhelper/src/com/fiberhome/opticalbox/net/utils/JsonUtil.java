package com.fiberhome.opticalbox.net.utils;

import java.util.ArrayList;
import java.util.List;

import com.fiberhome.opticalbox.net.bean.Parameter;
import com.fiberhome.opticalbox.net.bean.RequestBean;
import com.fiberhome.opticalbox.net.bean.data.DeviceRight;
import com.fiberhome.opticalbox.net.bean.data.DeviceTime;
import com.fiberhome.opticalbox.net.bean.data.UrlInfo;
import com.google.gson.Gson;

public class JsonUtil {

	public static byte[] get_DEVICE_INFO_ALL(String user, String psw) {
		// 网关快照
		Gson g = new Gson();
		Parameter p = new Parameter("GET_DEVICE_INFO_ALL", SequenceIdGenerator.getInstance().next());
		p.USER = user;
		p.PASSWORD = psw;
		RequestBean bean = new RequestBean("Post1", "DeviceRunTimeStatus", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_PON_STATUS_STRING() {
		// Pon口注册状态信息查询
		Gson g = new Gson();
		Parameter p = new Parameter("GET_PON_STATUS", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_PON_INFORMATION_STRINGG() {
		// Pon口状态信息查询
		Gson g = new Gson();
		Parameter p = new Parameter("GET_PONINFORM_REQ", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_ITMS_DIAG_REQUEST_STRING() {
		// ITMS 查询
		Gson g = new Gson();
		Parameter p = new Parameter("ITMS_DIAG_REQUEST", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_QUERY_WAN_NUM() {
		// Internet WAN连接获取IP地址查询
		Gson g = new Gson();
		Parameter p = new Parameter("QUERY_WAN_NUM", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_QUERY_WAN_INFO(String WANName) {
		// 查询某一条WAN连接的信息
		Gson g = new Gson();
		Parameter p = new Parameter("QUERY_WAN_INFO", SequenceIdGenerator.getInstance().next());
		p.WANName = WANName;
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_PING_RESULT(String USER, String PASSWORD) {
		// 发起Ping诊断
		Gson g = new Gson();
		Parameter p = new Parameter("GET_PING_RESULT", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		p.USER = USER;
		p.PASSWORD = PASSWORD;

		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_QUERY_SYSTEM_INFO() {
		// 查询系统信息
		Gson g = new Gson();
		Parameter p = new Parameter("QUERY_SYSTEM_INFO", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_GET_DEVICE_INFO_ALL() {
		// 查询系统信息
		Gson g = new Gson();
		Parameter p = new Parameter("GET_DEVICE_INFO_ALL", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_LAN_NET_INFO() {
		// 获取网络拓扑图
		Gson g = new Gson();
		Parameter p = new Parameter("GET_LAN_NET_INFO", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_SN_INFO() {
		// 查询设备的SN号
		Gson g = new Gson();
		Parameter p = new Parameter("GET_SN_INFO", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_HG_LOID() {
		// 获取LOID
		Gson g = new Gson();
		Parameter p = new Parameter("GET_HG_LOID", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_TIME_DURATION() {
		// 获取持续时间
		Gson g = new Gson();
		Parameter p = new Parameter("GET_TIME_DURATION", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_QUERY_CPU_INFO() {
		// 查询CPU占用率
		Gson g = new Gson();
		Parameter p = new Parameter("QUERY_CPU_INFO", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_QUERY_MEM_INFO() {
		// 查询内存占用率
		Gson g = new Gson();
		Parameter p = new Parameter("QUERY_MEM_INFO", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_SET_HG_WIFI_TIMER(String startTime, String endTime) {
		// Wifi定时开关
		Gson g = new Gson();
		Parameter p = new Parameter("SET_HG_WIFI_TIMER", SequenceIdGenerator.getInstance().next());
		p.StartTime = startTime;
		p.EndTime = endTime;
		p.ControlCycle = "DAY";
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_SET_HTTP_CFG(String USER, String PASSWORD, String url) {
		// 配置测速HTTP下载地址
		Gson g = new Gson();
		Parameter p = new Parameter("SET_HTTP_CFG", SequenceIdGenerator.getInstance().next());
		p.URL = url;
		p.USER = USER;
		p.PASSWORD = PASSWORD;
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_QUERY_WAN_REALRATE(String USER, String PASSWORD) {
		// 获取路由模式下WAN连接的实时速率
		Gson g = new Gson();
		Parameter p = new Parameter("QUERY_WAN_REALRATE", SequenceIdGenerator.getInstance().next());
		p.USER = USER;
		p.PASSWORD = PASSWORD;
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_GET_REALTIME_RATE(String USER, String PASSWORD) {
		// 获取路由模式下WAN连接的实时速率
		Gson g = new Gson();
		Parameter p = new Parameter("GET_REALTIME_RATE", SequenceIdGenerator.getInstance().next());
		p.USER = USER;
		p.PASSWORD = PASSWORD;
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] startHttpDownloaRequest(String user, String password, String method, String time) {
		// 控制HTTP下载操作
		Gson g = new Gson();
		Parameter p = new Parameter("HTTP_DOWNLOAD_REQUEST", SequenceIdGenerator.getInstance().next());
		p.USER = user;
		p.PASSWORD = password;
		p.Method = method;
		p.Time = time;

		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);
		return g.toJson(bean).getBytes();
	}

	public static byte[] get_HTTP_DOWNLOAD_REQUEST() {
		// 控制HTTP下载操作
		Gson g = new Gson();
		Parameter p = new Parameter("HTTP_DOWNLOAD_REQUEST", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	// 查询PPP拨号状态
	public static String getPPPOEDiagReq() {
		Gson g = new Gson();
		Parameter p = new Parameter("PPPOE_DIAG_REQ", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		return g.toJson(bean);
	}

	// 发起Traceroute诊断
	public static String getTraceroute(String dest, String WANName) {
		Gson g = new Gson();
		Parameter p = new Parameter("TRACEROUTE_DIAG_REQ", SequenceIdGenerator.getInstance().next());
		p.Dest = dest;
		p.WANName = WANName;
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		return g.toJson(bean);
	}

	// // 发起Traceroute诊断
	// public static String getTraceroute(String user, String password, String
	// dest, String WANName) {
	// Gson g = new Gson();
	// Parameter p = new Parameter("TRACEROUTE_DIAG_REQ",
	// SequenceIdGenerator.getInstance().next());
	// p.USER = user;
	// p.PASSWORD = password;
	// p.Dest = dest;
	// p.WANName = WANName;
	// RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);
	// return g.toJson(bean);
	// }
	// 查询Traceroute诊断结果
	public static String getTracerouteResult(String user, String password) {
		Gson g = new Gson();
		Parameter p = new Parameter("GET_TRACEROUTE_RESULT", SequenceIdGenerator.getInstance().next());
		p.USER = user;
		p.PASSWORD = user;
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		return g.toJson(bean);
	}

	// 发起WAN侧Ping诊断
	public static String getWanPing(String user, String password, String WANName, String Dest) {
		Gson g = new Gson();
		Parameter p = new Parameter("PING_DIAG_REQ", SequenceIdGenerator.getInstance().next());
		p.USER = user;
		p.PASSWORD = password;
		p.WANName = WANName;
		p.Dest = Dest;
		p.leng = 64 + "";
		p.time = 3 + "";
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		return g.toJson(bean);
	}

	// 查询Ping诊断结果
	public static String getPingResult(String user, String password) {
		Gson g = new Gson();
		Parameter p = new Parameter("GET_PING_RESULT", SequenceIdGenerator.getInstance().next());
		p.USER = user;
		p.PASSWORD = password;
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		return g.toJson(bean);
	}

	// 发起Inform诊断
	public static String getInformTrace(String user, String password) {
		Gson g = new Gson();
		Parameter p = new Parameter("INFORM_DIAG_REQ", SequenceIdGenerator.getInstance().next());
		p.USER = user;
		p.PASSWORD = password;
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		return g.toJson(bean);
	}

	// 发起设备逻辑ID注册
	public static String getLOIDRegister(String user, String password) {
		Gson g = new Gson();
		Parameter p = new Parameter("REGISTER", SequenceIdGenerator.getInstance().next());
		p.USER = user;
		p.PASSWORD = password;
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		return g.toJson(bean);
	}

	// 设备注册状态获取
	public static String getRegisterStatus() {
		Gson g = new Gson();
		Parameter p = new Parameter("REGISTER_GETSTAT", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		return g.toJson(bean);
	}

	public static byte[] get_SYSTEST() {
		// 获取物理接口连接状态
		Gson g = new Gson();
		Parameter p = new Parameter("SYSTEST", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	// 关闭SSID
	public static String getWlanSsidDisable(int index) {
		Gson g = new Gson();
		Parameter p = new Parameter("SET_WLAN_SSID_DISABLE", SequenceIdGenerator.getInstance().next());
		p.SSIDIndex = index + "";
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);
		return g.toJson(bean);
	}

	// 验证Useradmin或Telecomadmin密码是否正确
	public static byte[] getCheckPasswdParam(String pasword) {
		Gson g = new Gson();
		Parameter p = new Parameter("CHECK_PASSWD_PARAM", SequenceIdGenerator.getInstance().next());
		p.PassWD = pasword;
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);
		return g.toJson(bean).getBytes();
	}

	public static byte[] set_HG_LOCAL_REVOCERY() {
		// 网关恢复非关键参数
		Gson g = new Gson();
		Parameter p = new Parameter("HG_LOCAL_REVOCERY", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] set_HG_COMMAND_REBOOT() {
		// 网关重启
		Gson g = new Gson();
		Parameter p = new Parameter("HG_COMMAND_REBOOT", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_PPPOE_DIAG_REQ() {
		// 查询PPP拨号状态
		Gson g = new Gson();
		Parameter p = new Parameter("PPPOE_DIAG_REQ", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_VOIP_INFO() {
		// 查看VOIP信息
		Gson g = new Gson();
		Parameter p = new Parameter("GET_VOIP_INFO", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_VOIP_REG_STATUS() {
		// 获取VOIP信息注册状态
		Gson g = new Gson();
		Parameter p = new Parameter("GET_VOIP_REG_STATUS", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_GET_GN_ACCESS_URL_LIST() {
		// 获取黑白名单
		Gson g = new Gson();
		Parameter p = new Parameter("GET_GN_ACCESS_URL_LIST", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_SET_GN_ACCESS_URL_LIST(ArrayList<UrlInfo> WhiteAccessContents, int type) {
		// 设置黑白名单
		Gson g = new Gson();
		Parameter p = new Parameter("SET_GN_ACCESS_URL_LIST", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		// Gson gson = new Gson();
		// String str = gson.toJson(WhiteAccessContents);
		if (type == 0) {// 白名单设置
			p.setType("White");
		} else if (type == 1) {// 黑名单设置
			p.setType("Black");
		}
		p.Method = "Put";// Method为 Put表示设置或修改规则 Remove表示删除规则
		p.Urls = WhiteAccessContents;
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_GET_GN_ATTACH_DEVICE_RIGHT() {
		// 获取访问权限
		Gson g = new Gson();
		Parameter p = new Parameter("GET_GN_ATTACH_DEVICE_RIGHT", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_SET_GN_ATTACH_DEVICE_RIGHT(List<DeviceRight> deviceright) {
		// 设置访问权限
		Gson g = new Gson();
		Parameter p = new Parameter("SET_GN_ATTACH_DEVICE_RIGHT", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		Gson gson = new Gson();
		String str = gson.toJson(deviceright);
		p.AccessRights = str;

		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_GET_GN_TIMERANGE_RIGHT() {
		// 获取访问时段
		Gson g = new Gson();
		Parameter p = new Parameter("GET_GN_TIMERANGE_RIGHT", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_SET_GN_TIMERANGE_RIGHT(List<DeviceTime> AttachDeviceContents) {
		// 获取访问时段
		Gson g = new Gson();
		Parameter p = new Parameter("SET_GN_TIMERANGE_RIGHT", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		Gson gson = new Gson();
		String str = gson.toJson(AttachDeviceContents);
		p.DevTimeRange = str;

		String s = g.toJson(bean);
		return s.getBytes();
	}

	/** 1.4设置WIFI连接信息 2014-07-21 */
	/*
	 * public static byte[] get_SET_WIFI_INFO(String WifiIndex, String
	 * SSIDIndex, String SSID, String PWD, String ENCRYPT, // String PowerLevel,
	 * String Channel, String Enable) { Gson g = new Gson(); Parameter p = new
	 * Parameter("SET_WIFI_INFO", SequenceIdGenerator .getInstance().next());
	 * RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);
	 * 
	 * p.WifiIndex = WifiIndex; p.SSIDIndex = SSIDIndex; p.SSID = SSID; p.PWD =
	 * PWD; p.ENCRYPT = ENCRYPT; p.PowerLevel = "100"; // RPCMethod == post
	 * 
	 * p.Channel = Channel; p.Enable = Enable;
	 * 
	 * return g.toJson(bean).getBytes(); }
	 */

	public static byte[] get_SET_WIFI_INFO(String WifiIndex, String SSIDIndex, String SSID, String PWD, String ENCRYPT, String PowerLevel,
			String Channel, String Enable, String WifiCmdMode, String Hidden) {
		Gson g = new Gson();
		Parameter p = new Parameter("SET_WIFI_INFO", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		p.WifiIndex = WifiIndex;
		p.SSIDIndex = SSIDIndex;
		p.SSID = SSID;
		p.PWD = PWD;
		p.ENCRYPT = ENCRYPT;
		p.PowerLevel = PowerLevel;

		p.Channel = Channel;
		p.Enable = Enable;
		p.WiFiCmdMode = WifiCmdMode;
		p.Hidden = Hidden;

		return g.toJson(bean).getBytes();
	}

	public static byte[] get_SET_WIFI_INFO(Parameter param) {
		Gson g = new Gson();
		Parameter p = new Parameter("SET_WIFI_INFO", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		p.WifiIndex = param.WifiIndex;
		p.SSIDIndex = param.SSIDIndex;
		p.SSID = param.SSID;
		p.PWD = param.PWD;
		p.ENCRYPT = param.ENCRYPT;
		p.PowerLevel = param.PowerLevel;

		p.Channel = param.Channel;
		p.Enable = param.Enable;
		p.WiFiCmdMode = param.WiFiCmdMode;

		return g.toJson(bean).getBytes();
	}

	/** 1.11.2 管理员密码校验 */
	public static byte[] get_CHECK_ADMIN_PASSWORD(String USERNAME, String PASSWORD) {
		Gson g = new Gson();
		Parameter p = new Parameter("CHECK_ADMIN_PASSWORD", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		p.USERNAME = USERNAME;
		p.PASSWORD = PASSWORD;

		return g.toJson(bean).getBytes();
	}

	public static byte[] get_GET_ADMIN_PASSWORD() {
		Gson g = new Gson();
		Parameter p = new Parameter("GET_ADMIN_PASSWORD", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		return g.toJson(bean).getBytes();
	}

	// 设备快照合并消息
	public static byte[] getDeviceInfo(String user, String password) {
		Gson g = new Gson();
		Parameter p = new Parameter("GET_DEVICE_INFO_ALL", SequenceIdGenerator.getInstance().next());
		p.USER = user;
		p.PASSWORD = password;
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	/** 7.14获取SSID的下挂设备信息 */
	public static byte[] get_GET_WLAN_ATTACH_INFO() {
		Gson g = new Gson();
		Parameter p = new Parameter("GET_WLAN_ATTACH_INFO", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		return g.toJson(bean).getBytes();
	}

	/** 7.28获取WLAN接口信息 */
	public static byte[] get_GET_WLAN_INTF_INFO() {
		Gson g = new Gson();
		Parameter p = new Parameter("GET_WLAN_INTF_INFO", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		return g.toJson(bean).getBytes();
	}

	/** 7.33获取持续时间（系统时间、PPPOE持续时间、PON持续时间） */
	public static byte[] get_GET_TIME_DURATION() {
		Gson g = new Gson();
		Parameter p = new Parameter("GET_TIME_DURATION", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		return g.toJson(bean).getBytes();
	}

	public static byte[] get_SET_ROUTE_WAN_INFO(Parameter param) {
		Gson g = new Gson();
		Parameter p = new Parameter("SET_ROUTE_WAN_INFO", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		p.WANName = param.WANName;
		p.SERVICELIST = param.SERVICELIST;
		p.ADDRESSTYPE = param.ADDRESSTYPE;
		p.IPADDRESS = param.IPADDRESS;
		p.SUBNETMASK = param.SUBNETMASK;
		p.GATEWAY = param.GATEWAY;
		p.DNS1 = param.DNS1;
		p.DNS2 = param.DNS2;
		p.IPV6_IPADDRESS = param.IPV6_IPADDRESS;
		p.IPV6_PREFIXLENGTH = param.IPV6_PREFIXLENGTH;
		p.IPV6_GATEWAY = param.IPV6_GATEWAY;
		p.IPV6_DNS1 = param.IPV6_DNS1;
		p.IPV6_DNS2 = param.IPV6_DNS2;
		p.IPV6_PRIFIX = param.IPV6_PRIFIX;
		p.PPPOE_USR_NAME = param.PPPOE_USR_NAME;
		p.PPPOE_PASSWORD = param.PPPOE_PASSWORD;
		p.NATENABLE = param.NATENABLE;
		p.QOSENABLE = param.QOSENABLE;
		p.VLANID = param.VLANID;
		p.VLANPRIORITY = param.VLANPRIORITY;

		return g.toJson(bean).getBytes();
	}

	public static byte[] get_GET_LED_STATUS() {
		Gson g = new Gson();
		Parameter p = new Parameter("GET_LED_STATUS", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		return g.toJson(bean).getBytes();
	}

	public static byte[] get_SET_LED_STATUS(String LEDStatus) {
		Gson g = new Gson();
		Parameter p = new Parameter("SET_LED_STATUS", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		p.LEDStatus = LEDStatus;
		return g.toJson(bean).getBytes();
	}

	public static byte[] get_GET_UPLINK_MODE() {
		Gson g = new Gson();
		Parameter p = new Parameter("GET_UPLINK_MODE", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		return g.toJson(bean).getBytes();
	}

	public static byte[] get_SET_UPLINK_MODE(String MODE) {
		Gson g = new Gson();
		Parameter p = new Parameter("SET_UPLINK_MODE", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		p.MODE = MODE;

		return g.toJson(bean).getBytes();
	}

	public static byte[] get_SET_WIFI_MODE(String WifiIndex, String Mode) {
		Gson g = new Gson();
		Parameter p = new Parameter("SET_WIFI_MODE", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		p.WifiIndex = WifiIndex;
		p.Mode = Mode;

		return g.toJson(bean).getBytes();
	}

	public static byte[] get_GET_WIFI_SSIDLIST(String WifiIndex) {
		Gson g = new Gson();
		Parameter p = new Parameter("GET_WIFI_SSIDLIST", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		p.WifiIndex = WifiIndex;

		return g.toJson(bean).getBytes();
	}

	public static byte[] get_QUERY_WIFI_NUM() {

		Gson g = new Gson();
		Parameter p = new Parameter("QUERY_WIFI_NUM", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_GET_CLIENT_WIFI_INFO(String WifiIndex) {
		Gson g = new Gson();
		Parameter p = new Parameter("GET_CLIENT_WIFI_INFO", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post", "Plugin_ID", "1", p);

		p.WifiIndex = WifiIndex;

		return g.toJson(bean).getBytes();
	}

	public static byte[] get_HG_WPSBUTTON_PUSH() {

		Gson g = new Gson();
		Parameter p = new Parameter("HG_WPSBUTTON_PUSH", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_HG_DEVICE_UPDATE() {

		Gson g = new Gson();
		Parameter p = new Parameter("UPGRADEALL", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	// public static byte[] get_GET_WIFI_POWERLEVEL(String WifiIndex, String
	// SSIDIndex) {
	//
	// Gson g = new Gson();
	// Parameter p = new Parameter("GET_WIFI_POWERLEVEL", SequenceIdGenerator
	// .getInstance().next());
	// RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);
	//
	// p.WifiIndex = WifiIndex;
	// p.SSIDIndex = SSIDIndex;
	//
	// String s = g.toJson(bean);
	// return s.getBytes();
	// }

	// public static byte[] get_SET_WIFI_POWERLEVEL(String WifiIndex, String
	// SSIDIndex, String PowerLevel) {
	//
	// Gson g = new Gson();
	// Parameter p = new Parameter("SET_WIFI_POWERLEVEL", SequenceIdGenerator
	// .getInstance().next());
	// RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);
	//
	// p.WifiIndex = WifiIndex;
	// p.SSIDIndex = SSIDIndex;
	// p.PowerLevel = PowerLevel;
	//
	// String s = g.toJson(bean);
	// return s.getBytes();
	// }

	public static byte[] get_GET_WIFI_INFO(String WifiIndex, String SSIDIndex) {

		Gson g = new Gson();
		Parameter p = new Parameter("GET_WIFI_INFO", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);

		p.WifiIndex = WifiIndex;
		p.SSIDIndex = SSIDIndex;

		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_SET_INTERNET_QOS(String QoSMode) {

		Gson g = new Gson();
		Parameter p = new Parameter("SET_INTERNET_QOS", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);

		p.QoSMode = QoSMode;

		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_GET_INTERNET_QOS() {

		Gson g = new Gson();
		Parameter p = new Parameter("GET_INTERNET_QOS", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);

		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_GET_ATTACH_INFO() {

		Gson g = new Gson();
		Parameter p = new Parameter("GET_ATTACH_INFO", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);

		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_SET_ATTACH_DEVICE_RIGHT(String MAC, String InternetAccessRight, String StartTime, String EndTime,
			String DaysOfWeek, String ControlWifiMode) {

		Gson g = new Gson();
		Parameter p = new Parameter("SET_ATTACH_DEVICE_RIGHT", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);

		p.MAC = MAC;
		p.InternetAccessRight = InternetAccessRight;
		p.StartTime = StartTime;
		p.EndTime = EndTime;
		p.DaysOfWeek = DaysOfWeek;
		p.ControlWifiMode = ControlWifiMode;

		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_SET_DEVICE_NAME(String mac, String name, String Uprate, String downrate) {

		Gson g = new Gson();
		Parameter p = new Parameter("SET_DEVICE_NAME", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);

		p.mac = mac;
		p.name = name;
		p.Uprate = Uprate;
		p.downrate = downrate;

		String s = g.toJson(bean);
		return s.getBytes();
	}

	public static byte[] get_GET_ATTACH_DEVICE_RIGHT(String ControlWifiMode) {

		Gson g = new Gson();
		Parameter p = new Parameter("GET_ATTACH_DEVICE_RIGHT", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);
		p.ControlWifiMode=  ControlWifiMode;  // ControlWifiMode 1无线， 2有线   
		String s = g.toJson(bean);
		return s.getBytes();
	}

	/* 2015 新增 */
	/** 物理连接诊断 */
	public static byte[] get_GET_PHYLINK_RESULT() {
		Gson g = new Gson();
		Parameter p = new Parameter("GET_PHYLINK_RESULT", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	/** WAN连接诊断 */
	public static byte[] get_GET_WANCONN_RESULT() {
		Gson g = new Gson();
		Parameter p = new Parameter("GET_WANCONN_RESULT", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	/** 路由诊断 */
	public static byte[] get_GET_ROUTEDIAG_RESULT() {
		Gson g = new Gson();
		Parameter p = new Parameter("GET_ROUTEDIAG_RESULT", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	/** DNS诊断 */
	public static byte[] get_GET_DNSDIAG_RESULT() {
		Gson g = new Gson();
		Parameter p = new Parameter("GET_DNSDIAG_RESULT", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

	/** 关联设备 */
	public static byte[] get_GET_DEVICE_TYPE() {
		Gson g = new Gson();
		Parameter p = new Parameter("GET_DEVICE_TYPE", SequenceIdGenerator.getInstance().next());
		RequestBean bean = new RequestBean("Post1", "Plugin_ID", "1", p);
		String s = g.toJson(bean);
		return s.getBytes();
	}

}
