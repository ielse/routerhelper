package com.fiberhome.opticalbox.db.model;

import org.litepal.crud.DataSupport;

public class DBAttachInfo extends DataSupport {

	private long id;
	private String mac;
	private String nickname;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

}
