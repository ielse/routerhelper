package com.fiberhome.opticalbox.common.upgrade;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import android.os.Environment;

/**
 * 升级服务文件处理辅助类
 */
public class FileUtil {

	public static File updateDir = null;
	public static File updateFile = null;

	/*********** 保存升级APK的目录 ***********/
	public static final String CarApplication = "fhcat";

	public static boolean isCreateFileSucess;

	/**
	 * 方法描述：createFile方法
	 * 
	 * @param String
	 *            app_name
	 * @return
	 * @see FileUtil
	 */
	public static void createFile(String app_name) {

		if (android.os.Environment.MEDIA_MOUNTED.equals(android.os.Environment.getExternalStorageState())) {
			isCreateFileSucess = true;

			updateDir = new File(Environment.getExternalStorageDirectory() + "/" + CarApplication + "/");
			updateFile = new File(updateDir + "/" + app_name + ".apk");

			if (!updateDir.exists()) {
				updateDir.mkdirs();
			}
			if (!updateFile.exists()) {
				try {
					updateFile.createNewFile();
				} catch (IOException e) {
					isCreateFileSucess = false;
					e.printStackTrace();
				}
			}

		} else {
			isCreateFileSucess = false;
		}
	}

	public static boolean inputStreamToFile(InputStream ins, File file) {
		try {
			OutputStream os = new FileOutputStream(file);
			int bytesRead = 0;
			byte[] buffer = new byte[8192];
			while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
				os.write(buffer, 0, bytesRead);
			}
			os.close();
			ins.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static String FileToString(File file) {
		byte[] buffer = null;
		try {
			FileInputStream fis = new FileInputStream(file);
			ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
			byte[] b = new byte[1000];
			int n;
			while ((n = fis.read(b)) != -1) {
				bos.write(b, 0, n);
			}
			fis.close();
			bos.close();
			buffer = bos.toByteArray();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return "";
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
		return new String(buffer);
	}

	public static boolean inputStreamToOutputStream(InputStream input, OutputStream output) {
		BufferedOutputStream out = null;
		BufferedInputStream in = null;
		in = new BufferedInputStream(input, 8 * 1024);
		out = new BufferedOutputStream(output, 8 * 1024);
		int b;
		try {
			while ((b = in.read()) != -1) {
				out.write(b);
			}
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null)
					out.close();
				if (in != null)
					in.close();
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	public static String inputStreamToString(InputStream in) {
		String result = "";
		if (null != in) {
			InputStreamReader ir = null;
			BufferedReader br = null;
			StringBuffer sb = new StringBuffer();
			try {
				ir = new InputStreamReader(in);
				br = new BufferedReader(ir);
				String line;
				while ((line = br.readLine()) != null)
					sb.append("/n").append(line);
				if (result.length() > 0)
					result = result.substring("/n".length());
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (ir != null)
						ir.close();
					if (br != null)
						br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}

	public static InputStream byteToInputStream(byte[] bytes) {
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		return bais;
	}

}
