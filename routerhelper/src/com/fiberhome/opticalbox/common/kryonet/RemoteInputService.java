package com.fiberhome.opticalbox.common.kryonet;

import java.io.IOException;
import java.net.InetAddress;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.fiberhome.opticalbox.common.kryonet.RInputManager.OnInfoReceivedListener;
import com.fiberhome.opticalbox.utils.LogUtil;
import com.fiberhome.remoteime.kryonet.Client;
import com.fiberhome.remoteime.kryonet.Connection;
import com.fiberhome.remoteime.kryonet.Listener;

public class RemoteInputService extends Service implements OnInfoReceivedListener {

	private final LocalBinder binder = new LocalBinder();

	public class LocalBinder extends Binder {
		public RemoteInputService getService() {
			return RemoteInputService.this;
		}
	}

	@Override public IBinder onBind(Intent intent) {
		return binder;
	}

	private void connectToServer() {
		new Thread(new Runnable() {
			public void run() {
				try {
					InetAddress host = client.discoverHost(RInputManager.udpPort, 2000);
					if (host != null) {
						LogUtil.d("client connect " + host);
						client.connect(2000, host, RInputManager.tcpPort, RInputManager.udpPort);
					}
					else {
						Log.e("RemoteInputClient", "没有找到server端");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	final Client client = new Client();

	@Override public void onCreate() {
		super.onCreate();
		LogUtil.d("input service onCreate");
		client.start();
		client.addListener(l);

		mRInputManager.setListener(this);
		connectToServer();
	}

	@Override public void onDestroy() {
		super.onDestroy();
		LogUtil.d("input service onDestroy");
		client.stop();
		client.removeListener(l);

		mRInputManager.setListener(null);
	}

	private final Listener l = new Listener() {
		public void received(Connection connection, Object object) {
			LogUtil.e("client received:" + object);
			if (object instanceof String) {
				mRInputManager.decryptSyncInfo((String) object);
			}
		}
	};

	private final RInputManager mRInputManager = RInputManager.i();

	@Override public void onTextRecevied(String text) {
		broadcast(ACTION_TEXT, text);

		Intent intent = new Intent(this, RemoteInputActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtra(EXTRA_TEXTINFO, text);
		startActivity(intent);
	}

	@Override public void onKeyRecevied(int KeyCode) {
		broadcast(ACTION_KEY, String.valueOf(KeyCode));
	}

	public static final String EXTRA_TEXTINFO = "e_textinfo";

	public static final String ACTION_TEXT = "com.fiberhome.opticalbox.common.kryonet.ACTION_TEXT";
	public static final String ACTION_KEY = "com.fiberhome.opticalbox.common.kryonet.ACTION_KEY";
	public static final String EXTRA_DATA = "com.fiberhome.opticalbox.common.kryonet.EXTRA_DATA";

	private void broadcast(String action, String content) {
		final Intent intent = new Intent(action);
		intent.putExtra(EXTRA_DATA, content);
		sendBroadcast(intent);
	}

	public void syncTextToServer(final String text) {
		new Thread(new Runnable() {
			public void run() {
				LogUtil.e("client syncTextToServer:" + text);
				client.sendTCP(mRInputManager.encryptSyncInfo(RInputManager.EVENT_TEXT, text));
			}
		}).start();
	}

	public void sendKeyToServer(final int key) {
		new Thread(new Runnable() {
			public void run() {
				LogUtil.e("client sendKeyToServer:" + key);
				client.sendTCP(mRInputManager.encryptSyncInfo(RInputManager.EVENT_KEY, String.valueOf(key)));
			}
		}).start();
	}
}
