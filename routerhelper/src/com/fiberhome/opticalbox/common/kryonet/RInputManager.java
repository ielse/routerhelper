package com.fiberhome.opticalbox.common.kryonet;

import android.util.Log;

public class RInputManager {

	public static int tcpPort = 54555, udpPort = 54777;

	public static final String EVENT_TEXT = "TEXT:";
	public static final String EVENT_KEY = "KEY:";

	private RInputManager() {
	}

	private static RInputManager mRInputManager = new RInputManager();

	public static RInputManager i() {
		return mRInputManager;
	}

	private OnInfoReceivedListener listener;

	public void setListener(OnInfoReceivedListener listener) {
		this.listener = listener;
	}

	public interface OnInfoReceivedListener {
		void onTextRecevied(String text);

		void onKeyRecevied(int KeyCode);
	}

	public final String encryptSyncInfo(String event, String text) {
		if (event == null) throw new IllegalArgumentException("empty event");
		if (text == null) text = "";
		return event + text;
	}

	public final void decryptSyncInfo(String text) {
		if (text == null) {
			Log.d("RInputManager", "null content");
			return;
		}
		else if (text.startsWith(EVENT_TEXT)) {
			if (listener != null) {
				String value = text.substring(EVENT_TEXT.length());
				Log.d("RInputManager", "onTextRecevied:" + value);
				listener.onTextRecevied(value);
			}
		}
		else if (text.startsWith(EVENT_KEY)) {
			if (listener != null) {
				int key = Integer.valueOf(text.substring(EVENT_KEY.length()));
				Log.d("RInputManager", "onKeyRecevied:" + key);
				listener.onKeyRecevied(key);
			}
		}
	}
}
