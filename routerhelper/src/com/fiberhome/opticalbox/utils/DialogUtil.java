package com.fiberhome.opticalbox.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.fiberhome.opticalbox.R;

public class DialogUtil {

	private Context context;
	private Dialog dialog;
	private TextView txTitle;
	private Button btnConfirm, btnCancel;
	private View layoutTitle, layoutContent, layoutBtn;
	private View viewTitleLine;

	private DialogUtil() {
	}

	public static DialogUtil init(Context context) {
		DialogUtil dialogBuilder = new DialogUtil();
		dialogBuilder.context = context;
		return dialogBuilder;
	}

	public DialogUtil create(View content, boolean canCancel, boolean shadow) {
		dialog = new Dialog(context, shadow ? R.style.Theme_Light_NoTitle_Dialog : R.style.Theme_Light_NoTitle_NoShadow_Dialog);
		View view = LayoutInflater.from(context).inflate(R.layout.ob_dialog_confirm_style_blue, null);

		Window win = dialog.getWindow();
		win.getDecorView().setPadding(0, 0, 0, 0);
		WindowManager.LayoutParams lp = win.getAttributes();
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		win.setAttributes(lp);

		layoutTitle = view.findViewById(R.id.layout_title);
		layoutContent = view.findViewById(R.id.layout_content);
		layoutBtn = view.findViewById(R.id.layout_btn);

		txTitle = (TextView) view.findViewById(R.id.tx_title);
		btnConfirm = (Button) view.findViewById(R.id.btn_confirm);
		btnCancel = (Button) view.findViewById(R.id.btn_cancel);

		viewTitleLine = view.findViewById(R.id.view_title_line);

		((FrameLayout) layoutContent).addView(content);

		btnConfirm.setVisibility(View.GONE);

		btnCancel.setOnClickListener(new OnClickListener() {
			@Override public void onClick(View view) {
				if (dialog != null && dialog.isShowing())
					dialog.dismiss();
			}
		});

		dialog.setContentView(view);
		dialog.setCanceledOnTouchOutside(canCancel);
		dialog.setCancelable(canCancel);

		return this;
	}

	public DialogUtil setConfirmListener(OnClickListener confirmListener) {
		btnConfirm.setVisibility(View.VISIBLE);
		btnConfirm.setOnClickListener(confirmListener);
		return this;
	}
	
	public DialogUtil setCancelListener(OnClickListener cancelListener) {
		btnCancel.setOnClickListener(cancelListener);
		return this;
	}

	public DialogUtil setNoTitle() {
		layoutTitle.setVisibility(View.GONE);
		return this;
	}

	public DialogUtil setNoBtn() {
		layoutBtn.setVisibility(View.GONE);
		return this;
	}

	public Button getConfirmButton() {
		return btnConfirm;
	}

	public DialogUtil setTitleText(int resId) {
		return setTitleText(context.getString(resId));
	}

	public DialogUtil setTitleText(String text) {
		txTitle.setText(text);
		return this;
	}

	public DialogUtil setConfirmText(int resId) {
		btnConfirm.setText(resId);
		return this;
	}

	public DialogUtil setCancelText(int resId) {
		btnCancel.setText(resId);
		return this;
	}
	
	public Button getCancelButton(){
		return btnCancel;
	}

	public Dialog show() {
		dialog.show();
		return finish();
	}

	public Dialog finish() {
		return dialog;
	}

	public DialogUtil createSimple(int textId, boolean canCancel, boolean shadow) {
		return createSimple(context.getString(textId), canCancel, shadow);
	}

	public DialogUtil createSimple(String text, boolean canCancel, boolean shadow) {
		TextView content = (TextView) LayoutInflater.from(context).inflate(R.layout.ob_dialog_part_simple_text, null);
		content.setText(text);
		return create(content, canCancel, shadow);
	}

	public DialogUtil createBottom(View content, boolean canCancel, boolean shadow) {
		create(content, canCancel, shadow);
		Window win = dialog.getWindow();
		win.setGravity(Gravity.BOTTOM);
		win.setWindowAnimations(R.style.Animation_Bottom_Rising);

		viewTitleLine.setVisibility(View.GONE);
		return this;
	}

	public DialogUtil createTop(View content, boolean canCancel, boolean shadow) {
		create(content, canCancel, shadow);
		Window win = dialog.getWindow();
		win.setGravity(Gravity.BOTTOM);
		win.setWindowAnimations(R.style.Animation_Top_Rising);

		viewTitleLine.setVisibility(View.GONE);
		return this;
	}

	public DialogUtil createLoading(int textId, boolean canCancel, boolean shadow) {
		dialog = new Dialog(context, shadow ? R.style.Theme_Light_NoTitle_Dialog : R.style.Theme_Light_NoTitle_NoShadow_Dialog);
		View view = LayoutInflater.from(context).inflate(R.layout.ob_dialog_loading, null);
		TextView txMessage = (TextView) view.findViewById(R.id.tx_message);
		ImageView imgLoading = (ImageView) view.findViewById(R.id.img_loading);
		((AnimationDrawable) imgLoading.getDrawable()).start();
		txMessage.setText(textId);
		dialog.setContentView(view);
		dialog.setCanceledOnTouchOutside(canCancel);
		dialog.setCancelable(canCancel);
		return this;
	}

	public DialogUtil createQrCode(boolean canCancel, boolean shadow) {
		dialog = new Dialog(context, shadow ? R.style.Theme_Light_NoTitle_Dialog : R.style.Theme_Light_NoTitle_NoShadow_Dialog);
		View view = LayoutInflater.from(context).inflate(R.layout.ob_dialog_qr_code, null);

		Window win = dialog.getWindow();
		win.getDecorView().setPadding(0, 0, 0, 0);
		WindowManager.LayoutParams lp = win.getAttributes();
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		win.setAttributes(lp);

		ImageView imgQrCode = (ImageView) view.findViewById(R.id.img_qr_code);
		imgQrCode.setAnimation(AnimUtil.init().addAlpha(0.8f, 1f).create());

		dialog.setContentView(view);
		dialog.setCanceledOnTouchOutside(canCancel);
		dialog.setCancelable(canCancel);
		return this;
	}
}
