package com.fiberhome.opticalbox.utils;

import android.content.Context;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

public class AnimUtil {

	public static final int DEFAULT_DURATION = 300;

	private AnimationSet set;

	private AnimUtil() {
	}

	public static AnimUtil init() {
		AnimUtil animUtils = new AnimUtil();
		animUtils.set = new AnimationSet(false);
		return animUtils;
	}

	public static Animation load(Context context, int animId) {
		return AnimationUtils.loadAnimation(context, animId);
	}

	public AnimUtil addAlpha(float fromAlpha, float toAlpha) {
		AlphaAnimation alpha = new AlphaAnimation(fromAlpha, toAlpha);
		alpha.setDuration(DEFAULT_DURATION);
		set.addAnimation(alpha);
		return this;
	}

	public AnimUtil addAlpha(float fromAlpha, float toAlpha, long offset, long duration, Interpolator interpolator, int repeatMode) {
		AlphaAnimation alpha = new AlphaAnimation(fromAlpha, toAlpha);
		alpha.setDuration(duration);
		alpha.setStartOffset(offset);
		alpha.setInterpolator(interpolator);
		alpha.setRepeatMode(repeatMode);
		if (repeatMode == Animation.REVERSE)
			alpha.setRepeatCount(1);
		set.addAnimation(alpha);
		return this;
	}

	public AnimUtil addScale(float fromX, float toX, float fromY, float toY, float pivotX, float pivotY) {
		ScaleAnimation scale = new ScaleAnimation(fromX, toX, fromY, toY, Animation.RELATIVE_TO_SELF, pivotX, Animation.RELATIVE_TO_SELF,
				pivotY);
		scale.setDuration(DEFAULT_DURATION);
		set.addAnimation(scale);
		return this;
	}

	public AnimUtil addScale(float fromX, float toX, float fromY, float toY, int pivotXType, float pivotXValue, int pivotYType,
			float pivotYValue, long offset, long duration, Interpolator interpolator) {
		ScaleAnimation scale = new ScaleAnimation(fromX, toX, fromY, toY, pivotXType, pivotXValue, pivotYType, pivotYValue);
		scale.setDuration(duration);
		scale.setStartOffset(offset);
		scale.setInterpolator(interpolator);
		set.addAnimation(scale);
		return this;
	}

	public AnimUtil addTranslate(float fromXDelta, float toXdelta, float fromYdelta, float toYDelta) {
		TranslateAnimation translate = new TranslateAnimation(fromXDelta, toXdelta, fromYdelta, toYDelta);
		translate.setDuration(DEFAULT_DURATION);
		set.addAnimation(translate);
		return this;
	}

	public AnimUtil addTranslate(float fromXDelta, float toXdelta, float fromYdelta, float toYDelta, long offset, long duration,
			Interpolator interpolator) {
		TranslateAnimation translate = new TranslateAnimation(fromXDelta, toXdelta, fromYdelta, toYDelta);
		translate.setDuration(duration);
		translate.setStartOffset(offset);
		translate.setInterpolator(interpolator);
		set.addAnimation(translate);
		return this;
	}

	public AnimUtil addTranslate(int fromXType, float fromXValue, int toXType, float toXValue, int fromYType, float fromYValue,
			int toYType, float toYValue, long offset, long duration, Interpolator interpolator) {
		TranslateAnimation translate = new TranslateAnimation(fromXType, fromXValue, toXType, toXValue, fromYType, fromYValue, toYType,
				toYValue);
		translate.setDuration(duration);
		translate.setStartOffset(offset);
		translate.setInterpolator(interpolator);
		set.addAnimation(translate);
		return this;
	}

	public AnimUtil addRotate(float fromDegrees, float toDegrees, float pivotX, float pivotY) {
		RotateAnimation rotate = new RotateAnimation(fromDegrees, toDegrees, Animation.RELATIVE_TO_SELF, pivotX,
				Animation.RELATIVE_TO_SELF, pivotY);
		rotate.setDuration(DEFAULT_DURATION);
		set.addAnimation(rotate);
		return this;
	}

	public AnimUtil addRotate(float fromDegrees, float toDegrees, int pivotXType, float pivotXValue, int pivotYType, float pivotYValue,
			long offset, long duration, Interpolator interpolator) {
		RotateAnimation rotate = new RotateAnimation(fromDegrees, toDegrees, pivotXType, pivotXValue, pivotYType, pivotYValue);

		rotate.setDuration(duration);
		rotate.setStartOffset(offset);
		rotate.setInterpolator(interpolator);
		set.addAnimation(rotate);
		return this;
	}

	public AnimationSet create() {
		return set;
	}

	public static AnimationSet cearteAlpha(float fromAlpha, float toAlpha) {
		return AnimUtil.init().addAlpha(fromAlpha, toAlpha).create();
	}
}
