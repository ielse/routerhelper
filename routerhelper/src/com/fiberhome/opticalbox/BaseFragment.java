package com.fiberhome.opticalbox;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;

import com.fiberhome.opticalbox.net.NetManager;
import com.fiberhome.opticalbox.net.NetManager.TcpErrorListener;
import com.fiberhome.opticalbox.net.NetManager.TcpListener;
import com.fiberhome.opticalbox.ui.MainActivity;
import com.fiberhome.opticalbox.utils.AnimUtil;
import com.fiberhome.opticalbox.utils.BaseUtil;
import com.fiberhome.opticalbox.utils.DialogUtil;

/**
 * @description 所有碎片应该继承到基类<br>
 *              提供了碎片常用的对象和方法
 * @author wangpeng
 * @date 2014-12-20
 */
public class BaseFragment extends Fragment {

	/** 碎片持有者 */
	public BaseActivity holder;
	/** 自身當前是否可見 */
	public boolean selfVisible;
	/** 布局加载器 */
	public LayoutInflater inflater;
	/** 对话框构造器 */
	public DialogUtil dialogUtil;

	public NetManager netManager;

	public Handler handler = new Handler();

	@Override public void onAttach(Activity activity) {
		super.onAttach(activity);
		holder = (BaseActivity) activity;

		inflater = holder.inflater;
		netManager = holder.netManager;
		dialogUtil = holder.dialogUtil;
	}

	public void finishAttachActivity() {
		if (holder != null) {
			holder.finish();
		}
	}

	public void registerChildTcpListener(TcpListener tcpListener) {
		if (holder != null) {
			holder.registerChildTcpListener(tcpListener);
		}
	}

	public void unRegisterChildTcpListener(TcpListener tcpListener) {
		if (holder != null) {
			holder.unRegisterChildTcpListener(tcpListener);
		}
	}

	public void registerChildTcpErrorListener(TcpErrorListener tcpErrorListener) {
		if (holder != null) {
			holder.registerChildTcpErrorListener(tcpErrorListener);
		}
	}

	public void unRegisterChildTcpErrorListener(TcpErrorListener tcpErrorListener) {
		if (holder != null) {
			holder.unRegisterChildTcpErrorListener(tcpErrorListener);
		}
	}

	public void showToast(int textId) {
		if (holder != null) {
			holder.showToast(textId);
		}
	}

	public void showToast(String text) {
		if (holder != null) {
			holder.showToast(text);
		}
	}

	public void actionStart(Class<?> target, Object... params) {
		if (holder != null) {
			holder.actionStart(target, BaseUtil.initBundle(params));
		}
	}

	public void actionStart(Class<?> target, Bundle bundle) {
		if (holder != null) {
			holder.actionStart(target, bundle);
		}
	}

	public OnClickListener onMainBackClickListener = new OnClickListener() {
		@Override public void onClick(View view) {
			if (holder != null) {
				if (holder instanceof MainActivity) {
					startAnimation(view, AnimUtil.init().addAlpha(1f, 0.2f, 0, 200, new LinearInterpolator(), Animation.REVERSE).create());
					((MainActivity) holder).setSelection();
				}
			}
		}
	};

	public void startAnimation(View view, Animation animation) {
		view.setVisibility(View.VISIBLE);
		view.clearAnimation();
		view.startAnimation(animation);
	}

	public void endAnimation(View view, Animation animation) {
		view.clearAnimation();
		view.startAnimation(animation);
		view.setVisibility(View.GONE);
	}

	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override public void onDetach() {
		super.onDetach();
		if (holder != null) {
			if (this instanceof TcpListener) {
				holder.unRegisterChildTcpListener((TcpListener) this);
			}
			if (this instanceof TcpErrorListener) {
				holder.unRegisterChildTcpErrorListener((TcpErrorListener) this);
			}
			holder = null;
		}
	}

	@Override public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		selfVisible = !hidden;
	}

}
