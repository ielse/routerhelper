package com.fiberhome.opticalbox;

import org.litepal.LitePalApplication;
import org.litepal.tablemanager.Connector;

import com.fiberhome.opticalbox.common.cache.BitmapCache;
import com.fiberhome.opticalbox.utils.ScreenUtil;

public class App extends LitePalApplication {

	private static App instance;

	/** 登录者信息 */
	public String username = "telecomadmin";
	/** 登录者信息 */
	public String password = "nE7jA%5m";

	public String productType;

	private static boolean firstsendvideo = true;

	public BitmapCache bitmapCache;

	@Override public void onCreate() {
		super.onCreate();

		instance = this;

		bitmapCache = new BitmapCache();

		Connector.getDatabase(); // init litepal

		CrashHandler.init(this);
	}

	public static synchronized App i() {
		return instance;
	}

	public int getScreenWidth() {
		return ScreenUtil.getScreenWidth(this);
	}

	public int getScreenHeight() {
		return ScreenUtil.getScreenHeight(this);
	}

	public static void setVideoFlags(boolean flags) {
		firstsendvideo = flags;
		return;
	}

	public static boolean getVideoFlags() {
		return firstsendvideo;
	}

	@Override public void onTerminate() {
		super.onTerminate();
	}

	@Override public void onLowMemory() {
		super.onLowMemory();
	}

}
