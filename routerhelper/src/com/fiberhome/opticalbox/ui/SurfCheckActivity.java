package com.fiberhome.opticalbox.ui;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.BaseActivity;
import com.fiberhome.opticalbox.net.NetConstant;
import com.fiberhome.opticalbox.net.NetManager.TcpErrorListener;
import com.fiberhome.opticalbox.net.NetManager.TcpListener;
import com.fiberhome.opticalbox.net.bean.Parameter;
import com.fiberhome.opticalbox.net.utils.JsonUtil;
import com.fiberhome.opticalbox.view.HealthCheckingView;

/**
 * 绿色上网
 * 
 * @author wangpeng
 * 
 */
public class SurfCheckActivity extends BaseActivity {

	private HealthCheckingView checkingView;
	private TextView txCheckingInfo;
	private Button btnSubmit;
	private ImageView imgConnPphysical, imgConnWan, imgRouting, imgDns;

	public static final int STEP_PHYLINK = 1;
	public static final int STEP_WANCONN = 2;
	public static final int STEP_ROUTEDIAG = 3;
	public static final int STEP_DNSDIAG = 4;

	private Handler handler = new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.ob_activity_surf_check);
		super.onCreate(savedInstanceState);

		titleView.setTitle(R.string.ob_surf_check_1);
		titleView.addLeftDrawableMenu(this, R.drawable.ob_ico_back, 15, 25, onBackClickListener);

		checkingView = (HealthCheckingView) findViewById(R.id.ob_checkingView);
		txCheckingInfo = (TextView) findViewById(R.id.tx_checking_info);
		btnSubmit = (Button) findViewById(R.id.btn_submit);
		btnSubmit.setOnClickListener(onSubmitClickListener);
		imgConnPphysical = (ImageView) findViewById(R.id.img_conn_physical);
		imgConnWan = (ImageView) findViewById(R.id.img_conn_wan);
		imgRouting = (ImageView) findViewById(R.id.img_routing);
		imgDns = (ImageView) findViewById(R.id.img_dns);

		registerChildTcpListener(tcpListener);
		registerChildTcpErrorListener(tcpErrorListener);

		startHealthChecking();
	}

	private void startHealthChecking() {
		checkingView.restart();
		txCheckingInfo.setText(R.string.ob_health_checking_1);
		btnSubmit.setText(R.string.ob_cancel);
		btnSubmit.setTextColor(0xff5e5e5e);
		btnSubmit.setBackgroundResource(R.drawable.ob_selector_r30_ffffffff_stoke_c4c4c4);
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				request1PhyLinkResult();
			}
		}, 2000);
	}

	private void request1PhyLinkResult() {

		netManager.sendTcp(JsonUtil.get_GET_PHYLINK_RESULT());
	}

	private void request2WANConnResult() {
		netManager.sendTcp(JsonUtil.get_GET_WANCONN_RESULT());
	}

	private void request3RoutediagResult() {
		netManager.sendTcp(JsonUtil.get_GET_ROUTEDIAG_RESULT());
	}

	private void request4DnsdiagResult() {
		netManager.sendTcp(JsonUtil.get_GET_DNSDIAG_RESULT());
	}

	private TcpListener tcpListener = new TcpListener() {
		@Override
		public void onReceive(String cmdType, Parameter param) {
			if (NetConstant.GET_PHYLINK_RESULT.equals(cmdType)) {
				// 1、网线连接正常 2、无线连接正常 3、网线未连接 4、无线未连接 -1、系统错误
				if ("1".equals(param.Result) || "2".equals(param.Result)) {
					doRightUIUpdate(STEP_PHYLINK);
					handler.postDelayed(new Runnable() {
						@Override
						public void run() {
							request2WANConnResult();
						}
					}, 800);
				} else {
					 doWrongFinishUIUpdate(STEP_PHYLINK);
				}
			} else if (NetConstant.GET_WANCONN_RESULT.equals(cmdType)) {
				// 1、DHCP地址正常;2、PPP拨号正常;3、桥连接正常;4、未配置连接;5、DHCP地址配置失败;6、PPP拨号失败;-1、系统错误
				if ("1".equals(param.Result) || "2".equals(param.Result)) {
					doRightUIUpdate(STEP_WANCONN);
					handler.postDelayed(new Runnable() {
						@Override
						public void run() {
							request3RoutediagResult();
						}
					}, 800);
				} else {
					 doWrongFinishUIUpdate(STEP_WANCONN);
				}
			} else if (NetConstant.GET_ROUTEDIAG_RESULT.equals(cmdType)) {
				// 0、路由链路正常;1、路由链路故障，请咨询运营商;2、路由配置失败;-1、系统错误
				if ("0".equals(param.Result)) {
					doRightUIUpdate(STEP_ROUTEDIAG);
					handler.postDelayed(new Runnable() {
						@Override
						public void run() {
							request4DnsdiagResult();
						}
					}, 800);
				} else {
					 doWrongFinishUIUpdate(STEP_ROUTEDIAG);
				}
			} else if (NetConstant.GET_DNSDIAG_RESULT.equals(cmdType)) {
				// 0、DNS诊断正常;1、DNS故障，请咨询运营商;-1、系统错误
				if ("0".equals(param.Result)) {
					doRightUIUpdate(STEP_DNSDIAG);
				} else {
					 doWrongFinishUIUpdate(STEP_DNSDIAG);
				}
			}
		}
	};

	private TcpErrorListener tcpErrorListener = new TcpErrorListener() {
		@Override
		public void onError(String cmdType) {
			if (NetConstant.GET_PHYLINK_RESULT.equals(cmdType)) {
				doWrongFinishUIUpdate(STEP_PHYLINK);
			} else if (NetConstant.GET_WANCONN_RESULT.equals(cmdType)) {
				doWrongFinishUIUpdate(STEP_WANCONN);
			} else if (NetConstant.GET_ROUTEDIAG_RESULT.equals(cmdType)) {
				doWrongFinishUIUpdate(STEP_ROUTEDIAG);
			} else if (NetConstant.GET_DNSDIAG_RESULT.equals(cmdType)) {
				doWrongFinishUIUpdate(STEP_DNSDIAG);
			}
		}
	};

	private void doRightUIUpdate(int step) {
		switch (step) {
		case STEP_PHYLINK:
			checkingView.setProgress(25);
			imgConnPphysical.setVisibility(View.VISIBLE);
			imgConnPphysical.setImageResource(R.drawable.ob_check_right);
			break;
		case STEP_WANCONN:
			checkingView.setProgress(50);
			imgConnWan.setVisibility(View.VISIBLE);
			imgConnWan.setImageResource(R.drawable.ob_check_right);
			break;
		case STEP_ROUTEDIAG:
			checkingView.setProgress(75);
			imgRouting.setVisibility(View.VISIBLE);
			imgRouting.setImageResource(R.drawable.ob_check_right);
			break;
		case STEP_DNSDIAG:
			checkingView.setProgress(100);
			imgDns.setVisibility(View.VISIBLE);
			imgDns.setImageResource(R.drawable.ob_check_right);
			// finish all request
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					checkingView.finish(true);
					txCheckingInfo.setText(R.string.ob_success_health_checking);
					btnSubmit.setText(R.string.ob_finish_1);
					btnSubmit.setTextColor(0xffffffff);
					btnSubmit.setBackgroundResource(R.drawable.ob_selector_r30_ff00a2ff);
				}
			}, 800);
			
			break;
		}
	}

	private void doWrongFinishUIUpdate(int step) {
		checkingView.finish(false);
		txCheckingInfo.setText(R.string.ob_fail_health_checking);
		btnSubmit.setText(R.string.ob_finish_1);
		btnSubmit.setTextColor(0xffffffff);
		btnSubmit.setBackgroundResource(R.drawable.ob_selector_r30_ff00a2ff);

		imgConnPphysical.setVisibility(View.VISIBLE);
		imgConnWan.setVisibility(View.VISIBLE);
		imgRouting.setVisibility(View.VISIBLE);
		imgDns.setVisibility(View.VISIBLE);
		switch (step) {
		case STEP_PHYLINK:
			imgConnPphysical.setImageResource(R.drawable.ob_check_wrong);
		case STEP_WANCONN:
			imgConnWan.setImageResource(R.drawable.ob_check_wrong);
		case STEP_ROUTEDIAG:
			imgRouting.setImageResource(R.drawable.ob_check_wrong);
		case STEP_DNSDIAG:
			imgDns.setImageResource(R.drawable.ob_check_wrong);
			break;
		}
	}

	private OnClickListener onSubmitClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			finish();
		}
	};

}