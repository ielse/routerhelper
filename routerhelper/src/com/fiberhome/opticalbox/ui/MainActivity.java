package com.fiberhome.opticalbox.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.ActivityCollector;
import com.fiberhome.opticalbox.BaseActivity;
import com.fiberhome.opticalbox.common.kryonet.RemoteInputService;
import com.fiberhome.opticalbox.ui.fragment.frame.MainFragment;
import com.fiberhome.opticalbox.ui.fragment.frame.MainLeftMenuFragment;
import com.fiberhome.opticalbox.ui.fragment.frame.MenuFragment;
import com.fiberhome.opticalbox.ui.fragment.frame.MainLeftMenuFragment.Which;
import com.fiberhome.opticalbox.view.MFrameLayout;
import com.fiberhome.opticalbox.view.SlidingMenu;
import com.fiberhome.opticalbox.view.SlidingMenuScroller;
import com.fiberhome.opticalbox.view.ViewPagerScroller;
import com.fiberhome.opticalbox.view.SlidingMenu.OnSlidingFinishedListener;

/**
 * 首頁
 * 
 * @author wangpeng
 * @date 2014-12-20
 */
public class MainActivity extends BaseActivity {

	private SlidingMenu slidingMenu;
	private MainLeftMenuFragment leftMenuFragment;
	private MainFragment mainFragment;
	private MenuFragment menuFragment;
	private ViewPager viewPager;
	private ViewPagerScroller viewPagerScroller;

	private MFrameLayout layContent;

	@Override protected void onCreate(Bundle savedInstanceState) {

		setContentView(R.layout.ob_activity_main);
		super.onCreate(savedInstanceState);

		slidingMenu = (SlidingMenu) findViewById(R.id.sliding_menu);
		SlidingMenuScroller slidingMenuScroller = new SlidingMenuScroller(this);
		slidingMenuScroller.initViewPagerScroll(slidingMenu);
		slidingMenu.setOnSlidingFinishedListener(onToggleFinishedListener);

		FragmentTransaction transaction = fragmentManager.beginTransaction();
		leftMenuFragment = new MainLeftMenuFragment();
		transaction.add(R.id.layout_menu, leftMenuFragment);
		transaction.commit();

		menuFragment = new MenuFragment();
		mainFragment = new MainFragment();

		viewPager = (ViewPager) findViewById(R.id.ob_viewPager);
		viewPagerScroller = new ViewPagerScroller(this);
		viewPagerScroller.setScrollDuration(0);
		viewPagerScroller.initViewPagerScroll(viewPager);
		viewPager.setAdapter(new PagerAdapter(fragmentManager));

		layContent = (MFrameLayout) findViewById(R.id.layout_content);

		startService(new Intent(this, RemoteInputService.class));
	}

	@Override protected void onDestroy() {
		super.onDestroy();
		stopService(new Intent(this, RemoteInputService.class));
	}

	private OnSlidingFinishedListener onToggleFinishedListener = new OnSlidingFinishedListener() {
		@Override public void onFinished(boolean isOpen) {
			leftMenuFragment.leftMenuClickEvent(isOpen);
			layContent.setIntercept(isOpen);
		}
	};

	public void slidingMenuToggle() {
		if (slidingMenu != null) {
			slidingMenu.toggle();
		}
	}

	public void backToContent(Which... which) {
		slidingMenu.closeMenu();
		setSelection(which);
	}

	public void setSelection(Which... which) {
		if (which != null && which.length > 0) {
			menuFragment.setContent(which[0]);
			viewPager.setCurrentItem(1);
			viewPagerScroller.setScrollDuration(400);
			slidingMenu.setAbleToSliding(false);
		}
		else {
			viewPager.setCurrentItem(0);
			viewPagerScroller.setScrollDuration(0);
			slidingMenu.setAbleToSliding(true);
		}
	}

	class PagerAdapter extends FragmentPagerAdapter {

		public PagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override public CharSequence getPageTitle(int position) {
			return "fragment[" + position + "]";
		}

		@Override public int getCount() {
			return 2;
		}

		@Override public Fragment getItem(int position) {
			switch (position) {
			case 0:
				return mainFragment;
			case 1:
				return menuFragment;
			default:
				return null;
			}
		}
	}

	private long exitTime = 0;

	@Override public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
			if (viewPager.getCurrentItem() != 0) {
				setSelection();
				return true;
			}

			if ((System.currentTimeMillis() - exitTime) > 2000) {
				showToast(R.string.ob_exit_double_click);
				exitTime = System.currentTimeMillis();
			}
			else {
				ActivityCollector.finishAll();
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
