package com.fiberhome.opticalbox.ui;

import android.os.Bundle;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.BaseActivity;

public class HelpActivity extends BaseActivity {

	@Override protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.ob_activity_about_help);
		super.onCreate(savedInstanceState);

		titleView.setTitle(R.string.ob_help);
		titleView.setBackground(R.drawable.ob_bg_title);
		titleView.addLeftDrawableMenu(this, R.drawable.ob_ico_back, 15, 25,
				onBackClickListener);

	}
}