package com.fiberhome.opticalbox.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.BaseActivity;
import com.fiberhome.opticalbox.net.NetConstant;
import com.fiberhome.opticalbox.net.NetManager.TcpListener;
import com.fiberhome.opticalbox.net.bean.Parameter;
import com.fiberhome.opticalbox.net.utils.JsonUtil;

/**
 * Wi-Fi模式
 * 
 * @author wangpeng
 * 
 */
public class WifiModeActivity extends BaseActivity implements TcpListener {

	private View layWifiModeSleep, layWifiModeStandard, layWifiModePowerful;
	private ImageView imgCurrWifiMode;

	private final int WIFI_MODE_SLEEP = 60;
	private final int WIFI_MODE_STANDARD = 80;
	private final int WIFI_MODE_POWERFUL = 100;
	private int currWifiMode = 0;
	private int clickWifiMode = -1;
	private Parameter currParemter;

	private Dialog dlgWifiMode, dlgLoading;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.ob_activity_wifi_mode);
		super.onCreate(savedInstanceState);
		registerChildTcpListener(this);
		titleView.setTitle(R.string.ob_wifi_mode);
		titleView.addLeftDrawableMenu(this, R.drawable.ob_ico_back, 14, 25, onBackClickListener);

		imgCurrWifiMode = (ImageView) findViewById(R.id.img_curr_wifi_mode);
		layWifiModeSleep = findViewById(R.id.lay_wifi_mode_sleep);
		layWifiModeStandard = findViewById(R.id.lay_wifi_mode_standard);
		layWifiModePowerful = findViewById(R.id.lay_wifi_mode_powerful);

		layWifiModeSleep.setOnClickListener(onClickListener);
		layWifiModeStandard.setOnClickListener(onClickListener);
		layWifiModePowerful.setOnClickListener(onClickListener);

		requestGetWifiMode();
	}

	private void requestGetWifiMode() {
		String WifiIndex = "1";
		String SSIDIndex = "1";
		netManager.sendTcp(JsonUtil.get_GET_WIFI_INFO(WifiIndex, SSIDIndex));
	}

	private void requestSetWifiMode() {
		if (currParemter == null) {
			showToast(R.string.ob_fail_set_wifi_mode_1);
			return;
		}

		// 设置模式
		String WifiIndex = "1";
		String SSIDIndex = "1";
		String SSID = currParemter.SSID;
		String PWD = currParemter.PWD;
		String ENCRYPT = currParemter.ENCRYPT;
		String Channel = currParemter.Channel;
		String Enable = currParemter.Enable;
		String WifiCmdMode = "0"; // 0表示设置AP相关操作，1表示对关联其他AP动作
		String Hidden = "";
		netManager.sendTcp(JsonUtil.get_SET_WIFI_INFO(WifiIndex, SSIDIndex, SSID, PWD, ENCRYPT, String.valueOf(clickWifiMode), Channel,
				Enable, WifiCmdMode, Hidden));
	}

	private OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			switch (view.getId()) {
			case R.id.lay_wifi_mode_sleep:
				clickWifiMode = WIFI_MODE_SLEEP;
				break;
			case R.id.lay_wifi_mode_standard:
				clickWifiMode = WIFI_MODE_STANDARD;
				break;
			case R.id.lay_wifi_mode_powerful:
				clickWifiMode = WIFI_MODE_POWERFUL;
				break;
			}
			if (clickWifiMode != currWifiMode) {
				showSetWifiModeDialog(); // filter same mode request
			}
		}
	};

	private void showSetWifiModeDialog() {
		dlgWifiMode = dialogUtil.createSimple(R.string.ob_dlg_wifi_mode_reset, true, true).setConfirmListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dlgWifiMode.dismiss();
				showLoadingDialog();
				requestSetWifiMode();
			}
		}).show();
	}

	private void showLoadingDialog() {
		dlgLoading = dialogUtil.createLoading(R.string.ob_wait_set_wifi_mode_1, false, true).show();
	}

	@Override
	public void onReceive(String cmdType, Parameter param) {
		if (NetConstant.GET_WIFI_INFO.equals(cmdType)) {
			currParemter = param;
			int level = Integer.parseInt(param.PowerLevel);
			if (level > 80) {
				currWifiMode = WIFI_MODE_POWERFUL;
			} else if (level > 60) {
				currWifiMode = WIFI_MODE_STANDARD;
			} else {
				currWifiMode = WIFI_MODE_SLEEP;
			}
			resetCurrWifiMode(currWifiMode);
		} else if (NetConstant.SET_WIFI_INFO.equals(cmdType)) {
			showToast(R.string.ob_success_set_wifi_mode_1);
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					if (dlgLoading != null) {
						dlgLoading.dismiss();
					}
					actionStart(ProductListActivity.class);
					finish();
				}
			}, 3000);
		}
	}

	private void resetCurrWifiMode(int currentMode) {
		if (WIFI_MODE_SLEEP == currentMode) {
			layWifiModeSleep.setBackgroundColor(0xffe5e5e5);
			imgCurrWifiMode.setImageResource(R.drawable.ob_wifi_signal_sleep);
		} else if (WIFI_MODE_STANDARD == currentMode) {
			layWifiModeStandard.setBackgroundColor(0xffe5e5e5);
			imgCurrWifiMode.setImageResource(R.drawable.ob_wifi_signal_standard);
		} else if (WIFI_MODE_POWERFUL == currentMode) {
			layWifiModePowerful.setBackgroundColor(0xffe5e5e5);
			imgCurrWifiMode.setImageResource(R.drawable.ob_wifi_signal_powerful);
		}
	}

}