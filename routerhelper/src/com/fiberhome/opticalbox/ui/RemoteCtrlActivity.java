package com.fiberhome.opticalbox.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.ui.keyboard810.KeyboardGameActivity;
import com.fiberhome.opticalbox.ui.keyboard810.KeyboardInputActivity;
import com.fiberhome.opticalbox.ui.keyboard810.KeyboardTVFragment;
import com.fiberhome.opticalbox.ui.keyboard810.KeyboardTouchFragment;
import com.fiberhome.opticalbox.ui.keyboard810.MBaseRCActivity;

/**
 * 遥控器
 * 
 * @author wangpeng
 * 
 */
public class RemoteCtrlActivity extends MBaseRCActivity {

	private KeyboardTVFragment keyboardTVFragment;
	private KeyboardTouchFragment keyboardTouchFragment;

	private ImageView imgTV, imgGesture, imgInput, imgGame;

	@Override protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.ob_activity_remote_ctrl);
		super.onCreate(savedInstanceState);
		titleView.setTitle(R.string.ob_remote_ctrl);
		titleView.setTitleColor(0xff5e5e5e);
		titleView.addLeftDrawableMenu(this, R.drawable.ob_ico_back_black, 15, 25, onBackClickListener);

		findViewById(R.id.lay_tv).setOnClickListener(onClickListener);
		findViewById(R.id.lay_gesture).setOnClickListener(onClickListener);
		findViewById(R.id.lay_input).setOnClickListener(onClickListener);
		findViewById(R.id.lay_game).setOnClickListener(onClickListener);

		findViewById(R.id.lay_input).setOnTouchListener(onTouchListener);
		findViewById(R.id.lay_game).setOnTouchListener(onTouchListener);

		imgTV = (ImageView) findViewById(R.id.img_tv);
		imgGesture = (ImageView) findViewById(R.id.img_gesture);
		imgInput = (ImageView) findViewById(R.id.img_input);
		imgGame = (ImageView) findViewById(R.id.img_game);

		keyboardTVFragment = new KeyboardTVFragment();
		keyboardTouchFragment = new KeyboardTouchFragment();
		FragmentTransaction transation = fragmentManager.beginTransaction();
		transation.add(R.id.lay_content, keyboardTouchFragment);
		transation.hide(keyboardTouchFragment);
		transation.add(R.id.lay_content, keyboardTVFragment);
		transation.commit();
	}

	private OnClickListener onClickListener = new OnClickListener() {
		@Override public void onClick(View view) {
			switch (view.getId()) {
			case R.id.lay_tv:
			case R.id.lay_gesture:
				imgTV.setImageResource(R.drawable.ob_keyboard_tv_normal);
				imgGesture.setImageResource(R.drawable.ob_keyboard_gesture_normal);
				break;
			}
			switch (view.getId()) {
			case R.id.lay_tv:
				imgTV.setImageResource(R.drawable.ob_keyboard_tv_selected);
				FragmentTransaction transationTV = fragmentManager.beginTransaction();
				transationTV.hide(keyboardTouchFragment);
				transationTV.show(keyboardTVFragment);
				transationTV.commit();
				break;
			case R.id.lay_gesture:
				imgGesture.setImageResource(R.drawable.ob_keyboard_gesture_selected);
				FragmentTransaction transationTouch = fragmentManager.beginTransaction();
				transationTouch.hide(keyboardTVFragment);
				transationTouch.show(keyboardTouchFragment);
				transationTouch.commit();
				break;
			case R.id.lay_input:
				actionStart(KeyboardInputActivity.class);
				break;
			case R.id.lay_game:
				actionStart(KeyboardGameActivity.class);
				break;
			}
		}
	};

	private OnTouchListener onTouchListener = new OnTouchListener() {
		@Override public boolean onTouch(View view, MotionEvent event) {
			int action = event.getAction();
			switch (view.getId()) {
			case R.id.lay_input:
				if (action == MotionEvent.ACTION_DOWN) {
					imgInput.setImageResource(R.drawable.ob_keyboard_input_selected);
				} else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL) {
					imgInput.setImageResource(R.drawable.ob_keyboard_input_normal);
				}
				break;
			case R.id.lay_game:
				if (action == MotionEvent.ACTION_DOWN) {
					imgGame.setImageResource(R.drawable.ob_keyboard_game_selected);
				} else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL) {
					imgGame.setImageResource(R.drawable.ob_keyboard_game_normal);
				}
				break;
			}
			return false;
		}
	};

}