/**
 * @file BaseRCActivity.java
 * @par Copyright: - Copyright 2012 Amlogic Inc as unpublished work All Rights
 *      Reserved - The information contained herein is the confidential property
 *      of Amlogic. The use, copying, transfer or disclosure of such information
 *      is prohibited except by express written agreement with Amlogic Inc.
 * @version 1.0
 * @date 2012/12/03
 * @par function description: - 1
 */
package com.fiberhome.opticalbox.ui.keyboard810;

import android.annotation.SuppressLint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;

import com.amlapp.android.RemoteClient.DataBases;
import com.amlapp.android.RemoteClient.KeyMap;
import com.amlapp.android.RemoteClient.MSensor;
import com.amlapp.android.RemoteClient.ReceiveThread;
import com.amlapp.android.RemoteClient.SendThread;
import com.amlapp.android.RemoteClient.Service;
import com.amlapp.android.RemoteClient.SwitchKeyMode;
import com.fiberhome.opticalbox.BaseActivity;

/**
 * Description: This Class is The Basic View for RemoteControl Client. Default
 * Activity may inherit the Activity, set the layout file by setContentView()
 * method at the same time.
 */
@SuppressLint("HandlerLeak")
public class BaseRCActivity extends BaseActivity {
	private static final String TAG = "RCActivity";
	public ReceiveThread mReceiveThread = null;
	public Handler mSendCmdHandler = null;
	public SendThread mSendThread = null;
	public Handler myMessageHandler = null;
	private DataBases db = null;
	public Vibrator vibrator = null;
	private int sensorFilter;

	/*------------------------------------------------------------------------------------------*/
	public void setHandler(Handler handler) {
		this.myMessageHandler = handler;
	}

	public final void connectDatabase(DataBases db) {
		this.db = db;
		sendCmdMessage(SendThread.CMD_CONNECT, 0, 0, RemoteControl.mService);
	}

	public void ShowMouse(boolean show) {
		Log.d(TAG, "showMouse");
		int showID = show ? SwitchKeyMode.SHOWMOUSE : SwitchKeyMode.HIDEMOUSE;
		SwitchKeyMode T2MDown = new SwitchKeyMode(RemoteControl.mService, KeyEvent.ACTION_DOWN, KeyMap.RC_KEYCODE_PAD_MOUSE_SWITCH, showID);
		sendCmdMessage(SendThread.CMD_SEND_KEY_MODE, 0, 0, T2MDown);
		SwitchKeyMode T2MUp = new SwitchKeyMode(RemoteControl.mService, KeyEvent.ACTION_UP, KeyMap.RC_KEYCODE_PAD_MOUSE_SWITCH, showID);
		sendCmdMessage(SendThread.CMD_SEND_KEY_MODE, 0, 0, T2MUp);
	}

	public void sendCmdMessage(int what, int arg1, int arg2, Object obj) {
		if (mSendCmdHandler != null) {
			Message msg = Message.obtain();
			msg.what = what;
			msg.arg1 = arg1;
			msg.arg2 = arg2;
			msg.obj = obj;
			mSendCmdHandler.sendMessage(msg);
		} else {
			mSendCmdHandler = mSendThread.getHandler();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		mSendThread = SendThread.getInstance();
		mSendThread.set(myMessageHandler);
		mSendCmdHandler = mSendThread.getHandler();
		mReceiveThread = ReceiveThread.getInstance();
		mReceiveThread.set(myMessageHandler, mSendThread);
		sensorFilter = 0;
	}

	@SuppressLint("NewApi")
	public void sendSensorEvent(SensorEvent event) {
		float x = event.values[0];
		float y = event.values[1];
		float z = event.values[2];
		int accuracy = event.accuracy;
		if ((sensorFilter++) % 4 == 0) {
			sensorFilter = 0;
			MSensor mySensor = new MSensor(RemoteControl.mService, x, y, z, accuracy);
			sendCmdMessage(SendThread.CMD_SEND_SENSOR, Sensor.TYPE_ACCELEROMETER, 0, mySensor);
		}
	}

	// public void sendKey(View v, MotionEvent event) {
	// KeyImageView mKeyImageView = (KeyImageView) v;
	// switch (event.getAction()) {
	// case MotionEvent.ACTION_DOWN:
	// case MotionEvent.ACTION_UP:
	// Key mKey = new Key(RemoteIME.mService, event.getAction(),
	// mKeyImageView.getSendValue());
	// sendCmdMessage(SendThread.CMD_SEND_KEY, 0, 0, mKey);
	// vibrator.vibrate(10);
	// break;
	// default:
	// break;
	// }
	// }

	@SuppressWarnings("unused")
	private int switchTouchXY(int touch, int source, int target) {
		return (touch * target / source);
	}

	// public final void sendTouchValue(View v, MotionEvent event) {
	// KeyImageView mKeyImageView = (KeyImageView) v;
	// int width = mKeyImageView.getWidth();
	// int height = mKeyImageView.getHeight();
	// int touch_x = switchTouchXY((int) event.getX(), width,
	// RemoteIME.mService.getWidth());
	// int touch_y = switchTouchXY((int) event.getY(), height,
	// RemoteIME.mService.getHeigt());
	// Touch mTouch = new Touch(RemoteIME.mService, event.getAction(),
	// touch_x, touch_y);
	// sendCmdMessage(SendThread.CMD_SEND_TOUCH, 0, 0, mTouch);
	// }

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		vibrator = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
		if (mSendCmdHandler == null) {
			mSendCmdHandler = new Handler() {
				public void handleMessage(Message msg) {
					switch (msg.what) {
					case SendThread.CMD_DISCONNECT:
						if (RemoteControl.mService != null) {
							RemoteControl.mService.setConnected(Service.SERVER_IDLE);
						}
						if (db != null) {
							db.update(RemoteControl.mService);
						}
						finish();
						break;
					}
					super.handleMessage(msg);
				}
			};
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		sensorFilter = 0;
	}
}