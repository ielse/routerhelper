package com.fiberhome.opticalbox.ui.keyboard810;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wangpeng
 */
public class KeyboardCodeUtil {

	private static final Map<String, Integer> codeMaping = new HashMap<String, Integer>();

	public static Integer getCodeValue(String key) {
		return codeMaping.get(key);
	}

	static { // input
		codeMaping.put("shift", 61);
		codeMaping.put("a", 31);
		codeMaping.put("b", 32);
		codeMaping.put("c", 33);
		codeMaping.put("d", 34);
		codeMaping.put("e", 35);
		codeMaping.put("f", 36);
		codeMaping.put("g", 37);
		codeMaping.put("h", 38);
		codeMaping.put("i", 39);
		codeMaping.put("j", 40);
		codeMaping.put("k", 41);
		codeMaping.put("l", 42);
		codeMaping.put("m", 43);
		codeMaping.put("n", 44);
		codeMaping.put("o", 45);
		codeMaping.put("p", 46);
		codeMaping.put("q", 47);
		codeMaping.put("r", 48);
		codeMaping.put("s", 49);
		codeMaping.put("t", 50);
		codeMaping.put("u", 51);
		codeMaping.put("v", 52);
		codeMaping.put("w", 53);
		codeMaping.put("x", 54);
		codeMaping.put("y", 55);
		codeMaping.put("z", 56);

		codeMaping.put("A", -31);
		codeMaping.put("B", -32);
		codeMaping.put("C", -33);
		codeMaping.put("D", -34);
		codeMaping.put("E", -35);
		codeMaping.put("F", -36);
		codeMaping.put("G", -37);
		codeMaping.put("H", -38);
		codeMaping.put("I", -39);
		codeMaping.put("J", -40);
		codeMaping.put("K", -41);
		codeMaping.put("L", -42);
		codeMaping.put("M", -43);
		codeMaping.put("N", -44);
		codeMaping.put("O", -45);
		codeMaping.put("P", -46);
		codeMaping.put("Q", -47);
		codeMaping.put("R", -48);
		codeMaping.put("S", -49);
		codeMaping.put("T", -50);
		codeMaping.put("U", -51);
		codeMaping.put("V", -52);
		codeMaping.put("W", -53);
		codeMaping.put("X", -54);
		codeMaping.put("Y", -55);
		codeMaping.put("Z", -56);

		codeMaping.put("0", 19);
		codeMaping.put("1", 20);
		codeMaping.put("2", 21);
		codeMaping.put("3", 22);
		codeMaping.put("4", 23);
		codeMaping.put("5", 24);
		codeMaping.put("6", 25);
		codeMaping.put("7", 26);
		codeMaping.put("8", 27);
		codeMaping.put("9", 28);

		codeMaping.put(")", -19);
		codeMaping.put("!", -20);
		codeMaping.put("@", -21);
		codeMaping.put("#", -22);
		codeMaping.put("$", -23);
		codeMaping.put("%", -24);
		codeMaping.put("^", -25);
		codeMaping.put("&", -26);
		codeMaping.put("*", -27);
		codeMaping.put("(", -28);

		codeMaping.put(",", 57);
		codeMaping.put(".", 58);
		codeMaping.put("-", 71);
		codeMaping.put("=", 72);
		codeMaping.put("[", 73);
		codeMaping.put("]", 74);
		codeMaping.put("\\", 75);
		codeMaping.put("/", 78);
		codeMaping.put("+", 83);
		codeMaping.put("_", -71);
		codeMaping.put("{", -73);
		codeMaping.put("}", -74);
		codeMaping.put(":", -76);
		codeMaping.put("\"", -77);

		codeMaping.put("?", -78);

		codeMaping.put("Tab", 63);
		codeMaping.put("SPACE", 64);
		codeMaping.put("ENTER", 68);
		codeMaping.put("delete", 69);
	}

}