package com.fiberhome.opticalbox.ui.keyboard810;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.utils.LogUtil;

public class KeyboardInputActivity extends MBaseRCActivity {

	private int keySpacing;
	private boolean letterboard = true;
	private boolean shiftPressed = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.ob_activity_keyboard_input);
		super.onCreate(savedInstanceState);

		findViewById(R.id.btn_key_back).setOnClickListener(onBackClickListener);

		Button btnKeyAt = (Button) findViewById(R.id.btn_key_at);
		btnKeyAt.setText("@");

		KeyboardViewUtil.changeFonts(this, "fonts/RobotoBlack.ttf");
		KeyboardViewUtil.changeTextColor(this, 0xff5c5c5c);
		KeyboardViewUtil.setTouchStyle(this, 0xff5c5c5c, 0xffffffff);
		resetKeySpacing(findViewById(R.id.view_key_spacing));

		findViewById(R.id.btn_key_shift).setOnClickListener(onShiftClickListener);
		findViewById(R.id.btn_key_num).setOnClickListener(onKeyboardSwitchClickListener);

		findViewById(R.id.btn_key_shift).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_q).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_w).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_e).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_r).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_t).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_y).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_u).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_i).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_o).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_p).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_a).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_s).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_d).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_f).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_g).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_h).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_j).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_k).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_l).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_z).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_x).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_c).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_v).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_b).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_n).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_m).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_at).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_space).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_enter).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_underline).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_dot).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_comma).setOnTouchListener(onKeyTouchListener);
		findViewById(R.id.btn_key_tab).setOnTouchListener(onKeyTouchListener);

		findViewById(R.id.btn_key_delete).setOnTouchListener(onKeyDeleteTouchListener);
	}

	private OnTouchListener onKeyTouchListener = new OnTouchListener() {
		@Override
		public boolean onTouch(View view, MotionEvent event) {
			Integer keyCode = null;
			try {
				keyCode = KeyboardCodeUtil.getCodeValue(((Button) view).getText().toString());
			} catch (Exception e) {
				LogUtil.e("KeyboardInputActivity keyCode parse error");
				// showToast("KeyboardInputActivity keyCode parse error");
				return false;
			} finally {
				if (keyCode == null) {
					LogUtil.e("KeyboardInputActivity keyCode null");
					// showToast("KeyboardInputActivity keyCode null");
					return false;
				}
			}
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				((Button) view).setTextColor(0xffffffff);
				
				if (isNeedShiftPressed(keyCode)) {
					sendKey(KeyboardCodeUtil.getCodeValue("shift"), MotionEvent.ACTION_DOWN);
				}
				sendKey(Math.abs(keyCode), MotionEvent.ACTION_DOWN);
				break;
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP:
				((Button) view).setTextColor(0xff5c5c5c);
				
				sendKey(Math.abs(keyCode), MotionEvent.ACTION_UP);
				if (isNeedShiftPressed(keyCode)) {
					sendKey(KeyboardCodeUtil.getCodeValue("shift"), MotionEvent.ACTION_UP);
				}
				break;
			}
			return false;
		}
	};

	private OnTouchListener onKeyDeleteTouchListener = new OnTouchListener() {
		@Override
		public boolean onTouch(View view, MotionEvent event) {
			Integer keyCode = null;
			try {
				keyCode = KeyboardCodeUtil.getCodeValue("delete");
			} catch (Exception e) {
				LogUtil.e("KeyboardInputActivity keyCode parse error");
				// showToast("KeyboardInputActivity keyCode parse error");
				return false;
			} finally {
				if (keyCode == null) {
					LogUtil.e("KeyboardInputActivity keyCode null");
					// showToast("KeyboardInputActivity keyCode null");
					return false;
				}
			}
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				sendKey(keyCode, MotionEvent.ACTION_DOWN);
				break;
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP:
				sendKey(keyCode, MotionEvent.ACTION_UP);
				break;
			}
			return false;
		}
	};

	private boolean isNeedShiftPressed(Integer keyCode) {
		return keyCode < 0;
	}

	private OnClickListener onShiftClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			if (letterboard) {
				shiftPressed = !shiftPressed;
				view.setBackgroundResource(shiftPressed ? R.drawable.ob_layer_list_input_shift_pressed
						: R.drawable.ob_layer_list_input_shift_normal);
				KeyboardViewUtil.changeTextLetterCase(KeyboardInputActivity.this, shiftPressed);
			} else {
				shiftPressed = false;
				view.setBackgroundResource(R.drawable.ob_selector_key_input);
			}
		};
	};

	private OnClickListener onKeyboardSwitchClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			letterboard = !letterboard;
			KeyboardViewUtil.changeKeyboard(KeyboardInputActivity.this, letterboard);
		}
	};

	private void resetKeySpacing(final View demo) {
		demo.post(new Runnable() {
			@Override
			public void run() {
				keySpacing = demo.getWidth();
				KeyboardViewUtil.changeWidth(KeyboardInputActivity.this, getString(R.string.ob_key_spacing), keySpacing);
				resetKeyTabWidth();
			}
		});
	}

	private void resetKeyTabWidth() {
		Button btnKeyShift = (Button) findViewById(R.id.btn_key_shift);
		btnKeyShift.post(new Runnable() {
			@Override
			public void run() {
				int tabWidth = findViewById(R.id.btn_key_shift).getWidth();
				Button btnKeyTab = (Button) findViewById(R.id.btn_key_tab);
				LayoutParams lp = btnKeyTab.getLayoutParams();
				lp.width = tabWidth;
				btnKeyTab.setLayoutParams(lp);
			}
		});
	}
}