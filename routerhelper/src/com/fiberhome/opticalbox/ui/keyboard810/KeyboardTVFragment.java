package com.fiberhome.opticalbox.ui.keyboard810;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.BaseFragment;
import com.fiberhome.opticalbox.utils.LogUtil;

public class KeyboardTVFragment extends BaseFragment {

	private MBaseRCActivity holder;

	private ImageView imgDpadPressed;
	private Button btnKeyMute;
	private boolean isMute = false;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof MBaseRCActivity) {
			holder = (MBaseRCActivity) activity;
		}
	}

	public void sendKey(Integer keyCode, int action) {
		if (holder != null) {
			holder.sendKey(keyCode, action);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.ob_fragment_keyboard_tv, null);

		view.findViewById(R.id.lay_content).setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return true; // shielding gesture touch event
			}
		});

		btnKeyMute = (Button) view.findViewById(R.id.btn_key_mute);
		btnKeyMute.setOnTouchListener(onKeyTouchListener);

		imgDpadPressed = (ImageView) view.findViewById(R.id.img_dpad_pressed);

		view.findViewById(R.id.btn_key_ok).setOnTouchListener(onKeyTouchListener);
		view.findViewById(R.id.btn_key_up).setOnTouchListener(onKeyTouchListener);
		view.findViewById(R.id.btn_key_down).setOnTouchListener(onKeyTouchListener);
		view.findViewById(R.id.btn_key_left).setOnTouchListener(onKeyTouchListener);
		view.findViewById(R.id.btn_key_right).setOnTouchListener(onKeyTouchListener);

		view.findViewById(R.id.btn_key_list).setOnTouchListener(onKeyTouchListener);
		view.findViewById(R.id.btn_key_home).setOnTouchListener(onKeyTouchListener);
		view.findViewById(R.id.btn_key_back).setOnTouchListener(onKeyTouchListener);
		view.findViewById(R.id.btn_key_volume_down).setOnTouchListener(onKeyTouchListener);
		view.findViewById(R.id.btn_key_delete).setOnTouchListener(onKeyTouchListener);
		view.findViewById(R.id.btn_key_volume_up).setOnTouchListener(onKeyTouchListener);

		return view;
	}

	private OnTouchListener onKeyTouchListener = new OnTouchListener() {
		@Override
		public boolean onTouch(View view, MotionEvent event) {
			if (view.getTag() == null) {
				LogUtil.e("KeyboardTVFragment keyCode not set");
				// showToast("KeyboardTVFragment keyCode not set");
				return false;
			}

			int keyCode;
			try {
				keyCode = Integer.parseInt(view.getTag().toString());
			} catch (Exception e) {
				LogUtil.e("KeyboardTVFragment keyCode parse error");
				// showToast("KeyboardTVFragment keyCode parse error");
				return false;
			}
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				sendKey(keyCode, MotionEvent.ACTION_DOWN);

				switch (view.getId()) {
				case R.id.btn_key_up:
					imgDpadPressed.setBackgroundResource(R.drawable.ob_key_tv_dpad_up_pressed);
					break;
				case R.id.btn_key_down:
					imgDpadPressed.setBackgroundResource(R.drawable.ob_key_tv_dpad_down_pressed);
					break;
				case R.id.btn_key_left:
					imgDpadPressed.setBackgroundResource(R.drawable.ob_key_tv_dpad_left_pressed);
					break;
				case R.id.btn_key_right:
					imgDpadPressed.setBackgroundResource(R.drawable.ob_key_tv_dpad_right_pressed);
					break;
				case R.id.btn_key_ok:
					imgDpadPressed.setBackgroundResource(R.drawable.ob_key_tv_dpad_ok_pressed);
					break;
				case R.id.btn_key_mute:
					isMute = !isMute;
					btnKeyMute.setBackgroundResource(isMute ? R.drawable.ob_key_tv_mute_selected : R.drawable.ob_key_tv_mute_normal);
					break;
				}
				break;
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP:
				sendKey(keyCode, MotionEvent.ACTION_UP);

				switch (view.getId()) {
				case R.id.btn_key_up:
				case R.id.btn_key_down:
				case R.id.btn_key_left:
				case R.id.btn_key_right:
				case R.id.btn_key_ok:
					imgDpadPressed.setBackgroundResource(0);
					break;
				}
				break;
			}
			return false;
		}
	};

}
