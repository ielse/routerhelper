package com.fiberhome.opticalbox.ui.keyboard810;

import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.utils.LogUtil;

/**
 * @author wangpeng
 */
@SuppressLint("DefaultLocale")
public class KeyboardViewUtil {

	public static final String letter = "abcdefghijklmnopqrstuvwxyz";
	public static final Map<String, String> letterMaping = new HashMap<String, String>();
	static {
		letterMaping.put("1", "q");
		letterMaping.put("q", "1");
		letterMaping.put("2", "w");
		letterMaping.put("w", "2");
		letterMaping.put("3", "e");
		letterMaping.put("e", "3");
		letterMaping.put("4", "r");
		letterMaping.put("r", "4");
		letterMaping.put("5", "t");
		letterMaping.put("t", "5");
		letterMaping.put("6", "y");
		letterMaping.put("y", "6");
		letterMaping.put("7", "u");
		letterMaping.put("u", "7");
		letterMaping.put("8", "i");
		letterMaping.put("i", "8");
		letterMaping.put("9", "o");
		letterMaping.put("o", "9");
		letterMaping.put("0", "p");
		letterMaping.put("p", "0");
		// second line
		letterMaping.put("a", "#");
		letterMaping.put("#", "a");
		letterMaping.put("s", "$");
		letterMaping.put("$", "s");
		letterMaping.put("d", "%");
		letterMaping.put("%", "d");
		letterMaping.put("f", "&");
		letterMaping.put("&", "f");
		letterMaping.put("g", "*");
		letterMaping.put("*", "g");
		letterMaping.put("h", "-");
		letterMaping.put("-", "h");
		letterMaping.put("j", "+");
		letterMaping.put("+", "j");
		letterMaping.put("k", "(");
		letterMaping.put("(", "k");
		letterMaping.put("l", ")");
		letterMaping.put(")", "l");
		// third line
		letterMaping.put("z", "{");
		letterMaping.put("{", "z");
		letterMaping.put("x", "}");
		letterMaping.put("}", "x");
		letterMaping.put("c", "[");
		letterMaping.put("[", "c");
		letterMaping.put("v", "]");
		letterMaping.put("]", "v");
		letterMaping.put("b", "=");
		letterMaping.put("=", "b");
		letterMaping.put("n", "?");
		letterMaping.put("?", "n");
		letterMaping.put("m", ":");
		letterMaping.put(":", "m");
		letterMaping.put("/", ",");
		letterMaping.put(",", "/");
		// fourth line
		letterMaping.put("abc", "123");
		letterMaping.put("123", "abc");
		letterMaping.put("\"", "_");
		letterMaping.put("_", "\"");
		letterMaping.put(".", ".");
		letterMaping.put(".", ".");
	}

	public static void changeKeyboard(Activity act, boolean letterBoard) {
		changeKeyboard(getContentView(act), act, letterBoard);
	}

	private static void changeKeyboard(ViewGroup root, Activity act, boolean letterBoard) {
		for (int i = 0; i < root.getChildCount(); i++) {
			View v = root.getChildAt(i);
			if (v instanceof ViewGroup) {
				changeKeyboard((ViewGroup) v, act, letterBoard);
			} else {
				if (v instanceof Button) {
					String text = ((Button) v).getText().toString().trim();
					String change = letterMaping.get(text.toLowerCase());
					if (!TextUtils.isEmpty(change)) {
						((Button) v).setText(change);
					} else {
						if (v.getTag() != null && act.getString(R.string.ob_key_shift).equals(v.getTag().toString().trim())) {
							if (letterBoard) {
								v.setBackgroundResource(R.drawable.ob_selector_key_input_shift);
								((Button) v).setText("");
							} else {
								v.setBackgroundResource(R.drawable.ob_selector_key_input);
								((Button) v).setText("!");
							}
						}
					}
				}
			}
		}
	}

	public static void changeWidth(Activity act, String tag, int width) {
		if (act == null || TextUtils.isEmpty(tag) || width < 0) {
			LogUtil.e("KeyboardViewUtil changeWidth() error params");
			return;
		}
		changeWidth(getContentView(act), act, tag, width);
	}

	private static void changeWidth(ViewGroup root, Activity act, String tag, int width) {
		for (int i = 0; i < root.getChildCount(); i++) {
			View v = root.getChildAt(i);
			if (v instanceof ViewGroup) {
				changeWidth((ViewGroup) v, act, tag, width);
			} else {
				if (v.getTag() != null && tag.equals(v.getTag().toString())) {
					LayoutParams lp = v.getLayoutParams();
					lp.width = width;
					v.setLayoutParams(lp);
				}
			}
		}
	}

	public static void changeTextColor(Activity act, int color) {
		changeTextColor(getContentView(act), color);
	}

	private static void changeTextColor(ViewGroup root, int color) {
		for (int i = 0; i < root.getChildCount(); i++) {
			View v = root.getChildAt(i);
			if (v instanceof Button) {
				((Button) v).setTextColor(color);
			} else if (v instanceof ViewGroup) {
				changeTextColor((ViewGroup) v, color);
			}
		}
	}

	public static void setTouchStyle(Activity act, int normalColor, int pressedColor) {
		setTouchStyle(getContentView(act), normalColor, pressedColor);
	}

	private static void setTouchStyle(ViewGroup root, final int normalColor, final int pressedColor) {
		for (int i = 0; i < root.getChildCount(); i++) {
			View v = root.getChildAt(i);
			if (v instanceof Button) {
				((Button) v).setOnTouchListener(new OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						switch (event.getAction()) {
						case MotionEvent.ACTION_DOWN:
							((Button) v).setTextColor(pressedColor);
							break;
						case MotionEvent.ACTION_CANCEL:
						case MotionEvent.ACTION_UP:
							((Button) v).setTextColor(normalColor);
							break;
						}
						return false;
					}
				});
			} else if (v instanceof ViewGroup) {
				setTouchStyle((ViewGroup) v, normalColor, pressedColor);
			}
		}
	}

	/**
	 * 
	 * @param whichCase
	 *            true 大写， false 小写
	 */
	public static void changeTextLetterCase(Activity act, boolean whichCase) {
		changeTextLetterCase(getContentView(act), whichCase);
	}

	private static void changeTextLetterCase(ViewGroup root, boolean whichCase) {
		for (int i = 0; i < root.getChildCount(); i++) {
			View v = root.getChildAt(i);
			if (v instanceof Button) {
				String text = ((Button) v).getText().toString();
				if (text.length() == 1 && letter.contains(text.toLowerCase())) {
					((Button) v).setText(whichCase ? text.toUpperCase() : text.toLowerCase());
				}
			} else if (v instanceof ViewGroup) {
				changeTextLetterCase((ViewGroup) v, whichCase);
			}
		}
	}

	public static void changeFonts(Activity act, String fontsPath) {
		changeFonts(getContentView(act), act, fontsPath);
	}

	private static void changeFonts(ViewGroup root, Activity act, String fontsPath) {
		Typeface tf = Typeface.createFromAsset(act.getAssets(), fontsPath);

		for (int i = 0; i < root.getChildCount(); i++) {
			View v = root.getChildAt(i);
			if (v instanceof Button) {
				((Button) v).setTypeface(tf);
			} else if (v instanceof ViewGroup) {
				changeFonts((ViewGroup) v, act, fontsPath);
			}
		}
	}

	private static ViewGroup getContentView(Activity act) {
		ViewGroup systemContent = (ViewGroup) act.getWindow().getDecorView().findViewById(android.R.id.content);
		ViewGroup content = null;
		if (systemContent.getChildCount() > 0 && systemContent.getChildAt(0) instanceof ViewGroup) {
			content = (ViewGroup) systemContent.getChildAt(0);
		}
		return content;
	}
}
