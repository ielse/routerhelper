package com.fiberhome.opticalbox.ui.keyboard810;

import android.os.Bundle;

import com.amlapp.android.RemoteClient.Key;
import com.amlapp.android.RemoteClient.RemoteClient;
import com.amlapp.android.RemoteClient.SendThread;
import com.amlapp.android.RemoteClient.Service;
import com.fiberhome.opticalbox.C;
import com.fiberhome.opticalbox.utils.LogUtil;

public class MBaseRCActivity extends BaseRCActivity {

	private static Service mService = new Service(C.app.keybordServerIpAddress, "", null, Service.TRUE);
	private RemoteClient mRemoteClient = null;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		connectService();
	}

	protected void connectService() {
		connectService(C.app.keybordServerIpAddress);
	}

	private void connectService(final String ip) {
		new Thread(new Runnable() {
			@Override public void run() {
				LogUtil.e("keyboard : to connect " + ip);
				udpIpsend(ip);

				sendCmdMessage(SendThread.CMD_CONNECT, 0, 0, mService);
			}
		}).start();
	}

	private void udpIpsend(String ip) {
		if (mRemoteClient == null) {
			mRemoteClient = RemoteClient.getinstance();
			mRemoteClient.UdpInit();
		}
		mRemoteClient.UdpIpsend(ip);
	}

	protected void sendKey(Integer keyCode, int action) {
		sendCmdMessage(SendThread.CMD_SEND_KEY, 0, 0, new Key(mService, action, keyCode));
		// vibrator.vibrate(100);
	}

	// protected Handler connectHandler = new Handler() {
	// public void handleMessage(Message msg) {
	// switch (msg.what) {
	// case SendThread.CMD_CONNECT:
	// Service service = (Service) msg.obj;
	// LogUtil.e("connectHandler SendThread.CMD_CONNECT!");
	// LogUtil.e("CONNECT successful ? service :! " + service.getConnected() +
	// "!");
	// // showToast("connectHandler SendThread.CMD_CONNECT");
	// isConnected = service.getConnected();
	// break;
	// }
	// }
	// };

	@Override protected void onDestroy() {
		super.onDestroy();
		if (mRemoteClient != null) {
			mRemoteClient.UdpClose();
			mRemoteClient = null;
		}
	};

}