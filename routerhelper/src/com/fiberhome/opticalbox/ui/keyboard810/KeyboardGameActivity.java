package com.fiberhome.opticalbox.ui.keyboard810;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.utils.LogUtil;

public class KeyboardGameActivity extends MBaseRCActivity {

	private ImageView imgDpadPressed;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.ob_activity_keyboard_game);
		super.onCreate(savedInstanceState);
		findViewById(R.id.lay_back).setOnClickListener(onBackClickListener);

		imgDpadPressed = (ImageView) findViewById(R.id.img_dpad_pressed);

		findViewById(R.id.btn_key_up).setOnTouchListener(onKeyClickListener);
		findViewById(R.id.btn_key_down).setOnTouchListener(onKeyClickListener);
		findViewById(R.id.btn_key_left).setOnTouchListener(onKeyClickListener);
		findViewById(R.id.btn_key_right).setOnTouchListener(onKeyClickListener);
		findViewById(R.id.btn_key_triangle).setOnTouchListener(onKeyClickListener);
		findViewById(R.id.btn_key_circle).setOnTouchListener(onKeyClickListener);
		findViewById(R.id.btn_key_square).setOnTouchListener(onKeyClickListener);
		findViewById(R.id.btn_key_fork).setOnTouchListener(onKeyClickListener);
	}

	private OnTouchListener onKeyClickListener = new OnTouchListener() {
		@Override
		public boolean onTouch(View view, MotionEvent event) {
			if (view.getTag() == null) {
				LogUtil.e("KeyboardGameActivity keyCode not set");
				// showToast("KeyboardGameActivity keyCode not set");
				return false;
			}

			int keyCode;
			try {
				keyCode = Integer.parseInt(view.getTag().toString());
			} catch (Exception e) {
				LogUtil.e("KeyboardGameActivity keyCode parse error");
				// showToast("KeyboardGameActivity keyCode parse error");
				return false;
			}
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				sendKey(keyCode, MotionEvent.ACTION_DOWN);

				switch (view.getId()) {
				case R.id.btn_key_up:
					imgDpadPressed.setBackgroundResource(R.drawable.ob_key_game_dpad_up_pressed);
					break;
				case R.id.btn_key_down:
					imgDpadPressed.setBackgroundResource(R.drawable.ob_key_game_dpad_down_pressed);
					break;
				case R.id.btn_key_left:
					imgDpadPressed.setBackgroundResource(R.drawable.ob_key_game_dpad_left_pressed);
					break;
				case R.id.btn_key_right:
					imgDpadPressed.setBackgroundResource(R.drawable.ob_key_game_dpad_right_pressed);
					break;
				}
				break;
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP:
				sendKey(keyCode, MotionEvent.ACTION_UP);

				switch (view.getId()) {
				case R.id.btn_key_up:
				case R.id.btn_key_down:
				case R.id.btn_key_left:
				case R.id.btn_key_right:
					imgDpadPressed.setBackgroundResource(0);
					break;
				}
				break;
			}
			return false;
		}
	};

}