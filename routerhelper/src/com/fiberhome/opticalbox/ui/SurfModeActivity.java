package com.fiberhome.opticalbox.ui;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.BaseActivity;
import com.fiberhome.opticalbox.net.NetConstant;
import com.fiberhome.opticalbox.net.NetManager.TcpErrorListener;
import com.fiberhome.opticalbox.net.NetManager.TcpListener;
import com.fiberhome.opticalbox.net.bean.Parameter;
import com.fiberhome.opticalbox.net.utils.JsonUtil;
import com.fiberhome.opticalbox.utils.AnimUtil;
import com.fiberhome.opticalbox.utils.NetworkUtil;

/**
 * 上网模式
 * 
 * @author wangpeng
 * 
 */
public class SurfModeActivity extends BaseActivity implements TcpListener {

	private final String SURF_MODE_FAIR = "fair";
	private final String SURF_MODE_GAME = "game";
	private final String SURF_MODE_VIDEO = "video";
	private final String SURF_MODE_WEB = "web";

	private String currSurfMode = SURF_MODE_FAIR;
	private String clickSurfMode;

	private View laySurfModeFair, laySurfModeGame, laySurfModeVideo, laySurfModeWeb;
	private ImageView imgCurrSurfMode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.ob_activity_surf_mode);
		super.onCreate(savedInstanceState);

		titleView.setTitle(R.string.ob_surf_mode);
		titleView.addLeftDrawableMenu(this, R.drawable.ob_ico_back, 15, 25, onBackClickListener);
		imgCurrSurfMode = (ImageView) findViewById(R.id.img_curr_surf_mode);
		laySurfModeFair = findViewById(R.id.lay_surf_mode_fair);
		laySurfModeGame = findViewById(R.id.lay_surf_mode_game);
		laySurfModeVideo = findViewById(R.id.lay_surf_mode_video);
		laySurfModeWeb = findViewById(R.id.lay_surf_mode_web);

		laySurfModeFair.setOnClickListener(onClickListener);
		laySurfModeGame.setOnClickListener(onClickListener);
		laySurfModeVideo.setOnClickListener(onClickListener);
		laySurfModeWeb.setOnClickListener(onClickListener);

		requestGetSurfMode();

		registerChildTcpListener(this);
		registerChildTcpErrorListener(new TcpErrorListener() {
			@Override
			public void onError(String cmdType) {
				showToast(NetworkUtil.isDisconnected(SurfModeActivity.this) ? R.string.ob_error_net : R.string.ob_error_request);
				if (NetConstant.GET_INTERNET_QOS.equals(cmdType)) {
					resetCurrSurfMode();
				}
			}
		});
	}

	private void requestGetSurfMode() {
		netManager.sendTcp(JsonUtil.get_GET_INTERNET_QOS());
	}

	private void requestSetSurfMode() {
		netManager.sendTcp(JsonUtil.get_SET_INTERNET_QOS(clickSurfMode));
	}

	private OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			laySurfModeFair.setBackgroundColor(0xffffffff);
			laySurfModeGame.setBackgroundColor(0xffffffff);
			laySurfModeVideo.setBackgroundColor(0xffffffff);
			laySurfModeWeb.setBackgroundColor(0xffffffff);

			switch (view.getId()) {
			case R.id.lay_surf_mode_fair:
				clickSurfMode = SURF_MODE_FAIR;
				laySurfModeFair.setBackgroundColor(0xffe5e5e5);
				break;
			case R.id.lay_surf_mode_game:
				clickSurfMode = SURF_MODE_GAME;
				laySurfModeGame.setBackgroundColor(0xffe5e5e5);
				break;
			case R.id.lay_surf_mode_video:
				clickSurfMode = SURF_MODE_VIDEO;
				laySurfModeVideo.setBackgroundColor(0xffe5e5e5);
				break;
			case R.id.lay_surf_mode_web:
				clickSurfMode = SURF_MODE_WEB;
				laySurfModeWeb.setBackgroundColor(0xffe5e5e5);
				break;
			}

			if (!clickSurfMode.equals(currSurfMode)) {
				requestSetSurfMode(); // filter same mode request
			}
		}
	};

	@Override
	public void onReceive(String cmdType, Parameter param) {
		if (NetConstant.GET_INTERNET_QOS.equals(cmdType)) {
			currSurfMode = param.QoSMode;
			resetCurrSurfMode();
		} else if (NetConstant.SET_INTERNET_QOS.equals(cmdType)) {
			currSurfMode = clickSurfMode;
			resetCurrSurfMode();
			showToast(R.string.ob_success_set_surf_mode);
		}
	}

	private void resetCurrSurfMode() {
		if (SURF_MODE_FAIR.equals(currSurfMode)) {
			laySurfModeFair.setBackgroundColor(0xffe5e5e5);
			imgCurrSurfMode.setImageResource(R.drawable.ob_curr_surf_mode_fair);
		} else if (SURF_MODE_GAME.equals(currSurfMode)) {
			laySurfModeGame.setBackgroundColor(0xffe5e5e5);
			imgCurrSurfMode.setImageResource(R.drawable.ob_curr_surf_mode_game);
		} else if (SURF_MODE_VIDEO.equals(currSurfMode)) {
			laySurfModeVideo.setBackgroundColor(0xffe5e5e5);
			imgCurrSurfMode.setImageResource(R.drawable.ob_curr_surf_mode_video);
		} else if (SURF_MODE_WEB.equals(currSurfMode)) {
			laySurfModeWeb.setBackgroundColor(0xffe5e5e5);
			imgCurrSurfMode.setImageResource(R.drawable.ob_curr_surf_mode_web);
		}
		startAnimation(imgCurrSurfMode, AnimUtil.init().addAlpha(1f, 0.2f, 0, 600, new LinearInterpolator(), Animation.REVERSE).create());
	}
}