package com.fiberhome.opticalbox.ui;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.fiberhome.opticalbox.R;
import com.fiberhome.airplay.AirPlayClientService;
import com.fiberhome.airplay.jmdns.ServiceInfo;
import com.fiberhome.opticalbox.BaseActivity;
import com.fiberhome.opticalbox.C;
import com.fiberhome.opticalbox.common.FileSearchService;
import com.fiberhome.opticalbox.ui.fragment.TvProjectionFragment;
import com.fiberhome.opticalbox.utils.LogUtil;
import com.fiberhome.opticalbox.utils.ScreenUtil;

public class PutImageActivity extends BaseActivity {

	private ViewPager viewPager;

	private List<File> fileList = new ArrayList<File>();

	private int startPosition;

	private AirPlayClientService clientService;

	public static int screenWidth, screenHeight;

	@Override protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.ob_activity_put_image);
		super.onCreate(savedInstanceState);
		screenWidth = ScreenUtil.getScreenWidth(getApplicationContext());
		screenHeight = ScreenUtil.getScreenHeight(getApplicationContext());

		viewPager = (ViewPager) findViewById(R.id.ob_viewPager);
		viewPager.setAdapter(pagerAdapter);

		startPosition = getIntent().getIntExtra(C.bundle.imagePosition, 0);

		bindFileSearchService();

		// client service
		clientService = AirPlayClientService.getInstance();
		clientService.setCallback(null);

		viewPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override public void onPageSelected(int position) {
				LogUtil.i("ViewPager onPageSelected putImage(" + position + ")");
				putImage(position);
			}

			@Override public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override public void onPageScrollStateChanged(int arg0) {
			}
		});
	}

	private void putImage(int position) {
		ServiceInfo serviceInfo = TvProjectionFragment.getSelectedServiceInfo();
		if (serviceInfo == null) {
			showToast(R.string.ob_scan_3);
			return;
		}
		File file = fileList.get(position);

		LogUtil.i("PutImageActivity putImage() " + file.getName());
		String transition = "Slide right";
		try {
			clientService.putImage(file, serviceInfo, transition);
		} catch (Exception e) {
			showToast(R.string.ob_scan_4);
		}
	}
	
	private void stopImage() {
		ServiceInfo serviceInfo = TvProjectionFragment.getSelectedServiceInfo();
		if (serviceInfo == null) {
			showToast(R.string.ob_scan_3);
			return;
		}
		
		LogUtil.i("PutImageActivity stopImage() " );
		try {
			clientService.stopImage(serviceInfo);
		} catch (Exception e) {
			showToast(R.string.ob_scan_4);
		}
	}

	@Override protected void onDestroy() {
		super.onDestroy();
		unBindFileSearchService();
		
		/* stop image */
		stopImage();
		
		clientService.shutdown();
	}

	private void bindFileSearchService() {
		Intent intent = new Intent(PutImageActivity.this, FileSearchService.class);
		bindService(intent, mServiceConnection, Activity.BIND_AUTO_CREATE);
	}

	private void unBindFileSearchService() {
		unbindService(mServiceConnection);
	}

	private final ServiceConnection mServiceConnection = new ServiceConnection() {
		@Override public void onServiceConnected(ComponentName componentName, IBinder service) {
			FileSearchService fileSearchService = ((FileSearchService.LocalBinder) service).getService();
			fileList = fileSearchService.getImageList();

			viewPager.setCurrentItem(startPosition);
			pagerAdapter.notifyDataSetChanged();

			if (startPosition == 0) {
				// if position is 0, the ViewPager onPageSelected not invoke
				putImage(startPosition);
			}
		}

		@Override public void onServiceDisconnected(ComponentName componentName) {
		}
	};

	public PagerAdapter pagerAdapter = new PagerAdapter() {
		@Override public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView(container.findViewWithTag(position));
		}

		@Override public Object instantiateItem(ViewGroup container, final int position) {
			View content = LayoutInflater.from(PutImageActivity.this).inflate(R.layout.ob_lay_gallery_content, null);
			
			ImageView imageView = (ImageView) content.findViewById(R.id.ob_imageView);
			
			
			Glide.with(PutImageActivity.this).load(fileList.get(position).getAbsolutePath()).into(imageView);
			
			content.setTag(position);
			container.addView(content);
			
			/* quickly release memory */
			//Glide.with(PutImageActivity.this).load(fileList.get(position).getAbsolutePath()).into(imageView).onDestroy();

			return content;
		}

		@Override public int getCount() {
			return fileList.size();
		}

		@Override public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}
	};

}