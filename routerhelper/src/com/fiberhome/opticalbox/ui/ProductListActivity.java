package com.fiberhome.opticalbox.ui;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.ActivityCollector;
import com.fiberhome.opticalbox.App;
import com.fiberhome.opticalbox.BaseActivity;
import com.fiberhome.opticalbox.net.NetConstant;
import com.fiberhome.opticalbox.net.NetManager.TcpErrorListener;
import com.fiberhome.opticalbox.net.NetManager.TcpListener;
import com.fiberhome.opticalbox.net.bean.Parameter;
import com.fiberhome.opticalbox.net.utils.JsonUtil;

/**
 * 
 */
public class ProductListActivity extends BaseActivity {

	private ListView listView;
	private List<String> productList = new ArrayList<String>();
	private View layProductList, layDisconnect;

	private Button btnConnect;
	private ImageView imgConnect, imgProductList;

	@Override protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.ob_activity_product_list);
		super.onCreate(savedInstanceState);

		ActivityCollector.finishOthers(ProductListActivity.class);

		imgProductList = (ImageView) findViewById(R.id.img_product_list);
		((AnimationDrawable) imgProductList.getDrawable()).start();
		imgConnect = (ImageView) findViewById(R.id.img_connect);
		btnConnect = (Button) findViewById(R.id.btn_connect);
		btnConnect.setOnClickListener(onClickListener);

		layProductList = findViewById(R.id.lay_product_list);
		layDisconnect = findViewById(R.id.lay_disconnect);

		listView = (ListView) findViewById(R.id.ob_listView);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(onItemClickListener);

		registerChildTcpListener(tcpListener);
		registerChildTcpErrorListener(tcpErrorListener);

		requestGetProduct();
	}

	private OnClickListener onClickListener = new OnClickListener() {
		@Override public void onClick(View v) {
			requestGetProduct();
		}
	};

	private void requestGetProduct() {
		btnConnect.setVisibility(View.GONE);
		imgConnect.setVisibility(View.VISIBLE);
		netManager.sendTcp(JsonUtil.get_GET_DEVICE_TYPE());
	}

	private TcpListener tcpListener = new TcpListener() {
		@Override public void onReceive(String cmdType, Parameter param) {
			if (NetConstant.GET_DEVICE_TYPE.equals(cmdType)) {
				if (!TextUtils.isEmpty(param.DeviceType)) {
					imgProductList.setVisibility(View.GONE);
					layProductList.setVisibility(View.VISIBLE);
					productList.clear();
					productList.add(param.DeviceType);
					adapter.notifyDataSetChanged();
				}
			}
		}
	};

	private TcpErrorListener tcpErrorListener = new TcpErrorListener() {
		@Override public void onError(String cmdType) {
			if (NetConstant.GET_DEVICE_TYPE.equals(cmdType)) {
				imgProductList.setVisibility(View.GONE);
				layProductList.setVisibility(View.GONE);
				layDisconnect.setVisibility(View.VISIBLE);
				btnConnect.setVisibility(View.VISIBLE);
				imgConnect.setVisibility(View.GONE);

				toConnectBoxWifi();
			}
			// showToast(NetworkUtil.isDisconnected(ProductListActivity.this) ?
			// R.string.ob_error_net : R.string.ob_error_request);
		}
	};

	private void toConnectBoxWifi() {
		Intent intent = new Intent();
		intent.setAction("android.net.wifi.PICK_WIFI_NETWORK");
		intent.putExtra("extra_prefs_show_button_bar", true);
		// intent.putExtra("extra_prefs_set_next_text", "finish");
		// intent.putExtra("extra_prefs_set_back_text", "back");
		intent.putExtra("wifi_enable_next_on_connect", true);
		startActivity(intent);

	}

	private OnItemClickListener onItemClickListener = new OnItemClickListener() {
		@Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			App.i().productType = productList.get(position);
			actionStart(MainActivity.class);
			finish();
		}
	};

	private BaseAdapter adapter = new BaseAdapter() {

		class Viewactivity {
			TextView txName;
		}

		@Override public View getView(int position, View convertView, ViewGroup parent) {
			String productName = (String) getItem(position);
			View view = null;
			Viewactivity viewactivity;
			if (convertView == null) {
				viewactivity = new Viewactivity();
				view = LayoutInflater.from(ProductListActivity.this).inflate(R.layout.ob_list_content_product, null);
				viewactivity.txName = (TextView) view.findViewById(R.id.tx_name);
				view.setTag(viewactivity);
			} else {
				view = convertView;
				viewactivity = (Viewactivity) view.getTag();
			}
			viewactivity.txName.setText(productName);
			return view;
		}

		@Override public long getItemId(int position) {
			return position;
		}

		@Override public Object getItem(int position) {
			return productList.get(position);
		}

		@Override public int getCount() {
			return productList.size();
		}
	};

	@Override public void finish() {
		super.finish();
		overridePendingTransition(R.anim.ob_activity_fade_in, R.anim.ob_activity_scale_exit);
	}

}
