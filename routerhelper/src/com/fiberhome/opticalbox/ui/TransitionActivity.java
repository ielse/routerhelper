package com.fiberhome.opticalbox.ui;

import android.os.Bundle;
import android.os.Handler;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.BaseActivity;

/**
 * 过渡页
 * 
 */
public class TransitionActivity extends BaseActivity {

	private Handler handler = new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.ob_activity_transition);
		super.onCreate(savedInstanceState);

		appParamsLoading();
		transition();
	}

	private void appParamsLoading() {
	}

	private void transition() {
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				actionStart(ProductListActivity.class);
				finish();
			}
		}, 1000);
	}

	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.ob_activity_fade_in, R.anim.ob_activity_scale_exit);
	}

}
