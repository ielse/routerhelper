package com.fiberhome.opticalbox.ui.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.BaseFragment;
import com.fiberhome.opticalbox.ui.MainActivity;
import com.fiberhome.opticalbox.view.TitleView;

public class WifiCtrlFragment extends BaseFragment {

	private MainActivity activity;

	private WifiCtrlMasterFragment masterWifiFragment;
	private WifiCtrlGuesterFragment guesterWifiFragment;

	private ViewPager viewPager;
	private RadioButton rbMasterWifi, rbGuesterWifi;

	private TitleView titleView;

	@Override public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof MainActivity) {
			this.activity = (MainActivity) activity;
		}
	}

	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.ob_fragment_wifi_ctrl, null);

		titleView = (TitleView) view.findViewById(R.id.ob_titleView);
		titleView.setTitle(R.string.ob_wifi_ctrl_1);
		titleView.setBackground(R.drawable.ob_bg_title);
		titleView.addLeftDrawableMenu(activity, R.drawable.ob_ico_back, 15, 25, onMainBackClickListener);

		masterWifiFragment = new WifiCtrlMasterFragment();
		guesterWifiFragment = new WifiCtrlGuesterFragment();

		rbMasterWifi = (RadioButton) view.findViewById(R.id.rb_master_wifi);
		rbMasterWifi.setOnClickListener(onClickListener);
		rbMasterWifi.setChecked(true);
		rbGuesterWifi = (RadioButton) view.findViewById(R.id.rb_guester_wifi);
		rbGuesterWifi.setOnClickListener(onClickListener);
		
		viewPager = (ViewPager) view.findViewById(R.id.ob_viewPager);
		viewPager.setAdapter(new PagerAdapter(getChildFragmentManager()));
		viewPager.setOnPageChangeListener(onPageChangeListener);

		return view;
	}

	private OnClickListener onClickListener = new OnClickListener() {
		@Override public void onClick(View view) {
			switch (view.getId()) {
			case R.id.rb_master_wifi:
				viewPager.setCurrentItem(0);
				break;
			case R.id.rb_guester_wifi:
				viewPager.setCurrentItem(1);
				break;
			}
		}
	};

	private OnPageChangeListener onPageChangeListener = new OnPageChangeListener() {
		@Override public void onPageSelected(int position) {
			rbMasterWifi.setTextColor(Color.parseColor("#777777"));
			rbGuesterWifi.setTextColor(Color.parseColor("#777777"));
			switch (position) {
			case 0:
				rbMasterWifi.setChecked(true);
				rbMasterWifi.setTextColor(Color.parseColor("#FFFFFF"));
				break;
			case 1:
				rbGuesterWifi.setChecked(true);
				rbGuesterWifi.setTextColor(Color.parseColor("#FFFFFF"));
				break;
			}
		}

		@Override public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override public void onPageScrollStateChanged(int arg0) {
		}
	};

	public class PagerAdapter extends FragmentPagerAdapter {

		public PagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override public CharSequence getPageTitle(int position) {
			return "fragment[" + position + "]";
		}

		@Override public int getCount() {
			return 2;
		}

		@Override public Fragment getItem(int position) {
			switch (position) {
			case 0:
				return masterWifiFragment;
			case 1:
				return guesterWifiFragment;
			default:
				return null;
			}
		}
	}
}
