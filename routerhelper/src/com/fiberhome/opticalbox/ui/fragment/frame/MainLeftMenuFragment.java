package com.fiberhome.opticalbox.ui.fragment.frame;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.App;
import com.fiberhome.opticalbox.BaseFragment;
import com.fiberhome.opticalbox.C;
import com.fiberhome.opticalbox.net.NetConstant;
import com.fiberhome.opticalbox.net.NetManager.TcpErrorListener;
import com.fiberhome.opticalbox.net.NetManager.TcpListener;
import com.fiberhome.opticalbox.net.bean.Parameter;
import com.fiberhome.opticalbox.net.utils.JsonUtil;
import com.fiberhome.opticalbox.ui.MainActivity;
import com.fiberhome.opticalbox.ui.ProductListActivity;

/**
 * @description 左侧菜单碎片
 * @author wangpeng
 * @date 2014-12-22
 */
public class MainLeftMenuFragment extends BaseFragment {

	private MainActivity holder;

	private View layTvProjection, layWifiSettings, laySurfSettings, layWifiFastConnect, layIndicatorLight, layDeviceRestart, layFactoryReset, layAbout;
	private CheckBox ckIndicatorLight;

	private View imgWifiFastConnect, imgIndicatorLight, imgDeviceRestart, imgFactoryReset;

	private Dialog dlgFactoryReset, dlgWifiFastConnect, dlgDeviceRestart;

	@Override public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof MainActivity) {
			holder = (MainActivity) activity;
		}
		registerChildTcpListener(tcpListener);
		registerChildTcpErrorListener(tcpErrorListener);
	}

	public enum Which {
		TvProjection, WifiSettings, SurfSettings, About
	}

	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.ob_fragment_left_menu, null);

		layTvProjection = view.findViewById(R.id.lay_tv_projection);
		layWifiSettings = view.findViewById(R.id.lay_wifi_settings);
		laySurfSettings = view.findViewById(R.id.lay_surf_settings);
		layWifiFastConnect = view.findViewById(R.id.lay_wifi_fast_connect);
		layIndicatorLight = view.findViewById(R.id.lay_indicator_light);
		
		ckIndicatorLight = (CheckBox) view.findViewById(R.id.ck_indicator_light);
		layDeviceRestart = view.findViewById(R.id.lay_device_restart);
		layFactoryReset = view.findViewById(R.id.lay_factory_reset);
		layAbout = view.findViewById(R.id.lay_about);

		imgWifiFastConnect = view.findViewById(R.id.img_wifi_fast_connect);
		imgIndicatorLight = view.findViewById(R.id.img_indicator_light);
		imgDeviceRestart = view.findViewById(R.id.img_device_restart);
		imgFactoryReset = view.findViewById(R.id.img_factory_reset);

		leftMenuClickEvent(false);

		layIndicatorLight.setVisibility(View.GONE);
		
		if (C.app.productType_MR810.equals(App.i().productType)) {
			layIndicatorLight.setVisibility(View.VISIBLE);
			view.findViewById(R.id.view_line_indicator_light).setVisibility(View.VISIBLE);
			getIndicatorLight();
		}

		return view;
	}

	private TcpListener tcpListener = new TcpListener() {
		@Override public void onReceive(String cmdType, Parameter param) {
			if (NetConstant.HG_WPSBUTTON_PUSH.equals(cmdType)) {
				handler.postDelayed(new Runnable() {
					@Override public void run() {
						imgWifiFastConnect.setVisibility(View.GONE);
						showToast(R.string.ob_success_fast_connect);
					}
				}, 800);
			} else if (NetConstant.GET_LED_STATUS.equals(cmdType)) {
				ckIndicatorLight.setChecked("ON".equals(param.LEDStatus));
			} else if (NetConstant.SET_LED_STATUS.equals(cmdType)) {
				handler.postDelayed(new Runnable() {
					@Override public void run() {
						imgIndicatorLight.setVisibility(View.GONE);
						ckIndicatorLight.setVisibility(View.VISIBLE);
					}
				}, 800);
			} else if (NetConstant.HG_COMMAND_REBOOT.equals(cmdType)) {
				showToast(R.string.ob_success_reboot);
				handler.postDelayed(new Runnable() {
					@Override public void run() {
						imgDeviceRestart.setVisibility(View.GONE);
						actionStart(ProductListActivity.class);
						finishAttachActivity();
					}
				}, 3000);
			} else if (NetConstant.HG_LOCAL_REVOCERY.equals(cmdType)) {
				showToast(R.string.ob_success_local_reset);
				handler.postDelayed(new Runnable() {
					@Override public void run() {
						imgDeviceRestart.setVisibility(View.GONE);
						actionStart(ProductListActivity.class);
						finishAttachActivity();
					}
				}, 3000);
			}
		}
	};

	private TcpErrorListener tcpErrorListener = new TcpErrorListener() {
		@Override public void onError(String cmdType) {
			if (NetConstant.HG_WPSBUTTON_PUSH.equals(cmdType)) {
				handler.postDelayed(new Runnable() {
					@Override public void run() {
						imgWifiFastConnect.setVisibility(View.GONE);
						showToast(R.string.ob_fail_fast_connect);
					}
				}, 800);
			} else if (NetConstant.GET_LED_STATUS.equals(cmdType)) {
				// do nothing
			} else if (NetConstant.SET_LED_STATUS.equals(cmdType)) {
				handler.postDelayed(new Runnable() {
					@Override public void run() {
						imgIndicatorLight.setVisibility(View.GONE);
						ckIndicatorLight.setVisibility(View.VISIBLE);
						ckIndicatorLight.setChecked(!ckIndicatorLight.isChecked());
						showToast(R.string.ob_fail_indicator_light);
					}
				}, 800);
			} else if (NetConstant.HG_COMMAND_REBOOT.equals(cmdType)) {
				handler.postDelayed(new Runnable() {
					@Override public void run() {
						imgDeviceRestart.setVisibility(View.GONE);
						showToast(R.string.ob_error_request);
					}
				}, 800);
			} else if (NetConstant.HG_LOCAL_REVOCERY.equals(cmdType)) {
				handler.postDelayed(new Runnable() {
					@Override public void run() {
						imgFactoryReset.setVisibility(View.GONE);
						showToast(R.string.ob_error_request);
					}
				}, 800);
			}
		}
	};

	private OnClickListener onClickListener = new OnClickListener() {
		@Override public void onClick(View view) {
			switch (view.getId()) {
			case R.id.lay_tv_projection:
				tvProjection();
				break;
			case R.id.lay_wifi_settings:
				wifiSettings();
				break;
			case R.id.lay_surf_settings:
				surfSettings();
				break;
			case R.id.lay_wifi_fast_connect:
				wifiFastConnect();
				break;
			case R.id.ck_indicator_light:
				setIndicatorLight(ckIndicatorLight.isChecked());
				break;
			case R.id.lay_device_restart:
				deviceRestart();
				break;
			case R.id.lay_factory_reset:
				factoryReset();
				break;
			case R.id.lay_about:
				about();
				break;
			}
		}
	};

	private void tvProjection() {
		if (holder != null) {
			holder.backToContent(Which.TvProjection);
		}
	}

	private void wifiSettings() {
		if (holder != null) {
			holder.backToContent(Which.WifiSettings);
		}
	}

	private void about() {
		if (holder != null) {
			holder.backToContent(Which.About);
		}
	}

	private void surfSettings() {
		if (holder != null) {
			holder.backToContent(Which.SurfSettings);
		}
	}

	private void setIndicatorLight(boolean toLight) {
		if (holder != null && netManager != null) {
			imgIndicatorLight.setVisibility(View.VISIBLE);
			ckIndicatorLight.setVisibility(View.INVISIBLE);
			netManager.sendTcp(JsonUtil.get_SET_LED_STATUS(toLight ? "ON" : "OFF"));
		}
	}

	private void getIndicatorLight() {
		if (netManager != null) {
			netManager.sendTcp(JsonUtil.get_GET_LED_STATUS());
		}
	}

	private void factoryReset() {
		if (holder != null) {
			dlgFactoryReset = dialogUtil.createSimple(R.string.ob_dlg_factory_reset, true, true).setConfirmListener(new OnClickListener() {
				@Override public void onClick(View view) {
					if (netManager != null) {
						dlgFactoryReset.dismiss();
						imgFactoryReset.setVisibility(View.VISIBLE);
						netManager.sendTcp(JsonUtil.set_HG_LOCAL_REVOCERY());
					}
				}
			}).show();
		}
	}

	private void deviceRestart() {
		if (holder != null) {
			dlgDeviceRestart = dialogUtil.createSimple(R.string.ob_dlg_device_restart, true, true)
					.setConfirmListener(new OnClickListener() {
						@Override public void onClick(View view) {
							if (netManager != null) {
								dlgDeviceRestart.dismiss();
								imgDeviceRestart.setVisibility(View.VISIBLE);
								netManager.sendTcp(JsonUtil.set_HG_COMMAND_REBOOT());
							}
						}
					}).show();
		}

	}

	private void wifiFastConnect() {
		if (holder != null) {
			dlgWifiFastConnect = dialogUtil.createSimple(R.string.ob_dlg_wifi_fast_connect, true, true)
					.setConfirmListener(new OnClickListener() {
						@Override public void onClick(View view) {
							if (netManager != null) {
								dlgWifiFastConnect.dismiss();
								imgWifiFastConnect.setVisibility(View.VISIBLE);
								netManager.sendTcp(JsonUtil.get_HG_WPSBUTTON_PUSH());
							}
						}
					}).show();
		}
	}

	public void leftMenuClickEvent(boolean able) {
		layTvProjection.setOnClickListener(able ? onClickListener : null);
		layWifiSettings.setOnClickListener(able ? onClickListener : null);
		laySurfSettings.setOnClickListener(able ? onClickListener : null);
		layWifiFastConnect.setOnClickListener(able ? onClickListener : null);
		ckIndicatorLight.setOnClickListener(able ? onClickListener : null);
		layDeviceRestart.setOnClickListener(able ? onClickListener : null);
		layFactoryReset.setOnClickListener(able ? onClickListener : null);
		layAbout.setOnClickListener(able ? onClickListener : null);
	}
}
