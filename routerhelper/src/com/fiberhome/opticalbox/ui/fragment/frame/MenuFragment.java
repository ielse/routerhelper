package com.fiberhome.opticalbox.ui.fragment.frame;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.BaseFragment;
import com.fiberhome.opticalbox.ui.fragment.AboutFragment;
import com.fiberhome.opticalbox.ui.fragment.SurfSettingsFragment;
import com.fiberhome.opticalbox.ui.fragment.TvProjectionFragment;
import com.fiberhome.opticalbox.ui.fragment.WifiCtrlFragment;
import com.fiberhome.opticalbox.ui.fragment.frame.MainLeftMenuFragment.Which;

/**
 * @description 左侧菜单点击出现的碎片
 * @author wangpeng
 * @date 2014-12-22
 */
public class MenuFragment extends BaseFragment {

	private TvProjectionFragment tvProjectionFragment;
	private AboutFragment aboutFragment;
	private SurfSettingsFragment surfSettingsFragment;
	private WifiCtrlFragment wifiCtrlFragment;

	private FragmentManager fragmentManager;

	private Which current;

	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.ob_fragment_menu, null);

		fragmentManager = getChildFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
//		tvProjectionFragment = new TvProjectionFragment();
//		wifiCtrlFragment = new WifiCtrlFragment();
//		surfSettingsFragment = new SurfSettingsFragment();
//		aboutFragment = new AboutFragment();
//		transaction.add(R.id.layout_content, aboutFragment);
//		transaction.add(R.id.layout_content, tvProjectionFragment);
//		transaction.add(R.id.layout_content, wifiCtrlFragment);
//		transaction.add(R.id.layout_content, surfSettingsFragment);
//		hideFragments(transaction);
		transaction.commit();

		return view;
	}

	public void setContent(Which which) {
		if (fragmentManager == null || which == current) {
			return;
		}
		current = which;
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		hideFragments(transaction);
		switch (current) {
		case TvProjection:
			if (tvProjectionFragment == null) {
				tvProjectionFragment = new TvProjectionFragment();
				transaction.add(R.id.layout_content, tvProjectionFragment);
			}

			transaction.show(tvProjectionFragment);
			break;
		case WifiSettings:
			if (wifiCtrlFragment == null) {
				wifiCtrlFragment = new WifiCtrlFragment();
				transaction.add(R.id.layout_content, wifiCtrlFragment);
			}
			transaction.show(wifiCtrlFragment);
			break;
		case SurfSettings:
			if (surfSettingsFragment == null) {
				surfSettingsFragment = new SurfSettingsFragment();
				transaction.add(R.id.layout_content, surfSettingsFragment);
			}
			transaction.show(surfSettingsFragment);
			break;
		case About:
			if (aboutFragment == null) {
				aboutFragment = new AboutFragment();
				transaction.add(R.id.layout_content, aboutFragment);
			}
			transaction.show(aboutFragment);
			break;
		}
		transaction.commit();
	}

	private void hideFragments(FragmentTransaction transaction) {
		if (tvProjectionFragment != null) {
			transaction.hide(tvProjectionFragment);
		}
		if (aboutFragment != null) {
			transaction.hide(aboutFragment);
		}
		if (wifiCtrlFragment != null) {
			transaction.hide(wifiCtrlFragment);
		}
		if (surfSettingsFragment != null) {
			transaction.hide(surfSettingsFragment);
		}
	}
}
