package com.fiberhome.opticalbox.ui.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.fiberhome.opticalbox.R;
import com.fiberhome.opticalbox.BaseFragment;
import com.fiberhome.opticalbox.net.NetConstant;
import com.fiberhome.opticalbox.net.NetManager.TcpErrorListener;
import com.fiberhome.opticalbox.net.NetManager.TcpListener;
import com.fiberhome.opticalbox.net.bean.Parameter;
import com.fiberhome.opticalbox.net.utils.JsonUtil;
import com.fiberhome.opticalbox.ui.MainActivity;
import com.fiberhome.opticalbox.ui.ProductListActivity;
import com.fiberhome.opticalbox.utils.AnimUtil;

public class WifiCtrlGuesterFragment extends BaseFragment {

	private MainActivity holder;

	private TextView txName;
	private CheckBox ckWifiAble, ckSsidHidden;
	private Button btnSave;
	private View layWrapWifi;
	private Dialog dlgWifiCtrl;

	private String SSID, PWD, ENCRYPT;

	@Override public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof MainActivity) {
			holder = (MainActivity) activity;
		}
		registerChildTcpListener(tcpListener);
		registerChildTcpErrorListener(tcpErrorListener);
	}

	@Override public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.ob_fragment_wifi_ctrl_guester, null);

		ckWifiAble = (CheckBox) view.findViewById(R.id.ck_wifi_able);
		ckSsidHidden = (CheckBox) view.findViewById(R.id.ck_ssid_hidden);
		txName = (TextView) view.findViewById(R.id.tx_name);
		btnSave = (Button) view.findViewById(R.id.btn_save);
		layWrapWifi = view.findViewById(R.id.lay_wrap_wifi);

		ckWifiAble.setOnCheckedChangeListener(wifiAbleListener);
		btnSave.setOnClickListener(onSaveListener);

		ckWifiAble.setChecked(false);

		requestGetWifiInfo();
		return view;
	}

	private OnCheckedChangeListener wifiAbleListener = new OnCheckedChangeListener() {
		@Override public void onCheckedChanged(CompoundButton arg0,
				boolean checked) {
			if (checked) {
				holder.startAnimation(layWrapWifi,
						AnimUtil.init().addAlpha(0f, 1f).create());
			} else {
				holder.endAnimation(layWrapWifi,
						AnimUtil.init().addAlpha(1f, 0f).create());
			}
		}
	};

	private OnClickListener onSaveListener = new OnClickListener() {
		@Override public void onClick(View view) {
			showSetWifiCtrlDialog();
		}
	};

	private void requestGetWifiInfo() {
		if (netManager != null) {
			String WifiIndex = "1"; //
			String SSIDIndex = "2"; // 私人"1",共有"2"
			netManager
					.sendTcp(JsonUtil.get_GET_WIFI_INFO(WifiIndex, SSIDIndex));
		}
	}

	private void requestSetWifiInfo() {
		if (netManager != null) {
			String WifiIndex = "1"; //
			String PowerLevel = "100"; // 此SSID的网络功耗（百分比表示，取值0-100）
			String Channel = "0"; // 0表示Auto
			String WifiCmdMode = "0"; /* 0表示设置AP相关操作，1表示对关联其他AP动作 */
			String SSIDIndex = "2"; // 私人"1",共有"2"
			String Enable = ckWifiAble.isChecked() ? "1" : "0"; // 1表示启用，0表示不启用
			String Hidden = ckSsidHidden.isChecked() ? "1" : "0"; // 1表示隐藏，0表示不隐藏
			netManager.sendTcp(JsonUtil.get_SET_WIFI_INFO(WifiIndex, SSIDIndex,
					SSID, PWD, ENCRYPT, PowerLevel, Channel, Enable,
					WifiCmdMode, Hidden));
		}
	}

	private TcpListener tcpListener = new TcpListener() {
		@Override public void onReceive(String cmdType, Parameter param) {
			if (NetConstant.GET_WIFI_INFO.equals(cmdType)) {
				if ("2".equals(param.SSIDIndex)) {
					SSID = param.SSID;
					PWD = param.PWD;
					ENCRYPT = param.ENCRYPT;
					ckWifiAble.setChecked("1".equals(param.Enable));
					txName.setText(param.SSID);
					ckSsidHidden.setChecked("1".equals(param.Hidden));
				}
			} else if (NetConstant.SET_WIFI_INFO.equals(cmdType)) {
				showToast(R.string.ob_success_set_wifi_ctrl_1);
				new Handler().postDelayed(new Runnable() {
					@Override public void run() {

						actionStart(ProductListActivity.class);
						finishAttachActivity();
					}
				}, 20000);
			}
		}
	};

	private TcpErrorListener tcpErrorListener = new TcpErrorListener() {
		@Override public void onError(String cmdType) {
			if (NetConstant.GET_WIFI_INFO.equals(cmdType)) {
				// no nothing
			} else if (NetConstant.SET_WIFI_INFO.equals(cmdType)) {
				showToast(R.string.ob_fail_set_wifi_info);
			}
		}
	};

	private void showSetWifiCtrlDialog() {
		dlgWifiCtrl = dialogUtil
				.createSimple(R.string.ob_dlg_wifi_ctrl_reset, true, true)
				.setConfirmListener(new OnClickListener() {
					@Override public void onClick(View v) {
						dlgWifiCtrl.dismiss();
						showLoadingDialog();
						requestSetWifiInfo();
					}
				}).show();
	}

	private void showLoadingDialog() {
		dialogUtil.createLoading(R.string.ob_wait_set_wifi_mode_1, false, true)
				.show();
	}
}
