package com.fiberhome.opticalbox.ui.fragment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.fiberhome.opticalbox.R;
import com.fiberhome.airplay.jmdns.ServiceInfo;
import com.fiberhome.opticalbox.BaseFragment;
import com.fiberhome.opticalbox.C;
import com.fiberhome.opticalbox.common.FileSearchService;
import com.fiberhome.opticalbox.ui.MainActivity;
import com.fiberhome.opticalbox.ui.PutImageActivity;
import com.fiberhome.opticalbox.utils.ScreenUtil;

public class TvProjectionImageFragment extends BaseFragment {

	private MainActivity holder;

	@Override public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof MainActivity) {
			holder = (MainActivity) activity;
		}
	}

	private GridView gridView;
	private List<File> fileList = new ArrayList<File>();

	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.ob_fragment_tv_projection_image, null);
		gridView = (GridView) view.findViewById(R.id.ob_gridView);
		gridView.setAdapter(adapter);
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ServiceInfo serviceInfo = TvProjectionFragment.getSelectedServiceInfo();
				if (serviceInfo == null) {
					TvProjectionFragment tpFragment = (TvProjectionFragment) getParentFragment();
					tpFragment.animTitleInfoShake();
				} else {
					actionStart(PutImageActivity.class, C.bundle.imagePosition, position);
				}
			}
		});

		bindFileSearchService();
		registerFileSearchReceiver();

		return view;
	}

	@Override public void onDestroy() {
		super.onDestroy();
		unBindFileSearchService();
		unregisterFileSearchReceiver();
	}

	private void bindFileSearchService() {
		if (holder != null) {
			Intent intent = new Intent(holder, FileSearchService.class);
			holder.bindService(intent, mServiceConnection, Activity.BIND_AUTO_CREATE);
		}
	}

	private void unBindFileSearchService() {
		if (holder != null) {
			holder.unbindService(mServiceConnection);
		}
	}

	private final ServiceConnection mServiceConnection = new ServiceConnection() {
		@Override public void onServiceConnected(ComponentName componentName, IBinder service) {
			FileSearchService fileSearchService = ((FileSearchService.LocalBinder) service).getService();
			fileList = fileSearchService.getImageList();
			adapter.notifyDataSetChanged();
		}

		@Override public void onServiceDisconnected(ComponentName componentName) {
		}
	};

	private void registerFileSearchReceiver() {
		if (holder != null) {
			IntentFilter intentFilter = new IntentFilter();
			intentFilter.addAction(FileSearchService.ACTION_SEARCHED_IMAGE);
			holder.registerReceiver(fileSearchReceiver, intentFilter);
		}
	}

	private void unregisterFileSearchReceiver() {
		if (holder != null) {
			holder.unregisterReceiver(fileSearchReceiver);
		}
	}

	private final BroadcastReceiver fileSearchReceiver = new BroadcastReceiver() {

		@Override public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();
			if (FileSearchService.ACTION_SEARCHED_IMAGE.equals(action)) {
				adapter.notifyDataSetChanged();
			}
		}
	};

	public BaseAdapter adapter = new BaseAdapter() {

		@Override public int getCount() {
			return fileList.size();
		}

		@Override public Object getItem(int position) {
			return fileList.get(position);
		}

		@Override public long getItemId(int position) {
			return position;
		}

		@Override public View getView(int position, View convertView, ViewGroup parent) {
			File file = fileList.get(position);
			ViewHolder viewHolder;
			if (convertView != null) {
				viewHolder = (ViewHolder) convertView.getTag();
			} else {
				viewHolder = new ViewHolder();

				convertView = LayoutInflater.from(holder).inflate(R.layout.ob_list_content_tv_projection_image, null);
				viewHolder.imageView = (ImageView) convertView.findViewById(R.id.ob_imageView);

				LayoutParams para = viewHolder.imageView.getLayoutParams();
				para.height = ScreenUtil.getScreenHeight(holder) / 6;
				viewHolder.imageView.setLayoutParams(para);

				convertView.setTag(viewHolder);
			}

			Glide.with(TvProjectionImageFragment.this).load(file.getAbsolutePath()).centerCrop()
					.placeholder(R.drawable.ob_tv_image_default).crossFade().into(viewHolder.imageView);

			return convertView;
		}

		class ViewHolder {
			ImageView imageView;
		}
	};

}
