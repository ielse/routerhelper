package com.fiberhome.airplay;

import java.io.File;
import java.net.URL;

/**
 * Interface used as a callback to deliver notifications from AirPlay client
 * service back to UI.
 * 
 * @author Tuomas Tikka
 */
public interface AirPlayClientCallback {

	public void onPutImageSuccess(File file);

	public void onPutImageError(File file);

	public void onStopImageSuccess();

	public void onStopImageError();

	public void onPlayVideoSuccess(URL location);

	public void onPlayVideoError(URL location);

	public void onRateVideoSuccess(String state);

	public void onRateVideoError(String state);
	
	public void onSeekVideoSuccess(float seconds);

	public void onSeekVideoError(float seconds);

	public void onStopVideoSuccess();

	public void onStopVideoError();

}
