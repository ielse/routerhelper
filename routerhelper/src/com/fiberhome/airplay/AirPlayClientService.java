package com.fiberhome.airplay;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.fiberhome.airplay.jmdns.ServiceInfo;
import com.fiberhome.opticalbox.App;
import com.fiberhome.opticalbox.utils.ImageUtil;
import com.fiberhome.opticalbox.utils.LogUtil;

/**
 * Class used to communicate to the AirPlay-enabled device.
 * 
 * @author Tuomas Tikka
 */
@SuppressWarnings("deprecation")
public class AirPlayClientService {

	private AirPlayClientService() {
	}

	private static AirPlayClientService airPlayClientService = new AirPlayClientService();

	public static AirPlayClientService getInstance() {
		if (airPlayClientService.es == null) {
			airPlayClientService.es = Executors.newSingleThreadExecutor();
		}
		return airPlayClientService;
	}

	// executor service for asynchronous tasks to the service
	private ExecutorService es;
	// callback back to UI
	private AirPlayClientCallback callback;

	public void setCallback(AirPlayClientCallback callback) {
		this.callback = callback;
	}

	public void shutdown() {
		if (es != null) {
			es.shutdown();
			es = null;
		}
	}

	/** 推图片 */
	public void putImage(File file, ServiceInfo serviceInfo, String transition) throws Exception {
		if (serviceInfo == null) {
			throw new Exception("Not connected to AirPlay service");
		}
		es.submit(new PutImageTask(file, serviceInfo, transition));
	}

	/** 停止推图片 */
	public void stopImage(ServiceInfo serviceInfo) throws Exception {
		if (serviceInfo == null) {
			throw new Exception("Not connected to AirPlay service");
		}
		es.submit(new StopImageTask(serviceInfo));
	}

	/** 视频播放 */
	public void playVideo(URL location, ServiceInfo serviceInfo) throws Exception {
		if (serviceInfo == null) {
			throw new Exception("Not connected to AirPlay service");
		}
		es.submit(new PlayVideoTask(location, serviceInfo));
	}

	/** 视频停止 */
	public void stopVideo(ServiceInfo serviceInfo) throws Exception {
		if (serviceInfo == null) {
			throw new Exception("Not connected to AirPlay service");
		}
		es.submit(new StopVideoTask(serviceInfo));
	}

	/** 视频暂停，继续 */
	public void rateVideo(ServiceInfo serviceInfo, String state) throws Exception {
		if (serviceInfo == null) {
			throw new Exception("Not connected to AirPlay service");
		}
		es.submit(new RateVideoTask(serviceInfo, state));
	}

	/** 视频快进，后退 */
	public void seekVideo(ServiceInfo serviceInfo, float seconds) throws Exception {
		if (serviceInfo == null) {
			throw new Exception("Not connected to AirPlay service");
		}
		es.submit(new SeekVideoTask(serviceInfo, seconds));
	}
	
	/** 初始化视频组件 */
	public void initVideo(ServiceInfo serviceInfo) throws Exception {
		if (serviceInfo == null) {
			throw new Exception("Not connected to AirPlay service");
		}
		es.submit(new ReverseConnectionTask(serviceInfo));
	}

	private class PutImageTask implements Runnable {
		private File file;

		private ServiceInfo serviceInfo;

		private String transition;

		public PutImageTask(File file, ServiceInfo serviceInfo, String transition) {
			this.file = file;
			this.serviceInfo = serviceInfo;
			this.transition = transition;
		}

		public int findBestSampleSize(int actualWidth, int actualHeight, int desiredWidth, int desiredHeight) {
			double wr = (double) actualWidth / desiredWidth;
			double hr = (double) actualHeight / desiredHeight;
			double ratio = Math.min(wr, hr);
			float n = 1.0f;
			while ((n * 2) <= ratio) {
				n *= 2;
			}

			return (int) n;
		}

		public int getResizedDimension(int maxPrimary, int maxSecondary, int actualPrimary, int actualSecondary) {
			// If no dominant value at all, just return the actual.
			if (maxPrimary == 0 && maxSecondary == 0) {
				return actualPrimary;
			}

			// If primary is unspecified, scale primary to match secondary's
			// scaling ratio.
			if (maxPrimary == 0) {
				double ratio = (double) maxSecondary / (double) actualSecondary;
				return (int) (actualPrimary * ratio);
			}

			if (maxSecondary == 0) {
				return maxPrimary;
			}

			double ratio = (double) actualSecondary / (double) actualPrimary;
			int resized = maxPrimary;
			if (resized * ratio > maxSecondary) {
				resized = (int) (maxSecondary / ratio);
			}
			return resized;
		}

		public Bitmap getScaledBitmap(String filePath, int maxWidth, int maxHeight) {
			BitmapFactory.Options decodeOptions = new BitmapFactory.Options();
			Bitmap bitmap = null;
			// If we have to resize this image, first get the natural bounds.
			decodeOptions.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(filePath, decodeOptions);
			int actualWidth = decodeOptions.outWidth;
			int actualHeight = decodeOptions.outHeight;

			// Then compute the dimensions we would ideally like to decode to.
			int desiredWidth = getResizedDimension(maxWidth, maxHeight, actualWidth, actualHeight);
			int desiredHeight = getResizedDimension(maxHeight, maxWidth, actualHeight, actualWidth);

			// test
//			desiredWidth = desiredWidth / 2;
//			desiredHeight = desiredHeight / 2;

			// Decode to the nearest power of two scaling factor.
			decodeOptions.inJustDecodeBounds = false;
			// to do (ficus): Do we need this or is it okay since API 8 doesn't
			// support it?
			// decodeOptions.inPreferQualityOverSpeed =
			// PREFER_QUALITY_OVER_SPEED;
			decodeOptions.inSampleSize = findBestSampleSize(actualWidth, actualHeight, desiredWidth, desiredHeight);
			Bitmap tempBitmap = BitmapFactory.decodeFile(filePath, decodeOptions);
			// If necessary, scale down to the maximal acceptable size.
			if (tempBitmap != null && (tempBitmap.getWidth() > desiredWidth || tempBitmap.getHeight() > desiredHeight)) {
				bitmap = Bitmap.createScaledBitmap(tempBitmap, desiredWidth, desiredHeight, true);
				tempBitmap.recycle();
			} else {
				bitmap = tempBitmap;
			}
			return bitmap;
		}

		@Override public void run() {
			try {
				LogUtil.e("PutImageTask run() file length:" + (file.length() / 1024) + "kb");
				InputStream in = null;
				if (file.length() / 1024 > 1024) {
					Bitmap bitmap = getScaledBitmap(file.getAbsolutePath(), App.i().getScreenWidth(), App.i().getScreenHeight());
					in = ImageUtil.compressBitmap2InputStream(bitmap);
				}else {
					in = new BufferedInputStream(new FileInputStream(file));
				}
				LogUtil.e("PutImageTask run() in available:" + (in.available() / 1024) + "kb");
				URL url = new URL(serviceInfo.getURL() + "/photo");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true);
				conn.setDoOutput(true);
				conn.setConnectTimeout(20 * 1000);
				conn.setReadTimeout(20 * 1000);

				conn.setRequestMethod("PUT");
				conn.setRequestProperty("Content-Length", "" + in.available());
				conn.setRequestProperty("X-Apple-AssetKey", UUID.randomUUID().toString());
				conn.setRequestProperty("X-Apple-Session-ID", UUID.randomUUID().toString());
				conn.setRequestProperty("X-Apple-Transition", transition);
				conn.setRequestProperty("User-Agent", "MediaControl/1.0");

				BufferedOutputStream out = new BufferedOutputStream(conn.getOutputStream());
				byte[] buffer = new byte[32 * 1024];
				int i;
				while ((i = in.read(buffer)) != -1) {				
					out.write(buffer, 0, i);
					out.flush();
				}
				in.close();
				out.flush();
				out.close();
				int status = conn.getResponseCode();
				if (status == 200) {
					LogUtil.d("AirPlayClientService PutImageTask service responded 200");
					if (callback != null) {
						callback.onPutImageSuccess(file);
					}
				} else {
					LogUtil.e("AirPlayClientService PutImageTask service responded " + status);
					if (callback != null) {
						callback.onPutImageError(file);
					}
				}
			} catch (Exception e) {
				LogUtil.e("AirPlayClientService PutImageTask exception \n" + e.getMessage());
				if (callback != null) {
					callback.onPutImageError(file);
				}
			}
		}
	}

	private class StopImageTask implements Runnable {

		private ServiceInfo serviceInfo;

		public StopImageTask(ServiceInfo serviceInfo) {
			this.serviceInfo = serviceInfo;
		}

		@Override public void run() {
			LogUtil.d("AirPlayClientService StopImageTask run");
			try {
				/*************************************************************************************
				 * POST /stop HTTP/1.1 
				 * User-Agent: iTunes/10.6 (Macintosh; IntelMac OS X 10.7.3) AppleWebKit/535.18.5 Content-Length: 0
				 *************************************************************************************/
				URL url = new URL(serviceInfo.getURL() + "/stop");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setConnectTimeout(15 * 1000);
				conn.setReadTimeout(15 * 1000);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Length", "0");
				conn.setRequestProperty("User-Agent", "MediaControl/1.0");
				int status = conn.getResponseCode();
				if (status == 200) {
					LogUtil.d("AirPlayClientService StopImageTask service responded 200");
					if (callback != null) {
						callback.onStopImageSuccess();
					}
				} else {
					LogUtil.e("AirPlayClientService StopImageTask service responded " + status);
					if (callback != null) {
						callback.onStopImageError();
					}
				}
			} catch (Exception e) {
				LogUtil.e("AirPlayClientService StopImageTask exception \n" + e.getMessage());
				if (callback != null) {
					callback.onStopImageError();
				}
			}
		}
	}
	
    /* 与服务器进行协商，只协商一次即可 。*/
	private class ReverseConnectionTask implements Runnable {

		private ServiceInfo serviceInfo;

		public ReverseConnectionTask(ServiceInfo serviceInfo) {
			this.serviceInfo = serviceInfo;
		}
		/*************************************************************************************
			POST /reverse
			Upgrade: PTTH/1.0
			Connection: Upgrade
			X-Apple-Purpose: event
			Content-Length: 0
			User-Agent: MediaControl/1.0
			X-Apple-Session-ID: 1bd6ceeb-fffd-456c-a09c-996053a7a08c
		*************************************************************************************/
		@Override 
		public void run() {
			LogUtil.d("AirPlayClientService ReverseConnectionTask\n ");
			try {
				URL url = new URL(serviceInfo.getURL() + "/reverse");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true);
				conn.setDoOutput(true);
				conn.setConnectTimeout(15 * 1000);
				conn.setReadTimeout(15 * 1000);
				conn.setRequestMethod("POST");

				conn.setRequestProperty("Upgrade", "PTTH/1.0");
				conn.setRequestProperty("Connection", "Upgrade");
				conn.setRequestProperty("X-Apple-Purpose", "event");
				conn.setRequestProperty("Content-Length", "0");
				conn.setRequestProperty("User-Agent", "MediaControl/1.0");
				conn.setRequestProperty("X-Apple-Session-ID", UUID.randomUUID().toString());

				BufferedOutputStream out = new BufferedOutputStream(conn.getOutputStream());
				out.close();
				int status = conn.getResponseCode();
				if (status == 101) {
					LogUtil.d("AirPlayClientService ReverseConnectionTask 101 ");
				} else {
					LogUtil.e("AirPlayClientService ReverseConnectionTask " + status);
				}
			} catch (Exception e) {
				LogUtil.e("AirPlayClientService ReverseConnectionTask exception \n " + e.getMessage());
			}
		}
	}
	
	private class PlayVideoTask implements Runnable {
		private URL location;

		private ServiceInfo serviceInfo;

		public PlayVideoTask(URL location, ServiceInfo serviceInfo) {
			this.location = location;
			this.serviceInfo = serviceInfo;
		}

		@Override 
		public void run() {
			sendPostPlayiTunes();
		}

		/*************************************************************************************
		 * GET /server-info HTTP/1.1 X-Apple-Device-ID: 0xdc2b61a0ce79
		 * Content-Length: 0 User-Agent: MediaControl/1.0 X-Apple-Session-ID:
		 * 1bd6ceeb-fffd-456c-a09c-996053a7a08c
		 *************************************************************************************/
		@SuppressWarnings("unused") 
		private void sendGetServerInfo() {
			LogUtil.d("AirPlayClientService PlayVideoTask sendGetServerInfo()");
			try {
				URL url = new URL(serviceInfo.getURL() + "/server-info");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true);
				conn.setDoOutput(true);
				conn.setConnectTimeout(15 * 1000);
				conn.setReadTimeout(15 * 1000);
				conn.setRequestMethod("GET");

				conn.setRequestProperty("X-Apple-Device-ID", "0xdc2b61a0ce79");
				conn.setRequestProperty("Content-Length", "0");
				conn.setRequestProperty("User-Agent", "MediaControl/1.0");
				conn.setRequestProperty("X-Apple-Session-ID", UUID.randomUUID().toString());

				BufferedOutputStream out = new BufferedOutputStream(conn.getOutputStream());
				out.close();
				int status = conn.getResponseCode();
				if (status == 200) {
					LogUtil.d("AirPlayClientService PlayVideoTask sendGetServerInfo() service responded 200 ! to sendPostReverse()");
					sendPostReverse();
				} else {
					LogUtil.e("AirPlayClientService PlayVideoTask sendGetServerInfo() service responded " + status);
					if (callback != null) {
						callback.onPlayVideoError(location);
					}
				}
			} catch (Exception e) {
				LogUtil.e("AirPlayClientService PlayVideoTask sendGetServerInfo() exception \n " + e.getMessage());
				if (callback != null) {
					callback.onPlayVideoError(location);
				}
			}
		}

		/*************************************************************************************
		   POST /reverse 
           Upgrade: PTTH/1.0 
           Connection: Upgrade
		   X-Apple-Purpose: event 
		   Content-Length: 0 
           User-Agent:MediaControl/1.0 
           X-Apple-Session-ID:1bd6ceeb-fffd-456c-a09c-996053a7a08c
		 *************************************************************************************/
		private void sendPostReverse() {
			LogUtil.d("AirPlayClientService PlayVideoTask sendPostReverse()");
			try {
				URL url = new URL(serviceInfo.getURL() + "/reverse");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true);
				conn.setDoOutput(true);
				conn.setConnectTimeout(15 * 1000);
				conn.setReadTimeout(15 * 1000);
				conn.setRequestMethod("POST");

				conn.setRequestProperty("Upgrade", "PTTH/1.0");
				conn.setRequestProperty("Connection", "Upgrade");
				conn.setRequestProperty("X-Apple-Purpose", "event");
				conn.setRequestProperty("Content-Length", "0");
				conn.setRequestProperty("User-Agent", "MediaControl/1.0");
				conn.setRequestProperty("X-Apple-Session-ID", UUID.randomUUID().toString());

				BufferedOutputStream out = new BufferedOutputStream(conn.getOutputStream());
				out.close();
				int status = conn.getResponseCode();
				if (status == 101) {
					LogUtil.d("AirPlayClientService PlayVideoTask sendPostReverse() service responded 101 ! to sendPostPlayiTunes()");
					sendPostPlayiTunes();
				} else {
					LogUtil.e("AirPlayClientService PlayVideoTask sendPostReverse() service responded " + status);
					if (callback != null) {
						callback.onPlayVideoError(location);
					}
				}
			} catch (Exception e) {
				LogUtil.e("AirPlayClientService PlayVideoTask sendPostReverse() exception \n " + e.getMessage());
				if (callback != null) {
					callback.onPlayVideoError(location);
				}
			}
		}

		/*************************************************************************************
		 * // POST /play HTTP/1.1 // X-Transmit-Date:
		 * 2012-03-16T14:20:39.656533Z // Content-Type:
		 * application/x-apple-binary-plist // Content-Length: 491 //
		 * User-Agent: MediaControl/1.0 // X-Apple-Session-ID:
		 * 368e90a4-5de6-4196-9e58-9917bdd4ffd7 // // <BINARY PLIST DATA> // //
		 * <?xml version="1.0" encoding="UTF-8"?> // <!DOCTYPE plist PUBLIC
		 * "-//Apple//DTD PLIST 1.0//EN" //
		 * "http://www.apple.com/DTDs/PropertyList-1.0.dtd"> // <plist
		 * version="1.0"> // <dict> // <key>Content-Location</key> //
		 * <string>http://redirector.c.youtube.com/videoplayback?...</string> //
		 * <key>Start-Position</key> // <real>0.024613151326775551</real> //
		 * </dict> // </plist>
		 *************************************************************************************/
		@SuppressWarnings("unused")
		private void sendPostPlayIphone() {
			LogUtil.d("AirPlayClientService PlayVideoTask sendPostPlayIphone()");
			try {
				StringBuilder content = new StringBuilder();
				content.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "\n");
				content.append("<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">"
						+ "\n");
				content.append("<plist version=\"1.0\">" + "\n");
				content.append("<dict>" + "\n");
				content.append("<key>Content-Location</key>" + "\n");
				content.append("<string>" + location.toString() + "</string>" + "\n");
				content.append("<key>Start-Position</key>" + "\n");
				content.append("<real>0.0</real>" + "\n");
				content.append("</dict>" + "\n");
				content.append("</plist>" + "\n");

				URL url = new URL(serviceInfo.getURL() + "/play");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true);
				conn.setDoOutput(true);
				conn.setConnectTimeout(15 * 1000);
				conn.setReadTimeout(15 * 1000);
				conn.setRequestMethod("POST");

				conn.setRequestProperty("X-Transmit-Date: ", "2015-03-16T14:20:39.656533Z");
				conn.setRequestProperty("Content-Type: ", "application/x-apple-binary-plist");
				conn.setRequestProperty("Content-Length: ", "" + content.length());
				conn.setRequestProperty("User-Agent: ", "MediaControl/1.0");
				conn.setRequestProperty("X-Apple-Session-ID: ", UUID.randomUUID().toString());

				BufferedOutputStream out = new BufferedOutputStream(conn.getOutputStream());
				out.write(content.toString().getBytes());
				out.close();

				int status = conn.getResponseCode();
				if (status == 200) {
					LogUtil.e("AirPlayClientService PlayVideoTask sendPostPlayIphone() service responded 200");
					if (callback != null) {
						callback.onPlayVideoSuccess(location);
					}
				} else {
					LogUtil.e("AirPlayClientService PlayVideoTask sendPostPlayIphone() service responded " + status);
					if (callback != null) {
						callback.onPlayVideoError(location);
					}
				}
			} catch (Exception e) {
				LogUtil.e("AirPlayClientService PlayVideoTask sendPostPlayIphone() exception \n " + e.getMessage());
				if (callback != null) {
					callback.onPlayVideoError(location);
				}
			}
		}

		/*************************************************************************************
		 * POST /play HTTP/1.1 
		 * User-Agent: iTunes/10.6 (Macintosh; Intel Mac OSX 10.7.3) AppleWebKit/535.18.5 
		 * Content-Length: 163 
		 * Content-Type: text/parameters
		 * 
		 * Content-Location: http://192.168.8.34:3689/airplay.mp4
		 * Start-Position: 0.174051
		 *************************************************************************************/
		private void sendPostPlayiTunes() {
			LogUtil.d("AirPlayClientService PlayVideoTask sendPostPlayiTunes()");
			try {
				StringBuilder content = new StringBuilder();
				content.append("Content-Location: ");
				content.append(location.toString());
				content.append("\n");
				content.append("Start-Position: 0.00\n");

				URL url = new URL(serviceInfo.getURL() + "/play");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true);
				conn.setDoOutput(true);
				conn.setConnectTimeout(15 * 1000);
				conn.setReadTimeout(15 * 1000);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Length", "" + content.toString().getBytes().length);
				conn.setRequestProperty("Content-Type", "text/parameters");
				conn.setRequestProperty("X-Apple-AssetKey", UUID.randomUUID().toString());
				conn.setRequestProperty("X-Apple-Session-ID", UUID.randomUUID().toString());
				conn.setRequestProperty("User-Agent", "MediaControl/1.0");

				BufferedOutputStream out = new BufferedOutputStream(conn.getOutputStream());
				out.write(content.toString().getBytes());
				out.flush();
				out.close();

				int status = conn.getResponseCode();
				if (status == 200) {
					LogUtil.d("AirPlayClientService PlayVideoTask sendPostPlayiTunes() service responded 200 !");
					if (callback != null) {
						callback.onPlayVideoSuccess(location);
					}
				} else {
					LogUtil.d("AirPlayClientService PlayVideoTask sendPostPlayiTunes() service responded " + status);
					if (callback != null) {
						callback.onPlayVideoError(location);
					}
				}
			} catch (Exception e) {
				LogUtil.e("AirPlayClientService PlayVideoTask sendPostPlayiTunes() exception \n " + e.getMessage());
				if (callback != null) {
					callback.onPlayVideoError(location);
				}
			}
		}
	}

	private class RateVideoTask implements Runnable {

		private ServiceInfo serviceInfo;
		private String state;

		public RateVideoTask(ServiceInfo serviceInfo, String state) {
			this.serviceInfo = serviceInfo;
			this.state = state;
		}

		@Override public void run() {
			LogUtil.d("AirPlayClientService RateVideoTask run()");
			try {
				// POST /rate?value=0.000000 HTTP/1.1
				// User-Agent: iTunes/10.6 (Macintosh; Intel Mac OS X 10.7.3)
				// AppleWebKit/535.18.5
				// Content-Length: 0
				URL url = new URL(serviceInfo.getURL() + "/rate?value=" + state);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true);
				conn.setDoOutput(true);
				conn.setConnectTimeout(15 * 1000);
				conn.setReadTimeout(15 * 1000);
				conn.setRequestMethod("POST");

				conn.setRequestProperty("User-Agent", "android");
				conn.setRequestProperty("Content-Length", "0");

				BufferedOutputStream out = new BufferedOutputStream(conn.getOutputStream());
				out.close();

				int status = conn.getResponseCode();
				if (status == 200) {
					LogUtil.d("AirPlayClientService RateVideoTask service responded 200 !");
					if (callback != null) {
						callback.onRateVideoSuccess(state);
					}
				} else {
					LogUtil.e("AirPlayClientService RateVideoTask service responded " + status);
					if (callback != null) {
						callback.onRateVideoError(state);
					}
				}
			} catch (Exception e) {
				if (callback != null) {
					LogUtil.e("AirPlayClientService RateVideoTask exception \n" + e.getMessage());
					callback.onRateVideoError(state);
				}
			}
		}
	}

	private class StopVideoTask implements Runnable {

		private ServiceInfo serviceInfo;

		public StopVideoTask(ServiceInfo serviceInfo) {
			this.serviceInfo = serviceInfo;
		}

		@Override public void run() {
			LogUtil.d("AirPlayClientService StopVideoTask run()");
			try {

				/*************************************************************************************
				 * POST /stop HTTP/1.1 User-Agent: iTunes/10.6 (Macintosh; Intel
				 * Mac OS X 10.7.3) AppleWebKit/535.18.5 Content-Length: 0
				 *************************************************************************************/
				URL url = new URL(serviceInfo.getURL() + "/stop");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setConnectTimeout(15 * 1000);
				conn.setReadTimeout(15 * 1000);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Length", "0");
				conn.setRequestProperty("User-Agent", "MediaControl/1.0");
				int status = conn.getResponseCode();
				if (status == 200) {
					LogUtil.d("AirPlayClientService StopVideoTask service responded 200 !");
					if (callback != null) {
						callback.onStopVideoSuccess();
					}
				} else {
					LogUtil.d("AirPlayClientService StopVideoTask service responded " + status);
					if (callback != null) {
						callback.onStopVideoError();
					}
				}
			} catch (Exception e) {
				LogUtil.e("AirPlayClientService StopVideoTask exception \n" + e.getMessage());
				if (callback != null) {
					callback.onStopVideoError();
				}
			}
		}
	}

	/* 视频快进，后退 */
	private class SeekVideoTask implements Runnable {
		private ServiceInfo serviceInfo;
		private float seconds;

		public SeekVideoTask(ServiceInfo serviceInfo, float seconds) {
			this.serviceInfo = serviceInfo;
			this.seconds = seconds;
		}

		@Override public void run() {
			try {
				/*************************************************************************************
				 * POST /scrub?position=20.097000 HTTP/1.1 User-Agent:
				 * iTunes/10.6 (Macintosh; Intel Mac OS X 10.7.3)
				 * AppleWebKit/535.18.5 Content-Length: 0
				 *************************************************************************************/
				URL url = new URL(serviceInfo.getURL() + "/scrub?position=" + seconds);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setConnectTimeout(15 * 1000);
				conn.setReadTimeout(15 * 1000);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("User-Agent", "MediaControl/1.0");
				conn.setRequestProperty("Content-Length", "0");
				int status = conn.getResponseCode();
				if (status == 200) {
					LogUtil.d("AirPlayClientService SeekVideoTask service responded 200 !");
					if (callback != null) {
						callback.onSeekVideoSuccess(seconds);
					}
				} else {
					LogUtil.e("AirPlayClientService SeekVideoTask service responded " + status);
					if (callback != null) {
						callback.onSeekVideoError(seconds);
					}
				}
			} catch (Exception e) {
				LogUtil.e("AirPlayClientService SeekVideoTask exception \n" + e.getMessage());
				if (callback != null) {
					callback.onSeekVideoError(seconds);
				}
			}
		}
	}
}
