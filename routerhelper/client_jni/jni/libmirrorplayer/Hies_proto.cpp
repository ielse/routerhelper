#include <stdlib.h>
#include <stdarg.h>
#include <limits.h>
#include <float.h>
#include <sys/socket.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/select.h>
#include <errno.h>

#include <sys/param.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <unistd.h>


#include "pthread.h"
#include "Hies_proto.h"

#define TAG "Hies_proto"

#define true            1
#define false            0
#define HIES_FRAME_HEADLEN ((sizeof(int))+(sizeof(unsigned int))+(sizeof(unsigned int)))
/*视频帧数据结构*/
/*video frame struct*/
typedef struct _FrameInfo
{
    int pts;
    int datalen;
    int payload;
    unsigned char data[MAX_VIDEO_FRAME_SIZE];
    BOOL busy;
} FrameInfo;

static int mFreeCount = MAX_VIDEO_BUF_NUM;
/*缓存的视频帧，最大个数为MAX_VIDEO_BUF_NUM*/
/*cached video frame , max MAX_VIDEO_BUF_NUM*/
static FrameInfo mframeBuf[MAX_VIDEO_BUF_NUM] = {0};
static pthread_cond_t m_condition;
static pthread_mutex_t m_lock;
static unsigned char* m_VdopktData;
static int mVidCondWaitCount = 0;

static volatile BOOL m_brunning = false;
static BOOL m_bReset = true;
static BOOL m_bVdoEndFlag = true;
static BOOL m_bBeginReadFlag = false;

static pthread_t proto_thread_id = 0;

unsigned char mSPSBuf[128] = {0};
unsigned char mPPSBuf[128] = {0};
int s32SPSSize = 0;
int s32PPSSize = 0;

unsigned char SPSHead[5] = {0x00, 0x00, 0x00, 0x01, 0x67};
unsigned char PPSHead[5] = {0x00, 0x00, 0x00, 0x01, 0x68};
unsigned char NAL[4] = {0x00, 0x00, 0x00, 0x01};

//sps pps for 720p
unsigned char SPSFor720p[SPS_SIZE_FOR_720P] = {0x67, 0x42, 0x00, 0x1f, 0xda,0x01, 0x40, 0x16, 0xe4};
unsigned char PPSFor720p[PPS_SIZE_FOR_720P] = {0x68, 0xce, 0x3c, 0x80};


static BOOL m_bHaveSPSPPS = false;

double HI_HIES_getTime()
{
    struct timeval pTime;
    gettimeofday(&pTime, NULL);
    return (pTime.tv_sec + (pTime.tv_usec / 1000000.0));
}

//parse frame
int HI_HIES_Parse_SPSPPS(char* pBuf, int length)
{
    MMLOGD(TAG, "Hies proto [%s]:[%d]\n", __FUNCTION__, __LINE__);

    char* pData = pBuf;
    char* pSPSHead = NULL;
    char* pPPSHead = NULL;
    char* pFrameHead = NULL;
    int i  = 0, j = 0;
    int spsSize = 0;
    int ppsSize = 0;

    /*
    for(i=0; i< 100; i++)
    {
        MMLOGI( TAG, "0x%02x,", *(pData+i));
    }
    */
    for ( i = 0; i < length - 5; i++)
    {
        if (*(pData + i) == SPSHead[0] &&
            *(pData + i + 1) == SPSHead[1] &&
            *(pData + i + 2) == SPSHead[2] &&
            *(pData + i + 3) == SPSHead[3] &&
            *(pData + i + 4) == SPSHead[4] )
        {
            pSPSHead = pData + i + 4; //point into 0x67
            //MMLOGI(TAG, "i=%d, size=%d\n", i, pSPSHead - pData);
            break;
        }
    }

    if (NULL == pSPSHead)
    {
        MMLOGE(TAG, "not find  SPS [%s]:[%d]\n", __FUNCTION__, __LINE__);
        return HI_FAILURE;
    }
    pData = pSPSHead;
    for (i = 0; i < length - 5 - (pSPSHead - pBuf) ; i++)
    {
        if (*(pData + i) == PPSHead[0] &&
            *(pData + i + 1) == PPSHead[1] &&
            *(pData + i + 2) == PPSHead[2] &&
            *(pData + i + 3) == PPSHead[3] &&
            *(pData + i + 4) == PPSHead[4] )
        {
            pPPSHead = pData + i + 4; //point into 0x68
            //MMLOGI(TAG, "i=%d\n", i);
            break;
        }
    }
    if (NULL == pPPSHead)
    {
        MMLOGE(TAG, "not find  PPS [%s]:[%d]\n", __FUNCTION__, __LINE__);
        return HI_FAILURE;
    }
    //delete sps head size 4
    spsSize = pPPSHead - pSPSHead - 4;
    MMLOGI(TAG, "spsSize=%d\n", spsSize);

    pData = pPPSHead;
    for (i = 0; i < length - 4 - (pPPSHead - pBuf) ; i++)
    {
        if (*(pData + i) == NAL[0] &&
            *(pData + i + 1) == NAL[1] &&
            *(pData + i + 2) == NAL[2] &&
            *(pData + i + 3) == NAL[3] )
        {
            //MMLOGI(TAG, "i=%d\n", i);
            pFrameHead = pData + i;
            break;
        }
    }

    if (NULL == pFrameHead)
    {
        //frame only has sps and pps
        //delete sps head size and pps head size
        ppsSize = length - spsSize - 4 - 4;
    }
    else
    {
        //I frame contains sps and pps
        ppsSize = pFrameHead - pPPSHead;
    }

    if ( ppsSize > 128 || spsSize > 128)
    {
        MMLOGE(TAG, "wrong sps or pps size [%s]:[%d]\n", __FUNCTION__, __LINE__);
        return HI_FAILURE;
    }
    //copy the sps and pps into buffer
    memcpy(mSPSBuf, pSPSHead, spsSize);
    memcpy(mPPSBuf, pPPSHead, ppsSize);

    s32SPSSize = spsSize;
    s32PPSSize = ppsSize;

    return HI_SUCCESS;
}

/*把接收到的一帧视频，存入buffer中*/
/*put one video frame received into cache buffer*/
void HI_HIES_put_video(void* pbuf, int dataLen , int pts, int payload)
{
    static BOOL bLastIFameLost = false;
    if (mFreeCount == 0)
    {
        pthread_cond_signal(&m_condition);
        //MMLOGI(TAG, "video buffer is full!!!,payload: %d\n", payload);
        if (payload == 5)
        {
           bLastIFameLost = true;
        }
        return;
    }
    if (payload == 5)
    {
       bLastIFameLost = false;
    }
    if (!bLastIFameLost)
    {
        int i = 0;
        for (i = 0; i < MAX_VIDEO_BUF_NUM; i++)
        {
            if (!mframeBuf[i].busy)
            {
              break;
            }
        }
        mframeBuf[i].datalen = dataLen;
        mframeBuf[i].pts = pts;
        mframeBuf[i].payload = payload;
        memcpy( mframeBuf[i].data, pbuf,  dataLen);
        pthread_mutex_lock(&m_lock);
        mframeBuf[i].busy = true;
        mFreeCount -= 1;
        //MMLOGI(TAG, " put video frame  len : %d pts: %d  index : %d free: %d\n", dataLen, pts,  i, mFreeCount);
        if (mFreeCount == MAX_VIDEO_BUF_NUM - 1)
        {
           pthread_cond_signal(&m_condition);
        }
        pthread_mutex_unlock(&m_lock);
    }
    else
    {
        MMLOGI(TAG, "lost this  video frame\n");
    }
}


void HI_HIES_put_frame(void* pbuf, int dataLen , int pts, int payload)
{
    //if omx not begin to read, don't put the frame into the buffers
    if (!m_bBeginReadFlag)
    {
        return;
    }
    else
    {
        /*如果发生过protocol 重置, 需保证第一帧视频要是I 帧*/
        /*if protocol reset happened, need to ensure the frist video frame to be I frame*/
        if (m_bReset)
        {
            if (payload != 5)
            {
              return;
            }
            m_bReset = false;
            MMLOGI(TAG, "first put video buffer is key frame!!!\n");
        }
        HI_HIES_put_video(pbuf, dataLen, pts, payload);
    }
}

void HI_HIES_Reset_mediaBuf(void)
{
    MMLOGD(TAG, "HI_HIES_Reset_mediaBuf begin\n");
    pthread_mutex_lock(&m_lock);
    mFreeCount = MAX_VIDEO_BUF_NUM;
    memset((void*)&mframeBuf[0], 0x00, MAX_VIDEO_BUF_NUM * sizeof(FrameInfo));
    pthread_mutex_unlock(&m_lock);
}


void HI_HIES_Put_VideoFrame(char* pBuf,  int dataLen)
{
    int i_ptype = 0;
    unsigned int i_pts = 0;
    unsigned int  i_dataLen = 0;
    char* pdata = pBuf;
    int iret = HI_FAILURE;

    //MMLOGI(TAG,"enter HI_HIES_Put_VideoFrame, frame length=%d\n", dataLen);
    memcpy(&i_ptype, pdata, sizeof(int));
    pdata += sizeof(int);
    memcpy(&i_pts, pdata,  sizeof(unsigned int));
    pdata += sizeof(unsigned int);
    memcpy(&i_dataLen, pdata, sizeof(unsigned int));
    pdata += sizeof(unsigned int);
    //MMLOGI(TAG, "video rev pt: %d, pts: %d, len: %d\n", i_ptype, i_pts, i_dataLen);
    /*如果payload type 不在视频帧范围内，可能是帧头丢失，丢弃该帧*/
    /*if the frame payload type is not one of 1, 5, 7, 8. then maybe frame header are lost*/
    if (i_ptype > 10 || i_ptype < 0)
    {
        MMLOGI(TAG, "wrong type:%d\n", i_ptype);
        return;
    }
    if (i_dataLen > MAX_VIDEO_FRAME_SIZE)
    {
        MMLOGI(TAG, "video buf size is not big enough\n");
        return;
    }

    if(i_dataLen + HIES_FRAME_HEADLEN != dataLen)
    {
        MMLOGI(TAG,"may lost some pack, recv len: %d \n", dataLen);
        i_dataLen = dataLen - HIES_FRAME_HEADLEN;
    }

    if ((m_bHaveSPSPPS == false) && ((i_ptype == 5) || (i_ptype == 7)))
    {
        MMLOGI(TAG, "receive type %d parse spspps\n", i_ptype);
        iret = HI_HIES_Parse_SPSPPS(pdata, i_dataLen);
        if (iret == HI_SUCCESS)
        {
            m_bHaveSPSPPS = true;
        }
    }

    //not spspps frame then put frame to buffer.
    if (i_ptype != 7)
    {
        HI_HIES_put_frame(pdata, i_dataLen, i_pts, i_ptype);
    }

}

/**
Receive frame from STB by udp, then put the frame date into frames buffer
*/
void* HI_HIES_Receive_Process(void* arg)
{
    MMLOGD(TAG, "Hies proto [%s]:[%d]\n", __FUNCTION__, __LINE__);
    Rtphead* rtpHeadMsg = NULL;
    HI_U16 seq ;
    HI_U16 prevSeq;

    //whether lost flag
    HI_U16 rtpFlg = 0;
    int mark;
    int type;
    int sum = 0;

    int bodyLen = 0;

    //static rtp lost para
    HI_U16 start_count_seq;
    HI_U16 recv_rtp_num;

    //init socket
    struct sockaddr_in servaddr;
    int sockfd = -1, n, dataLen;
    socklen_t servaddr_len;
    HI_S32 keepalive = 1;
    HI_S32 s32Ret = HI_FAILURE;

    fd_set read_fds;
    struct timeval TimeoutVal;
    char* framebufer = NULL;
    char* recvs = NULL;

    m_bVdoEndFlag = false;
    framebufer = (char*)malloc(MAX_VIDEO_FRAME_SIZE);

    if (NULL == framebufer)
    {
        MMLOGE(TAG, "framebufer buffer is NULL.");
        goto Ret;
    }
    memset(framebufer, 0, sizeof(framebufer));

    recvs = (char*)malloc(RTP_MSG_LEN);

    if (NULL == recvs)
    {
        MMLOGE(TAG, "receive buffer is NULL.");
        goto Ret;
    }

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
    {
        MMLOGE(TAG, "udp socket error\n");
        goto Ret;
    }

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);

    servaddr.sin_port = htons(CLIENT_TRANSMIT_PORT);

    if (-1 == bind(sockfd, (struct sockaddr*)&servaddr, sizeof(servaddr)))
    {
        MMLOGE(TAG, "udp bind error.");
        close(sockfd);
        goto Ret;
    }
    else
    {
        s32Ret = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &keepalive, sizeof(keepalive));
        if (-1 == s32Ret)
        {
            close(sockfd);

            MMLOGE(TAG, "set socket option reuse error.");
            goto Ret;
        }
    }

    while (true == m_brunning)
    {
        FD_ZERO(&read_fds);
        FD_SET(sockfd, &read_fds);

        TimeoutVal.tv_sec  = 1;
        TimeoutVal.tv_usec = 0;

        s32Ret = select(sockfd + 1, &read_fds, 0, 0, &TimeoutVal);
        if ( s32Ret <= 0)
        {
            continue;
        }
        else
        {
            //LOGE("FD_ISSET");
            /*lint -e573  -e737 -e778*/
            if (false == FD_ISSET(sockfd, &read_fds))
            {
                MMLOGI(TAG, "FD_ISSET error");
                close(sockfd);
                goto Ret;
            }
            /*lint +e573  +e737 +e778*/

            memset(recvs, 0, sizeof(recvs));

            dataLen = recvfrom(sockfd, (char*)(recvs), RTP_MSG_LEN, 0, NULL, 0);
            if (dataLen < RTP_HEADER_SIZE)
            {
                MMLOGI(TAG, "dataLen < RTP_HEADER_SIZE");
                continue ;
            }

            rtpHeadMsg = (Rtphead*)recvs;
            mark = rtpHeadMsg->marker;
            bodyLen = dataLen - RTP_HEADER_SIZE;
            seq = rtpHeadMsg->sequencenumber;
            type = rtpHeadMsg->payloadtype;
            recv_rtp_num++;

            if ((HI_U16)(seq - start_count_seq) >= H264_RTP_LOST_INTERVAL)
            {
                HI_U16 cur_lost_num = (HI_U16)(H264_RTP_LOST_INTERVAL - recv_rtp_num) % H264_RTP_LOST_INTERVAL;
                MMLOGD(TAG, "lost h264 rtp num %d / in %d \n ", cur_lost_num, H264_RTP_LOST_INTERVAL);
                start_count_seq = seq;
                recv_rtp_num = 0;
            }
            //MMLOGI(TAG, "seq=%d, mark=%d, bodyLen= %d", seq, mark, bodyLen);

            //if seqNum is right, copy the data into frame buffer
            if (mark == 1 && rtpFlg == 0)
            {
                rtpFlg = 1;
                sum = 0;

            }
            else if (rtpFlg == 1)
            {
                if (seq == (prevSeq + 1))
                {
                    //LOGD("seq in order");
                    if ( NULL != framebufer)
                    {
                        memcpy(framebufer + sum, (recvs + RTP_HEADER_SIZE), bodyLen);
                        sum += bodyLen;
                        //LOGD("sum=%d",sum);
                    }
                }
                else
                {
                    sum = 0;
                    rtpFlg = 0;
                    continue;
                }
            }
            else
            {
                //abandon the frame which already lost packet
                continue;
            }
            //LOGD("PrevSeq=%d",PrevSeq);
            prevSeq = seq;

            //trans the frame into buffer even lost some packet
            if (mark == 1  && rtpFlg == 1 && sum > 0)
            {
                if ( (NULL != framebufer))
                {
                    HI_HIES_Put_VideoFrame(framebufer, sum);
                }
                sum = 0;
                rtpFlg = 1;
            }
        }
    }
    close(sockfd);
Ret:
    if (recvs)
    {
      free(recvs);
    }

    if (framebufer)
    {
      free(framebufer);
    }

    m_bVdoEndFlag = true;
    MMLOGI(TAG, "HI_HIES_Receive_Process  exit \n");
    return 0;
}


int  HI_HIES_Connect()
{
    MMLOGD(TAG, "Hies proto [%s]:[%d]\n", __FUNCTION__, __LINE__);
    int ret = 0;
    if (false == m_brunning)
    {
        m_brunning = true;
        ret = pthread_create(&proto_thread_id, 0, HI_HIES_Receive_Process, NULL);
        if (ret < 0)
        {
            MMLOGE(TAG, "pthread_create error \n");
            return HI_FAILURE;
        }
    }
    return HI_SUCCESS;
}

int HI_HIES_Init_Proto(void)
{
    MMLOGD(TAG, "Hies proto [%s]:[%d]\n", __FUNCTION__, __LINE__);
    pthread_cond_init(&m_condition, NULL);
    pthread_mutex_init(&m_lock, NULL);
    memset((void*)&mframeBuf[0], 0x00, MAX_VIDEO_BUF_NUM * sizeof(FrameInfo));
    m_bBeginReadFlag = false;
    return 0;
}

/*deinit protocol,close the data receiver*/
int HI_HIES_DeInit_Proto(void)
{
    MMLOGD(TAG, "Hies proto [%s]:[%d]\n", __FUNCTION__, __LINE__);
    m_brunning = false;
    if (pthread_join(proto_thread_id, 0) < 0)
    {
        MMLOGI(TAG, "pthread_join proto_thread_id failed\n");
    }
    HI_HIES_Reset_mediaBuf();
    int i = 0;
    for (i = 0; i < 5; i++)
    {
        pthread_cond_signal(&m_condition);
        usleep(10000);
    }
    pthread_cond_destroy(&m_condition);
    pthread_mutex_destroy(&m_lock);

    HI_S32 iSleepTime = 0;
    while (!m_bVdoEndFlag )
    {
        MMLOGI(TAG, "audio and video thread have not cease\n");
        usleep(200000);
        if (iSleepTime > 20)
        {
            break;
        }
        iSleepTime += 1;
    }
    m_bReset = true;
    m_bBeginReadFlag = false;
    m_bHaveSPSPPS = false;
    return 0;
}


/*从视频 缓冲区 中读取视频帧，如果当前没有数据可读，会
    wait在这里，直到等到数据ready的通知*/
/*get video frame from video buffer, if there no data, we'll wait here until
    receive the signal of ready to read*/
int HI_HIES_read_video_stream(void* ptr, int* dataSize, int64_t* pts, int* pType)
{
    //MMLOGI(TAG,"HI_HIES_read_video_stream enter \n");
    if (!m_bBeginReadFlag)
    {
        m_bBeginReadFlag = true;
    }
    if (mFreeCount == MAX_VIDEO_BUF_NUM)
    {
        //MMLOGI(TAG, "before HI_HIES_read_video_stream wait \n");
        mVidCondWaitCount++;
        pthread_mutex_lock(&m_lock);
        pthread_cond_wait(&m_condition, &m_lock);
        mVidCondWaitCount--;
        //MMLOGI(TAG, "after HI_HIES_read_video_stream wait \n");
    }
    else
    {
        pthread_mutex_lock(&m_lock);
    }
    if (!m_brunning)
    {
        MMLOGI(TAG, "m_brunning is false \n");
        pthread_mutex_unlock(&m_lock);
        return -1;
    }
    int iminPts = 0;
    int iminIdx = 0;
    int i = 0;
    for (i = 0; i < MAX_VIDEO_BUF_NUM; i++)
    {
        if (mframeBuf[i].busy
            && (iminPts == 0 || mframeBuf[i].pts < iminPts))
        {
            iminPts = mframeBuf[i].pts;
            iminIdx = i;
        }
    }
    FrameInfo* pFrame = &mframeBuf[iminIdx];
    //MMLOGI(TAG,"read video frame index: %d  datalen: %d pts: %d \n", iminIdx, pFrame->datalen, pFrame->pts);

    if (mframeBuf[iminIdx].datalen == 0)
    {
        mframeBuf[iminIdx].busy = false;
        MMLOGI(TAG, "video buffer len  is 0 \n");
        pthread_mutex_unlock(&m_lock);
        return -1;
    }
    if (pFrame->datalen > *dataSize)
    {
        mframeBuf[iminIdx].busy = false;
        MMLOGI(TAG, "buffer length is too small\n");
        pthread_mutex_unlock(&m_lock);
        return -1;
    }
    *dataSize = pFrame->datalen;
    *pts = pFrame->pts;
    *pType = pFrame->payload;
    memcpy(ptr, pFrame->data, pFrame->datalen);
    mFreeCount += 1;
    pFrame->busy = false;
    pthread_mutex_unlock(&m_lock);
    return 0;
}


int HI_HIES_GetParameterFrame(HI_U8* pSpsBuffer, int* spsLen, HI_U8* pPPSBuffer, int* ppsLen)
{
    int i = 0;
    MMLOGD(TAG, "Hies proto [%s]:[%d]\n", __FUNCTION__, __LINE__);
    while (i < 20)
    {
        if (m_bHaveSPSPPS == true)
        {
            break;
        }
        else
        {
            usleep(100000);
            i++;
        }
    }
    if ( i >= 20)
    {
        MMLOGI(TAG, "set default sps pps [%s]:[%d]\n", __FUNCTION__, __LINE__);
        *spsLen =  SPS_SIZE_FOR_720P;
        *ppsLen =  PPS_SIZE_FOR_720P;
        memcpy(pSpsBuffer, SPSFor720p, SPS_SIZE_FOR_720P);
        memcpy(pPPSBuffer, PPSFor720p, PPS_SIZE_FOR_720P);

    }
    else
    {
        MMLOGI(TAG, "get sps pps from received data [%s]:[%d]\n", __FUNCTION__, __LINE__);
        *spsLen =  s32SPSSize;
        *ppsLen = s32PPSSize;
        memcpy(pSpsBuffer, mSPSBuf, s32SPSSize);
        memcpy(pPPSBuffer, mPPSBuf, s32PPSSize);
    }

    MMLOGI( TAG, "spssize %d  ppssize %d,",*spsLen,*ppsLen);

    MMLOGI( TAG, "sps:  ");
    for (i = 0; i < *spsLen; i++)
    {
        MMLOGI( TAG, "0x%02x,", pSpsBuffer[i]);
    }

    MMLOGI( TAG, "pps:  ");
    for (i = 0; i < *ppsLen; i++)
    {
        MMLOGI( TAG, "0x%02x,", pPPSBuffer[i]);
    }

    return HI_SUCCESS;
}
