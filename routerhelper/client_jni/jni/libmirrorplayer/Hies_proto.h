#ifndef __HIES_PROTO__
#define __HIES_PROTO__

#define MAX_VIDEO_BUF_NUM  30
typedef int BOOL;

#include <stdlib.h>
//#include <stdint.h>
#include "com_mirror_common.h"

#ifdef __cplusplus
extern "C" {
#endif

#define CLIENT_TRANSMIT_PORT          8888
#define RTP_HEADER_SIZE             sizeof(Rtphead)

#define BYTES_PER_SEND                (10240)  //10K
#define RTP_MSG_LEN                 (RTP_HEADER_SIZE + BYTES_PER_SEND)
#define H264_RTP_LOST_INTERVAL       200

#define MAX_VIDEO_FRAME_SIZE (200*1024)

#define SPS_SIZE_FOR_720P         9

#define PPS_SIZE_FOR_720P         4

//void HI_HIES_log_set_callback(void (*callback)(void*, int, const char*, va_list));
typedef struct RTPHeader
{

    HI_U8 csrccount:4;

    HI_U8 extension:1;

    HI_U8 padding:1;

    HI_U8 version:2;

    HI_U8 payloadtype:7;

    HI_U8 marker:1;
    HI_U16     sequencenumber;
    HI_U32     timestamp;
    HI_U32     ssrc;
}Rtphead;

int HI_HIES_Init_Proto(void);
int HI_HIES_DeInit_Proto(void);

int  HI_HIES_Connect();

int HI_HIES_read_video_stream(void* ptr, int* dataSize, int64_t* pts, int* pType);

int HI_HIES_GetParameterFrame(HI_U8* pSpsBuffer,int* spsLen,HI_U8* pPPSBuffer,int* ppsLen);

#ifdef __cplusplus
}
#endif
#endif
