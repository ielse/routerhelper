#include "HiRtpVideoSource.h"
#include <media/stagefright/MetaData.h>

#define TAG "HiRtpVideoSource"
namespace android{
HiRtpVideoSource::HiRtpVideoSource(HiRtpClient* protocol)
:mProto(protocol)
{

}

HiRtpVideoSource::~HiRtpVideoSource()
{

}

int HiRtpVideoSource::start()
{

}

int HiRtpVideoSource::stop()
{

}

int HiRtpVideoSource::read(MediaBuffer* pbuffer)
{
    MediaBuffer* buffer = pbuffer;
    int dataSize = buffer->size();
    void *ptr = (void *)buffer->data();
    int64_t pts = 0;
    int payloadType = 0;

    int ret =0;
    ret = mProto->readVideoStream(ptr, dataSize, pts, payloadType);
    if(ret != 0)
    {
        MMLOGE(TAG, "readVideoStream failed\n");
        return -1;
    }
    buffer->meta_data().clear();
    if(payloadType == 5)
    {
        buffer->meta_data()->setInt32(
                kKeyIsSyncFrame,  1);
    }
    buffer->meta_data()->setInt64(
            kKeyTime,  pts*1000);

    buffer->set_range(0, dataSize);
    return OK;
}
}
