#ifndef  _HI_RTP_CLIENT_H_
#define _HI_RTP_CLIENT_H_

#include <stdlib.h>
#include "pthread.h"
#include "com_mirror_common.h"
class HiRtpClient{
public:
    HiRtpClient();
    virtual ~HiRtpClient();
    int getSPSPPS(void* sps, int *spsSize, void* pps, int* ppsSize);
    int readVideoStream(void* ptr, int& dataSize, int64_t& pts, int& ptype);
    virtual int connect();
    virtual int disconnect();

private:
    int mRunning;
    pthread_t mReceiveThread;
};

#endif
