#ifndef _HI_OMXCODEC_H_
#define _HI_OMXCODEC_H_
#include <jni.h>
#include "HiMediaSource.h"
#include <android/native_window_jni.h>
#include "HiOmxCodecDecoder.h"
#include "HiRtpClient.h"

#include <utils/RefBase.h>

namespace android{

class HiOmxCodecHandlr :public RefBase{
public:
    HiOmxCodecHandlr(jobject jsurface,
        JNIEnv* env,
        HiRtpClient* client);
    int open();
    int close();
    int start();
    int stop();
    virtual ~HiOmxCodecHandlr();

protected:
    int MakeAVCCodecSpecData(void* outbuf, int* outsize);
    void decodeMovie(void* ptr);
    static void* startPlayVideo(void* ptr);
    void setNativeWindow(const sp<ANativeWindow> &native);

private:
    HiRtpClient* mClient;
    sp<MetaData> mformat;
    MediaBuffer *mVideoBuffer;
    ANativeWindow* mANativeWindow;
    sp<ANativeWindow> spWindow;
    HiMediaSource* mVideoSource;
    HiOmxCodecDecoder* mVideoDecoder;
    pthread_t mVidThread;
    int mRunning;
    int mOpened;
    int mWidth;
    int mHeight;
};
}
#endif
