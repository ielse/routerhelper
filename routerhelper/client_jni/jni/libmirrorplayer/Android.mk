LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_CFLAGS := -D__STDC_CONSTANT_MACROS

WITH_ANDROID_VECTOR := true
include $(LOCAL_PATH)/../utils.mk
LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/../include/android  \
    $(LOCAL_PATH)/../include/openmax \
    $(LOCAL_PATH)

LOCAL_SRC_FILES := \
    mirror_player.cpp \
    HiRtpClient.cpp \
    HiOmxCodecHandlr.cpp \
    HiOmxCodecDecoder.cpp \
    HiRtpVideoSource.cpp \
    HisiVideoSource.cpp

LOCAL_SRC_FILES += \
    Hies_proto.cpp

ifeq ($(WITH_ANDROID_VECTOR),true)
LOCAL_SRC_FILES += \
    android/atomic.c \
    android/atomic-android-arm.S \
    android/SharedBuffer.cpp \
    android/VectorImpl.cpp
endif

LOCAL_PRELINK_MODULE := false


LOCAL_LDLIBS := -llog

#LOCAL_STATIC_LIBRARIES :=
#LOCAL_LDLIBS += -L$(LOCAL_PATH)/andlib -landroid_runtime -lstagefright -lutils -lbinder -lmedia -lgui

LOCAL_MODULE := libmirrorplayer
ALL_DEFAULT_INSTALLED_MODULES += $(LOCAL_MODULE)
include $(BUILD_STATIC_LIBRARY)
