#ifndef MIRROR_PLAYER_H
#define MIRROR_PLAYER_H

#include <pthread.h>

#include <jni.h>
#include "HiOmxCodecHandlr.h"

using namespace android;

enum media_player_states {
    MEDIA_PLAYER_IDLE               = 1 << 0,
    MEDIA_PLAYER_INITIALIZED        = 1 << 1,
    MEDIA_PLAYER_PREPARED           = 1 << 2,
    MEDIA_PLAYER_RUNNING            = 1 << 3,
    MEDIA_PLAYER_ERROR             = 1 << 4,
    MEDIA_PLAYER_STOPPING            = 1 << 5,
    MEDIA_PLAYER_STOPPED            = 1 << 5
};
class MirrorPlayer
{
public:
    MirrorPlayer();
    ~MirrorPlayer();
    int setDataSource();
    int prepare();
    int start();
    int stop();
    int setVideoSurface(JNIEnv* env, jobject jsurface);

private:
    HiRtpClient* mClient;
    HiOmxCodecHandlr* mMediaHandlr;

    media_player_states         mCurrentState;
    jobject mjsurface;
    JNIEnv* mjenv;
};

#endif // FFMPEG_MEDIAPLAYER_H
