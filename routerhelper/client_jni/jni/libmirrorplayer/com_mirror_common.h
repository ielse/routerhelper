#ifndef __COM_MIRROR_COMMON_H__
#define __COM_MIRROR_COMMON_H__


#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */


#include <android/log.h>

#define  MMLOGI(TAG, ...)   __android_log_print(ANDROID_LOG_INFO, TAG, __VA_ARGS__)

#define  MMLOGD(TAG, ...)   __android_log_print(ANDROID_LOG_DEBUG, TAG, __VA_ARGS__)

#define  MMLOGE(TAG, ...)   __android_log_print(ANDROID_LOG_ERROR, TAG, __VA_ARGS__)

#define  MMLOGW(TAG, ...)   __android_log_print(ANDROID_LOG_WARN, TAG, __VA_ARGS__)

#define  MMLOGV(TAG, ...)   __android_log_print(ANDROID_LOG_VERBOSE, TAG, __VA_ARGS__)



/*----------------------------------------------*
 * 基本数据类型定义，应用层和内核代码均使用     *
 *----------------------------------------------*/

typedef unsigned char           HI_U8;
typedef unsigned char           HI_UCHAR;
typedef unsigned short          HI_U16;
typedef unsigned int            HI_U32;

typedef signed char             HI_S8;
typedef short                   HI_S16;
typedef int                     HI_S32;

#ifndef _M_IX86
typedef unsigned long long      HI_U64;
typedef long long               HI_S64;
#else
typedef __int64                   HI_U64;
typedef __int64                   HI_S64;
#endif

typedef char                    HI_CHAR;
typedef char*                   HI_PCHAR;

typedef float                   HI_FLOAT;
typedef double                  HI_DOUBLE;
typedef void                    HI_VOID;

typedef unsigned long           HI_SIZE_T;
typedef unsigned long           HI_LENGTH_T;


/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/
typedef enum {
    HI_FALSE    = 0,
    HI_TRUE     = 1,
} HI_BOOL;

#ifndef NULL
#define NULL             0L
#endif
#define HI_NULL          0L
#define HI_NULL_PTR      0L

#define HI_SUCCESS          0
#define HI_FAILURE          (-1)

#define SOCKET_BIND_ERROR           HI_FAILURE
#define SOCKET_OPTION_REUSE_ERROR      HI_FAILURE

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* __COM_MIRROR_COMMON_H__ */
