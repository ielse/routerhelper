#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

extern "C" {
#include "Hies_proto.h"

} // end of extern C
#include <android/log.h>
#include "mirror_player.h"
#include "com_mirror_common.h"
#include "mediaPlayerListener.h"

#define TAG "MirrorPlayer"


MirrorPlayer::MirrorPlayer()
{
    mClient = NULL;
    mMediaHandlr = NULL;
}


MirrorPlayer::~MirrorPlayer()
{

}
//start receive data from server
int MirrorPlayer::setDataSource()
{
    MMLOGD(TAG, "MirrorPlayer [%s]:[%d]",__FUNCTION__,__LINE__);
    int err = 0;
    /*connect */
    mClient = new HiRtpClient();
    err = mClient->connect();
    if(err != OK)
    {
        MMLOGE(TAG, "connect fail [%s]:[%d]",__FUNCTION__,__LINE__);
        mCurrentState = MEDIA_PLAYER_ERROR;
        return HI_FAILURE;
    }
    mCurrentState = MEDIA_PLAYER_INITIALIZED;
    return HI_SUCCESS;
}

/*配置音视频播放的一些参数*/
/* config some parameters of audio and video*/
int MirrorPlayer::prepare()
{
    int ret;
    int err = 0;
    MMLOGD(TAG, "MirrorPlayer [%s]:[%d]",__FUNCTION__,__LINE__);
    mMediaHandlr = new HiOmxCodecHandlr(mjsurface, mjenv, mClient);
    ret = mMediaHandlr->open();
    if(ret < 0)
    {
        MMLOGE(TAG, "openDecoder failed(%d)", ret);
        mCurrentState = MEDIA_PLAYER_ERROR;
        return INVALID_OPERATION;
    }
    mCurrentState = MEDIA_PLAYER_PREPARED;
    return HI_SUCCESS;
}

/*设定player事件监听回调对象*/
/*set listener object for player event*/
/*
int MirrorPlayer::setListener(MediaPlayerListener* listener)
{
    MMLOGI(TAG, "setListener");
    mListener = listener;
    return NO_ERROR;
}
*/
/*
int MirrorPlayer::suspend() {
    MMLOGI(TAG, "suspend");
    //if is paused, decoder thread will be blocked, so we set state = started
    if(mCurrentState == MEDIA_PLAYER_STOPPED
        || mCurrentState == MEDIA_PLAYER_STOPPING)
    {
        MMLOGI(TAG, "already player released or being released  state : %d\n", mCurrentState);
        return NO_ERROR;
    }
    mCurrentState = MEDIA_PLAYER_STOPPING;
    if(mMediaHandlr)
    {
        mMediaHandlr->stop();
        mMediaHandlr->close();
        delete mMediaHandlr;
        mMediaHandlr = NULL;
    }

    if(mClient)
    {
        mClient->disconnect();
        delete mClient;
        mClient = NULL;
    }

    MMLOGI(TAG, "suspended");
    mCurrentState = MEDIA_PLAYER_STOPPED;
    return NO_ERROR;
}
*/

/*从java层传递surface 对象到Cpp层，注册该surface, 以显示视频*/
/*deliver from java to cpp through jni, register this surface for video display*/
int MirrorPlayer::setVideoSurface(JNIEnv* env, jobject jsurface)
{
        /*注册surface 到Native Window, 并返回window实例*/
        /*register the surface to Native Window, return instance of window*/
    MMLOGD(TAG, "MirrorPlayer [%s]:[%d]",__FUNCTION__,__LINE__);
    mjsurface = jsurface;
    mjenv = env;
    return HI_SUCCESS;
}

/*启动音视频播放*/
/*start the audio and video play */
int MirrorPlayer::start()
{
    int ret = 0;
    MMLOGI(TAG, "MirrorPlayer [%s]:[%d]",__FUNCTION__,__LINE__);
    if (mCurrentState != MEDIA_PLAYER_PREPARED)
    {
        MMLOGE(TAG, "state is not prepared  [%s]:[%d]",__FUNCTION__,__LINE__);
        return HI_FAILURE;
    }
    ret = mMediaHandlr->start();
    if(ret != 0)
    {
        MMLOGI(TAG, "mMediaHandlr start failed [%s]:[%d]",__FUNCTION__,__LINE__);
        return HI_FAILURE;
    }
    mCurrentState= MEDIA_PLAYER_RUNNING;
    return HI_SUCCESS;
}
/*stop player播放*/
/*stop player*/
int MirrorPlayer::stop()
{
    MMLOGD(TAG, "MirrorPlayer [%s]:[%d]",__FUNCTION__,__LINE__);
    //if is paused, decoder thread will be blocked, so we set state = started
    if(mCurrentState == MEDIA_PLAYER_STOPPED
        || mCurrentState == MEDIA_PLAYER_STOPPING)
    {
        MMLOGI(TAG, "play state : %d  [%s]:[%d]\n", mCurrentState,__FUNCTION__,__LINE__);
        return NO_ERROR;
    }
    mCurrentState = MEDIA_PLAYER_STOPPING;
    if(mClient)
    {
        mClient->disconnect();
        delete mClient;
        mClient = NULL;
    }

    if(mMediaHandlr)
    {
        mMediaHandlr->stop();
        mMediaHandlr->close();
        delete mMediaHandlr;
        mMediaHandlr = NULL;
    }
    mCurrentState = MEDIA_PLAYER_STOPPED;
    return HI_SUCCESS;
}
