#ifndef _HI_RTSP_VIDEO_SOURCE_H_
#define _HI_RTSP_VIDEO_SOURCE_H_

#include "HiMediaSource.h"
#include "HiRtpClient.h"
#include "com_mirror_common.h"

namespace android{

class HiRtpVideoSource:public HiMediaSource{
public:
    HiRtpVideoSource(HiRtpClient* protocol);

    virtual int start();
    virtual int stop();
    virtual int read(MediaBuffer* pbuffer);
    ~HiRtpVideoSource();

private:
    HiRtpClient* mProto;
};
}
#endif
