extern "C"
{
#include "Hies_proto.h"
}

#include "HiRtpClient.h"


#define TAG "HiRtspClient"

HiRtpClient::HiRtpClient()
{
    mRunning = 0;

}

HiRtpClient::~HiRtpClient()
{

}

int HiRtpClient::connect()
{
    MMLOGD(TAG, "HiRtpClient [%s]:[%d]\n", __FUNCTION__, __LINE__);
    HI_HIES_Init_Proto();
    HI_HIES_Connect();
    return 0;
}
int HiRtpClient::disconnect()
{
    MMLOGD(TAG, "HiRtpClient [%s]:[%d]\n", __FUNCTION__, __LINE__);
    HI_HIES_DeInit_Proto();
    return 0;
}

int HiRtpClient::getSPSPPS(void* sps, int* spsSize, void* pps, int* ppsSize)
{
    int ret = 0;
    MMLOGD(TAG, "HiRtpClient [%s]:[%d]\n", __FUNCTION__, __LINE__);
    ret = HI_HIES_GetParameterFrame((HI_U8*)sps, spsSize, (HI_U8*)pps, ppsSize);
    if (ret != 0)
    {
        MMLOGE(TAG, "HI_HIES_GetParameterFrame error [%s]:[%d]\n", __FUNCTION__, __LINE__);
        return -1;
    }
    return 0;
}

int HiRtpClient::readVideoStream(void* ptr, int& dataSize, int64_t& pts, int& ptype)
{
    //MMLOGD(TAG, "HiRtpClient [%s]:[%d]\n", __FUNCTION__, __LINE__);
    int iSize = dataSize;
    int64_t  iPts = 0;
    int iPtype = 0;
    if (HI_HIES_read_video_stream(ptr, &iSize, &iPts, &iPtype) < 0)
    {
        MMLOGE(TAG, "HI_HIES_read_video_stream failed [%s]:[%d]\n", __FUNCTION__, __LINE__);
        return -1;
    }

    dataSize = iSize;
    pts = iPts;
    ptype = iPtype;
    return 0;
}
