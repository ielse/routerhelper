LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE:= libairmouse_jni

LOCAL_SRC_FILES := airmouse_jni.cpp

LOCAL_C_INCLUDES += \
    $(JNI_H_INCLUDE) \
    $(LOCAL_PATH)/../include/android \

LOCAL_PRELINK_MODULE :=false
LOCAL_LDLIBS := -llog -landroid
include $(BUILD_SHARED_LIBRARY)
