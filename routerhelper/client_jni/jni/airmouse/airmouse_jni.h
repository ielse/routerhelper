#ifndef __HI_AIR_MOUSE_JNI_H__
#define __HI_AIR_MOUSE_JNI_H__

#include <jni.h>
#include <android/log.h>

#define TAG "HiMultiScreen"

#ifndef NULL
#define NULL            0L
#endif
#define HI_SUCCESS      0
#define HI_FAILURE      (-1)

//#define  LOGV(...)   __android_log_print(ANDROID_LOG_VERBOSE, TAG, __VA_ARGS__)
#define  LOGD(...)   __android_log_print(ANDROID_LOG_DEBUG, TAG, __VA_ARGS__)
//#define  LOGI(...)   __android_log_print(ANDROID_LOG_INFO, TAG, __VA_ARGS__)
//#define  LOGW(...)   __android_log_print(ANDROID_LOG_WARN, TAG, __VA_ARGS__)
#define  LOGE(...)   __android_log_print(ANDROID_LOG_ERROR, TAG, __VA_ARGS__)

static const char* classPathName = "com/hisilicon/multiscreen/gsensor/AirMouse";

/**
 * Max value of gravity.<br>
 */
static const float GRAVITY_EARTH = 9.80665;

/**
 * Weight gyroscope of mouse X.<br>
 */
static const float WEIGHT_GYR_X = 36;// 60f;

/**
 * Weight gyroscope of mouse Y.<br>
 */
static const float WEIGHT_GYR_Y = 36;// 48f;

/**
 * Weight rotation of mouse X.<br>
 */
static const float WEIGHT_ROT_X = 0;// 0.5f;

/**
 * Weight accelerometer of mouse Y.<br>
 */
static const float WEIGHT_ROT_Y = 0;// 0.4f;

/**
 * Max distance of mouse move smooth step.<br>
 */
static const float SMOOTH_DIST_STEP = 20;

/**
 * Default minimum distance of move step.<br>
 * CN:最小位移量，用于防抖。
 */
static const float MIN_MOVE_STEP = 2;

/**
 * If mouse click down, set minimum distance of move step with the value.<br>
 */
static const float ANTI_SHAKE_MOVE_STEP = 8;

/**
 * Max distance of move step.<br>
 */
static const float MAX_MOVE_STEP = 1280;

#endif /* __HI_AIR_MOUSE_JNI_H__ */