#include <stdlib.h>
#include <stdarg.h>
#include "HiffmpegDecoder.h"
#include "com_mirror_common.h"

#define TAG "HiffmpegDecoder"

namespace android{
HiffmpegDecoder::HiffmpegDecoder()
{
    mAudCodecCtx = NULL;
    mFirstAudioFlag = 0;
    mRunning = 0;
}

HiffmpegDecoder::~HiffmpegDecoder()
{

}


int HiffmpegDecoder::openAudioDecoder()
{
    struct AVCodec* i_pcodec;
    int ret = 0;
    /*音频解码器Context*/
    /*audio decoder context*/
    mAudCodecCtx = avcodec_alloc_context();
    if(!mAudCodecCtx)
    {
        MMLOGE(TAG, "malloc error \n");
        goto err_ret;
    }
    i_pcodec = avcodec_find_decoder(CODEC_ID_AAC);

    if (avcodec_open(mAudCodecCtx, i_pcodec) < 0)
    {
        MMLOGE(TAG, "avcodec_open error \n");
       goto open_fail;
    }
    return 0;

open_fail:
    av_free(mAudCodecCtx);
err_ret:
    return -1;
}

int HiffmpegDecoder::open()
{
    if(!mRunning)
    {
        /*设置ffmpeg callback函数
            set the callback of ffmpeg to display the log of ff*/
        MMLOGD(TAG,  "enter openAudioDecoder \n");
        av_log_set_callback(ffmpegNotify);
        /* must be called before using avcodec lib */
        avcodec_init();

        /* register all the codecs */
        avcodec_register_all();
        if(openAudioDecoder() < 0)
        {
            MMLOGE(TAG,  "openAudioDecoder error \n");
            close();
            return -1;
        }
        mRunning = 1;
    }
    else
    {
        MMLOGI(TAG,  "HiffmpegDecoder already opened \n");
    }
    return 0;
}

int HiffmpegDecoder::close()
{

    MMLOGI(TAG, "HiffmpegDecoder close video ctx ok \n");
    if(mAudCodecCtx)
    {
        avcodec_close(mAudCodecCtx);
        MMLOGD(TAG, "avcodec_close  audio  ok \n");
        av_free(mAudCodecCtx);
        mAudCodecCtx = NULL;
    }
    mRunning = 0;
    mFirstAudioFlag = false;
}

/*循环获取音频数据，并送入decoder解码，获得PCM数据，回调java AudioTrack 播放*/
/*get the audio data , the input to decoder and get pcm output, then input pcm to audio track*/
int HiffmpegDecoder::decodeAudio(void *outdata, int *outdata_size, AVPacket *avpkt)
{
    int ret = 0;
    //MMLOGD(TAG, "enter decodeAudio \n");
    ret = avcodec_decode_audio3(mAudCodecCtx, (int16_t*)outdata, outdata_size, avpkt);
    if(ret < 0)
    {
        MMLOGE(TAG, "decodeAudio failed  \n");
        return -1;
    }
    if(!mFirstAudioFlag)
        mFirstAudioFlag = true;
    return ret;
}


int HiffmpegDecoder::getAudioAttr(int* chnlCnt, int* sampleRate, int* sample_fmt)
{
    if(mFirstAudioFlag)
    {
        *chnlCnt = mAudCodecCtx->channels;
        *sampleRate = mAudCodecCtx->sample_rate;
        *sample_fmt = mAudCodecCtx->sample_fmt;
        return 0;
    }
    else
    {
        MMLOGI(TAG, "there no audio frame are decoded\n");
        return -1;
    }
}

void HiffmpegDecoder::ffmpegNotify(void* ptr, int level, const char* fmt, va_list vl)
{
    char tmpBuffer[1024];
//    __android_log_print(ANDROID_LOG_ERROR, TAG, "AV_LOG_ERROR: %s", tmpBuffer);
    vsnprintf(tmpBuffer,1024,fmt,vl);

    switch(level) {
            /**
             * Something went really wrong and we will crash now.
             */
        case AV_LOG_PANIC:
            MMLOGE(TAG, "AV_LOG_PANIC: %s", tmpBuffer);
            //sPlayer->mCurrentState = MEDIA_PLAYER_STATE_ERROR;
            break;

            /**
             * Something went wrong and recovery is not possible.
             * For example, no header was found for a format which depends
             * on headers or an illegal combination of parameters is used.
             */
        case AV_LOG_FATAL:
            MMLOGE(TAG, "AV_LOG_FATAL: %s", tmpBuffer);
            //sPlayer->mCurrentState = MEDIA_PLAYER_STATE_ERROR;
            break;

            /**
             * Something went wrong and cannot losslessly be recovered.
             * However, not all future data is affected.
             */
        case AV_LOG_ERROR:
            MMLOGE(TAG, "AV_LOG_ERROR: %s", tmpBuffer);
            //sPlayer->mCurrentState = MEDIA_PLAYER_STATE_ERROR;
            break;

            /**
             * Something somehow does not look correct. This may or may not
             * lead to problems. An example would be the use of '-vstrict -2'.
             */
        case AV_LOG_WARNING:
            MMLOGI("AV_LOG_WARNING: %s", tmpBuffer);
            break;

        case AV_LOG_INFO:
            MMLOGI(TAG, "%s", tmpBuffer);
            break;

        case AV_LOG_DEBUG:
            MMLOGI(TAG, "%s", tmpBuffer);
            break;

    }
}
}
