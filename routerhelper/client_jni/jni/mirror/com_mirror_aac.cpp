#include <dlfcn.h>
#include <stdlib.h>
#include <math.h>

#include <stdio.h>
#include <sys/param.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/select.h>
#include <unistd.h>

#include "com_mirror_aac.h"
#include "com_mirror_rtp.h"
#include "libavutil/samplefmt.h"
#include "HiffmpegDecoder.h"

extern "C"
{
#include "libavcodec/avcodec.h"
}
using namespace android;
#define AAC_RTP_LOST_INTERVAL 200

static AACOperation g_aac_Opera;     //aac handle

static int mAudFreeCount = MAX_AUDIO_BUF_NUM;
/*缓存的音频帧，最大个数为MAX_AUDIO_BUF_NUM*/
/*cached audio frame , max MAX_AUDIO_BUF_NUM*/
static AudFrameInfo mAudframeBuf[MAX_AUDIO_BUF_NUM] = {0};
static pthread_cond_t m_Audcondition;
static pthread_mutex_t m_Audlock;
static int condWaitCount = 0 ;
int getAudioFrame(void* ptr, int* dataSize, unsigned int* pts);
void putAudioFrame(void* pbuf, int dataLen , unsigned int pts, int payload);
void resetAudioBuff();
int findMinPtsIndexInBuffer();

//get current ms time
static double getMsTime()
{
    struct timeval pTime;
    gettimeofday(&pTime, NULL);
    return (pTime.tv_sec * 1000 + (pTime.tv_usec / 1000.0));
}


//config audio track
void configAudioRender(int chnlCnt, int sampleRate, int sample_fmt)
{
    int AudioLatency = 0;
    /*配置AudioTrack 参数*/
    /*config the paras of AudioTrack*/
    AudioLatency = g_aac_Opera.writer->configAudioTrack(STREAM_MUSIC,
                   sampleRate,
                   ((chnlCnt == 2) ? CHANNEL_CONFIGURATION_STEREO : CHANNEL_CONFIGURATION_MONO),
                   ((sample_fmt == AV_SAMPLE_FMT_S16) ? (ENCODING_PCM_16BIT) : (ENCODING_PCM_8BIT)),
                   MODE_STREAM);
}

//demux aac packet
int demuxAACPacket(char* buffer,  int* type,  unsigned int* pts, unsigned int* size)
{
    int i_ptype = 0;
    unsigned int i_pts = 0;
    unsigned int  i_dataLen = 0;
    char* pdata = buffer;

    memcpy(&i_ptype, pdata, sizeof(int));
    pdata += sizeof(int);
    memcpy(&i_pts, pdata,  sizeof(unsigned int));
    pdata += sizeof(unsigned int);
    memcpy(&i_dataLen, pdata, sizeof(unsigned int));
    pdata += sizeof(unsigned int);

    //MMLOGD(TAG,"type=%d, pts=%u, size=%d", i_ptype,i_pts, i_dataLen);
    *type = i_ptype;
    *pts = i_pts;
    *size = i_dataLen;
}

//RTP receive aac thread func
static void* native_receive_aac_process(void* args)
{
    MMLOGD(TAG, "native_receive_aac_process");
    //RTP parse packet
    int bodyLen = 0; // msg body length

    RTPHeader* rtpHeadMsg = NULL; //RTP head
    unsigned short seq ; //current seq
    int mark; //RTP end flag

    //init socket
    struct sockaddr_in servaddr;
    int sockfd = -1, dataLen;
    //socklen_t servaddr_len;
    HI_S32 keepalive = 1;
    HI_S32 s32Ret = HI_FAILURE;

    //init params for count frame
    HI_U16 receive_frame = 0;
    HI_U16 start_count_seq = 0;
    HI_U16 recv_rtp_num = 0;

    unsigned int pts = 0;
    unsigned int size = 0;
    int type = 0;

    fd_set read_fds;

    struct timeval TimeoutVal;

    char* recvs = (char*)malloc(AAC_RECV_BUFF_LENGTH);

    if (NULL == recvs)
    {
        MMLOGE(TAG, "receive buffer is NULL.");
        return NULL;
    }

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
    {
        MMLOGE(TAG, "udp socket error\n");
        free(recvs);
        return NULL;
    }

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);

    servaddr.sin_port = htons(CLIENT_TRANSMIT_PORT + 2); //audio port is video port +2

    if (SOCKET_BIND_ERROR == bind(sockfd, (struct sockaddr*)&servaddr, sizeof(servaddr)))
    {
        MMLOGE(TAG, "udp bind error.");
        close(sockfd);
        free(recvs);
        return NULL;
    }
    else
    {
        s32Ret = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &keepalive, sizeof(keepalive));
        if (SOCKET_OPTION_REUSE_ERROR == s32Ret)
        {
            close(sockfd);
            free(recvs);
            MMLOGE(TAG, "set socket option reuse error.");
            return NULL;
        }
    }
    //sound status is run,receive rtp packet
    while (SOUND_STATUS_RUN == g_aac_Opera.runStatus)
    {
        FD_ZERO(&read_fds);
        FD_SET(sockfd, &read_fds);

        //set socket timeout 1s
        TimeoutVal.tv_sec  = 1;
        TimeoutVal.tv_usec = 0;
        s32Ret = select(sockfd + 1, &read_fds, 0, 0, &TimeoutVal);
        if ( s32Ret <= 0)
        {
            //not receive data
            continue;
        }
        else
        {
            //LOGE("FD_ISSET");
            /*lint -e573  -e737 -e778*/
            if (false == FD_ISSET(sockfd, &read_fds))
            {
                MMLOGE(TAG, "FD_ISSET error");
                close(sockfd);
                free(recvs);
                return NULL;
            }
            /*lint +e573  +e737 +e778*/

            memset(recvs, 0, sizeof(recvs));
            dataLen = recvfrom(sockfd, (char*)(recvs), AAC_RECV_BUFF_LENGTH, 0, NULL, 0);
            if (dataLen < RTP_HEADER_SIZE)
            {
                //recv packet length small than rtp header
                MMLOGE(TAG, "dataLen < RTP_HEADER_SIZE");
                continue ;
            }

            // 1- read and parse rtp head
            rtpHeadMsg = (RTPHeader*)recvs;
            mark = rtpHeadMsg->marker;
            bodyLen = dataLen - RTP_HEADER_SIZE;
            seq = rtpHeadMsg->sequencenumber;
            //MMLOGD(TAG,"seq=%d, mark=%d, bodyLen= %d", seq, mark, bodyLen);
            recv_rtp_num++;
            //count lost aac rtp number
            if ((HI_U16)(seq - start_count_seq) >= AAC_RTP_LOST_INTERVAL)
            {
                HI_U16 cur_lost_num = (HI_U16)(AAC_RTP_LOST_INTERVAL - recv_rtp_num) % AAC_RTP_LOST_INTERVAL;
                MMLOGD(TAG, "lost aac rtp num %d / in %d \n ", cur_lost_num, AAC_RTP_LOST_INTERVAL);
                start_count_seq = seq;
                recv_rtp_num = 0;
            }

            if ((mark == 1) && (bodyLen > AUDIO_FRAME_HEAD_SIZE))
            {
                receive_frame++;
                //do demux
                char* bufAddr = (char*)(recvs + RTP_HEADER_SIZE);
                demuxAACPacket(bufAddr, &type, &pts, &size);

                if (type == AUDIO_PACKET_TYPE)
                {
                    char* dataAddr = bufAddr + AUDIO_FRAME_HEAD_SIZE;
                    putAudioFrame(dataAddr, size, pts, type);
                }
            }
        }
    }

    //close socket
    close(sockfd);

    if (NULL != recvs)
    {
        free(recvs);
        recvs = NULL;
    }
    pthread_exit((void*)0);

    return HI_SUCCESS;
}

//aac decode thread func
static void* native_decode_aac_process(void* args)
{
    unsigned int pts = 0;
    int size = 0;
    AVPacket pkt;
    int i_outSize = 0;
    int audConfigFlag = 0;  //flag for if config audioTrack param
    int mChnCnt, mSampleRate, mSampleFmt = 0;
    int ret = HI_FAILURE;

    double beginTime, endTime;

    MMLOGD(TAG, "native_decode_aac_process");
    HiffmpegDecoder* ffmpegDecoder = new HiffmpegDecoder();
    ret = ffmpegDecoder->open();
    if (ret < 0)
    {
        MMLOGE(TAG, "HiffmpegDecoder open error \n");
        return NULL;
    }
    void* outbuf = av_malloc(AVCODEC_MAX_AUDIO_FRAME_SIZE);
    if (!outbuf)
    {
        MMLOGE(TAG, "HI_HIES_av_malloc failed");
        //mListener->notify(MEDIA_ERROR, MEDIA_ERROR_SERVER_DIED, -1);
        ffmpegDecoder->close();
        return NULL;
    }

    void* pInBuffer = av_malloc(MAX_AUDIO_FRAME_SIZE);
    if (!pInBuffer)
    {
        MMLOGE(TAG, "malloc error \n");
        //mListener->notify(MEDIA_ERROR, MEDIA_ERROR_SERVER_DIED, -1);
        av_free(outbuf);
        ffmpegDecoder->close();
        return NULL;
    }
    while (SOUND_STATUS_RUN == g_aac_Opera.runStatus)
    {
        if (HI_SUCCESS == getAudioFrame(pInBuffer, &size, &pts))
        {
            //Step 1, get the AAC Data and decode
            av_init_packet(&pkt);
            pkt.size = MAX_AUDIO_FRAME_SIZE;
            pkt.pts = pts;
            pkt.size = size;
            pkt.data = (uint8_t*)(pInBuffer);
            pkt.dts = pkt.pts;

            i_outSize = AVCODEC_MAX_AUDIO_FRAME_SIZE;


            if (AUDIO_PLAY == g_aac_Opera.playStatus)
            {
                beginTime = getMsTime();
                ret = ffmpegDecoder->decodeAudio(outbuf, &i_outSize, &pkt);
                endTime = getMsTime();
                //MMLOGD(TAG, "decodeAudio cost time %lf\n",endTime-beginTime);
            }

            if (ret < 0)
            {
                MMLOGE(TAG, "HI_HIES_decodeAudio failed");
                continue;
            }

            //Step 2, write PCM into AudioTrack
            if (!audConfigFlag)
            {
                int ret = 0;
                ret = ffmpegDecoder->getAudioAttr(&mChnCnt, &mSampleRate, &mSampleFmt);
                if (ret < 0)
                {
                    MMLOGE(TAG, "there some error happened, could not get the attr \n");
                    //errHappened = 1;
                    break;
                }
                MMLOGI(TAG, "Audio Attr chnl: %d sampleRate: %d sampleFmt: %d\n", mChnCnt, mSampleRate, mSampleFmt);
                configAudioRender(mChnCnt, mSampleRate, mSampleFmt);
                audConfigFlag = 1;
            }

            if (AUDIO_PLAY == g_aac_Opera.playStatus)
            {
                beginTime = getMsTime();
                g_aac_Opera.writer->writePCM((unsigned char*)outbuf, i_outSize);
                endTime = getMsTime();
                //MMLOGD(TAG, "writePCM cost time %lf\n",endTime-beginTime);
            }

        }
        else
        {
            usleep(5000);
        }
    }

    if (outbuf)
    {
        av_free(outbuf);
    }
    if (pInBuffer)
    {
        av_free(pInBuffer);
    }
    g_aac_Opera.writer->detach();
    MMLOGD(TAG, "start Notify Detaching decodeAudio");
    ffmpegDecoder->close();
    delete ffmpegDecoder;
    ffmpegDecoder = NULL;
    pthread_exit((void*)0);
    return NULL;
}

//start audio play
int HI_NativeAAC_SetEnable(JNIEnv* env, MediaPlayerPCMWriter* writer)
{
    //accessip = env->GetStringUTFChars(ip,0);
    MMLOGD(TAG, "enter HI_NativeAAC_SetEnable.");
    if (SOUND_STATUS_INIT == g_aac_Opera.runStatus)
    {
        MMLOGD(TAG, "AAC_SetEnable enter malloc.");
        g_aac_Opera.runStatus = SOUND_STATUS_RUN;
        g_aac_Opera.playStatus = AUDIO_PLAY;
        g_aac_Opera.writer = writer;

        pthread_cond_init(&m_Audcondition, NULL);
        pthread_mutex_init(&m_Audlock, NULL);
        condWaitCount = 0;

        resetAudioBuff();

        pthread_create(&g_aac_Opera.pidReceive, NULL, native_receive_aac_process, NULL);
        pthread_create(&g_aac_Opera.pidDecode, NULL, native_decode_aac_process, NULL);

    }
    else if (SOUND_STATUS_RUN == g_aac_Opera.runStatus)
    {
        MMLOGD(TAG, "sound status is already run");
    }
    else
    {
        MMLOGD(TAG, "sound status is stopping");
        return HI_FAILURE;
    }
    return HI_SUCCESS;
}

//Native AAC DeInit func
int HI_NativeAAC_DeInit(void* pHandle)
{
    MMLOGD(TAG, "HI_NativeAAC_DeInit");
    if (g_aac_Opera.runStatus == SOUND_STATUS_RUN)
    {
        g_aac_Opera.runStatus = SOUND_STATUS_STOP;

        pthread_join(g_aac_Opera.pidReceive, NULL);

        for (int i = 0; i < 5; i++)
        {
            MMLOGD(TAG, "condition wait count %d", condWaitCount);
            pthread_cond_signal(&m_Audcondition);
            usleep(10000);
        }

        pthread_join(g_aac_Opera.pidDecode, NULL);
        resetAudioBuff();
        pthread_cond_destroy(&m_Audcondition);
        pthread_mutex_destroy(&m_Audlock);

        g_aac_Opera.runStatus = SOUND_STATUS_INIT;
        return HI_SUCCESS;
    }
    else
    {
        MMLOGD(TAG, "runstatus is not run.");
    }
}

//config aac play or pause
void HI_NativeAAC_Config(int flag)
{
    switch (flag)
    {
        case AAC_PLAY_FLAG:
        {
            MMLOGD(TAG, "config audio play\n");
            g_aac_Opera.playStatus = AUDIO_PLAY;
            break;
        }

        case AAC_PAUSE_FLAG:
        {
            MMLOGD(TAG, "config audio pause\n");
            g_aac_Opera.playStatus = AUDIO_PAUSE;
            break;
        }

        default:
        {
            MMLOGD(TAG, "config  wrong command: %d\n", flag);
            break;
        }

    }

}

//put receive audio frame into buffer.
void putAudioFrame(void* pbuf, int dataLen , unsigned int pts, int payload)
{
    //MMLOGD(TAG, "[%s] [%d] mAudFreeCount %d \n", __FUNCTION__, __LINE__, mAudFreeCount);
    if (mAudFreeCount == 0)
    {
        pthread_cond_signal(&m_Audcondition);
        //flush buffer
        resetAudioBuff();
        return;
    }
    int i = 0;
    for (i = 0; i < MAX_AUDIO_BUF_NUM; i++)
    {
        if (!mAudframeBuf[i].busy)
        {
            break;
        }
    }
    mAudframeBuf[i].datalen = dataLen;
    mAudframeBuf[i].pts = pts;
    mAudframeBuf[i].payload = payload;
    memcpy(mAudframeBuf[i].data, pbuf,  dataLen);

    pthread_mutex_lock(&m_Audlock);
    mAudframeBuf[i].busy = true;
    if (mAudFreeCount > 0)
    {
        mAudFreeCount -= 1;
    }
    //MMLOGI(TAG," put audio  frame  len : %d pts: %d  index : %d free: %d\n", dataLen,pts,  i, mAudFreeCount);
    if (mAudFreeCount == MAX_AUDIO_BUF_NUM - 1)
    {
        //MMLOGI(TAG,"audio send out signal!!!\n");
        pthread_cond_signal(&m_Audcondition);
    }
    pthread_mutex_unlock(&m_Audlock);
}

//get audio frame from buffer.
int getAudioFrame(void* ptr, int* dataSize, unsigned int* pts)
{
    //MMLOGD(TAG, "[%s] [%d] mAudFreeCount %d \n", __FUNCTION__, __LINE__, mAudFreeCount);
    pthread_mutex_lock(&m_Audlock);
    if (mAudFreeCount == MAX_AUDIO_BUF_NUM)
    {

        condWaitCount++;
        pthread_cond_wait(&m_Audcondition, &m_Audlock);
        condWaitCount--;
    }
    //find the min pts's frame
    int iMinIdx = 0;

    iMinIdx = findMinPtsIndexInBuffer();

    if (mAudframeBuf[iMinIdx].datalen == 0)
    {
        mAudframeBuf[iMinIdx].busy = false;
        MMLOGI(TAG, "audio buffer len  is 0 \n");
        pthread_mutex_unlock(&m_Audlock);
        return HI_FAILURE;
    }

    AudFrameInfo* pFrame = &mAudframeBuf[iMinIdx];
    *dataSize = pFrame->datalen;
    *pts = pFrame->pts;
    memcpy(ptr, pFrame->data, pFrame->datalen);
    mAudFreeCount += 1;
    pFrame->busy = false;
    pthread_mutex_unlock(&m_Audlock);
    return HI_SUCCESS;

}
//reset audio buff
void resetAudioBuff()
{
    pthread_mutex_lock(&m_Audlock);
    mAudFreeCount = MAX_AUDIO_BUF_NUM;
    memset((void*)&mAudframeBuf[0], 0x00, MAX_AUDIO_BUF_NUM * sizeof(AudFrameInfo));
    pthread_mutex_unlock(&m_Audlock);
}

//find buffer index which is earliest
int findMinPtsIndexInBuffer()
{
    unsigned int iMinPts = 0;
    int iMinIdx = 0;
    int bufIdx = 0;
    for (bufIdx = 0; bufIdx < MAX_AUDIO_BUF_NUM; bufIdx++)
    {
        if (mAudframeBuf[bufIdx].busy && ((iMinPts == 0) || (mAudframeBuf[bufIdx].pts < iMinPts)))
        {
            iMinIdx = bufIdx;
            iMinPts = mAudframeBuf[bufIdx].pts;
        }
    }
    return iMinIdx;
}
