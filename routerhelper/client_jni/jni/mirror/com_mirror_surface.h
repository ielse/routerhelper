#ifndef __COM_MIRROR_SURFACE_H__
#define __COM_MIRROR_SURFACE_H__

#include <SkStream.h>
#include <SkImageDecoder.h>
#include <SkBitmap.h>
#include <jni.h>
#include <surfaceflinger/Surface.h>
#include <surfaceflinger/ISurface.h>
#include <pthread.h>
#include "com_mirror_common.h"

//自定义JPEG头长度 48
#define HI_MIRROR_JPEG_HEAD_LENGTH         sizeof(HI_Mirror_JPEG_Head)
//图像数据缓存长度 <= 1280*720*2
#define HI_MIRROR_PRE_BUFFER_LENGTH        (1280 * 1080 * 16 / 8)
//接收的JPEG图片缓存长度 < 200KB
#define HI_MIRROR_JPEG_BUFFER_LENGTH    (1024 * 512)
//基础图片的宽度
#define HI_MIRROR_JPEG_MAX_WIDTH         1280
//基础图片的高度
#define HI_MIRROR_JPEG_MAX_HEIGHT         720
//基础图片的位深 字节数/像素
#define BYTES_PER_PIXEL                    2

//CN:安卓版本2.2及更低版本使用的库。
#define LIB_SURFACE_FLINGER_CLIENT_FOR_ANDROID_2_2    "libsurfaceflinger_client.so"
#define LIB_UI_FOR_ANDROID_2_2             "libui.so"
//CN:安卓版本2.3。
#define ANDROID_VERSION_2_3                9
//CN:安卓2.3及更高版本使用的库。
#define LIB_ANDROID_FOR_ANDROID_2_3        "libandroid.so"

#define TAG "HiMultiScreen_Mirror"

typedef void* PF_ANWindow_register(void* pHandle,JNIEnv* env, jobject jsurface);

typedef struct stHI_Mirror_JPEG_Head
{
    HI_S32 Width;
    HI_S32 Height;
    HI_S32 x;
    HI_S32 y;
    HI_S32 NTPtime;
    HI_S32 MetaLen;    //原始JPEG长度
    //保留字节
    HI_S32 Jpegprofileset[2];
    HI_S32 Reverse[4];
}HI_Mirror_JPEG_Head;

#if 0
typedef int PF_ANWindow_update(void* pPixels,int length);
#else
typedef int PF_ANWindow_update(void* pPixels); //add by zk
#endif

typedef int PF_ANWindow_unregister(void* pHandle);
// _ZN7android7Surface4lockEPNS0_11SurfaceInfoEb
#if 0
typedef int PF_ANWindow_lock(void *, void *, int);
#else
typedef int PF_ANWindow_lock(void *, void *, ARect*); //add by zk
typedef int PF_ANWindow_getwidth(ANativeWindow* window);
typedef int PF_ANWindow_getheight(ANativeWindow* window);
#endif
// _ZN7android7Surface13unlockAndPostEv
typedef int PF_ANWindow_unlockAndPost(void *);
typedef int PF_ANWindow_release(void *);
typedef int PF_ANWindow_setBuffersGeometry(void* pHandle, int width, int height, int format);
typedef void* PF_ANWindow_get(JNIEnv* env, jobject surface);

typedef enum eDisplayStatus
{
    DISPLAY_STATUS_INIT,
    DISPLAY_STATUS_RUN,
    DISPLAY_STATUS_STOP,
}DisplayStatus;

typedef struct stSurfaceOperation
{
    void* pSurfaceNative;
    void* pPixelCache;
    int mPixelFormat;
    int mBytesPerPixels;
    int mAndroidAPIVersion;//<9的是2.3以下的操作系统，>9的是2.3及以后的操作系统
    void* psurface;
    DisplayStatus status;
    pthread_t threadID;
    //begin add by zk
    pthread_t pidSurfaceUpdate; //surface定时刷新线程
    PF_ANWindow_getwidth* pf_getwidth; //surface动态变化保护只支持2.3以上系统
    PF_ANWindow_getheight* pf_getheight;
    //end add
    //add by ywh
    pthread_t pidDecode;        //解码线程
    int width;
    int height;
    PF_ANWindow_register* pf_register;
    PF_ANWindow_update* pf_update;
    PF_ANWindow_unregister* pf_unregister;
    PF_ANWindow_lock* pf_lock;
    PF_ANWindow_unlockAndPost* pf_unlockAndPost;
    PF_ANWindow_release* pf_release;
    PF_ANWindow_get* pf_get;
    PF_ANWindow_setBuffersGeometry* pf_setBuffersGeometry;
    void* pLibHandle;
}SurfaceOperation;

typedef struct stSurface_Buffer {
    // The number of pixels that are show horizontally.
    int32_t width;

    // The number of pixels that are shown vertically.
    int32_t height;

    // The number of *pixels* that a line in the buffer takes in
    // memory.  This may be >= width.
    int32_t stride;

    // The format of the buffer.  One of WINDOW_FORMAT_*
    int32_t format;

    // The actual bits.
    void* bits;

    // Do not touch.
    uint32_t reserved[6];
} Surface_Buffer;

int JpegDecodeSkia(char* pBmpAddr,int length,char* pJpegAddr);

int HI_NativeSurface_SetEnable(JNIEnv * env, jobject jsurface, int apiVersion,jstring string);

int HI_NativeSurface_DeInit(void* pHandle);

int HI_NativeSurface_GetDecodeFPS();

#endif
