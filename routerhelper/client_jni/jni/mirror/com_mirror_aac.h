#ifndef __COM_MIRROR_AAC_H__
#define __COM_MIRROR_AAC_H__

#include <jni.h>
#include <pthread.h>
#include "com_mirror_common.h"

#include "mediaPlayerListener.h"

#define HI_MIRROR_AAC_BUFFER_LENGTH (1024*2)
#define MAX_AUDIO_FRAME_SIZE (10*1024)

#define STREAM_MUSIC 3
#define CHANNEL_CONFIGURATION_MONO 2
#define CHANNEL_CONFIGURATION_STEREO 3
#define ENCODING_PCM_8BIT 3
#define ENCODING_PCM_16BIT 2
#define MODE_STREAM 1
#define AAC_RECV_BUFF_LENGTH 1500
#define AAC_PLAY_FLAG 1
#define AAC_PAUSE_FLAG 0
#define AUDIO_PACKET_TYPE 10
#define MAX_AUDIO_BUF_NUM 5
#define AUDIO_FRAME_HEAD_SIZE (sizeof(int) + sizeof(unsigned int) + sizeof(unsigned int))

#define TAG "HiMultiScreen_Mirror_AAC"

//sound run Status
typedef enum eSoundStatus
{
    SOUND_STATUS_INIT,
    SOUND_STATUS_RUN,
    SOUND_STATUS_STOP,
}SoundStatus;

//audio play status
typedef enum eAudioPlayStatus
{
    AUDIO_PLAY,
    AUDIO_PAUSE,
}AudioPlayStatus;

//aac handle for recv,decode and play
typedef struct stAACOperation
{
    SoundStatus runStatus;
    AudioPlayStatus playStatus;
    MediaPlayerPCMWriter* writer;
    pthread_t pidReceive;
    pthread_t pidDecode;
}AACOperation;

/*��Ƶ֡���ݽṹ*/
/*audio frame struct*/
typedef struct stAudFrameInfo
{
    unsigned int pts;
    int datalen;
    int payload;
    unsigned char data[MAX_AUDIO_FRAME_SIZE];
    bool busy;
}AudFrameInfo;


int HI_NativeAAC_SetEnable(JNIEnv * env, MediaPlayerPCMWriter *writer);

int HI_NativeAAC_DeInit(void* pHandle);

void HI_NativeAAC_Config(int playFlag);

//void HI_AAC_setPCMWriter(MediaPlayerPCMWriter *writer);


#endif
