#ifndef __COM_MIRROR_RTP_H__
#define __COM_MIRROR_RTP_H__

//CN:打印LOG控制位
#define SHOW_LOG                     1
//CN:接收传屏的端口号
#define CLIENT_TRANSMIT_PORT          8888
//CN:RTP头的大小
#define RTP_HEADER_SIZE             sizeof(Rtphead)
//CN:每次发送的有效数据
#define BYTES_PER_SEND                (10240)  //10K
//CN:发送包的大小 10KB+12KB
#define RTP_MSG_LEN                 (RTP_HEADER_SIZE + BYTES_PER_SEND)

typedef struct RTPHeader
{
    //计数CSRC 标识符的数量
    HI_U8 csrccount:4;
    //扩展，置1表示PTR 报头后面有扩展报头
    HI_U8 extension:1;
    //填充，置1表示用户数据后面有填充位
    HI_U8 padding:1;
    //版本号
    HI_U8 version:2;
    //载荷类别
    HI_U8 payloadtype:7;
    //标记，1表示一帧的结束
    HI_U8 marker:1;

    //序号
    HI_U16     sequencenumber;
    //时间戳
    HI_U32     timestamp;
    //同步源标识符
    HI_U32     ssrc;
}Rtphead;




#endif