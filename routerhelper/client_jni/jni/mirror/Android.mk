LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_CFLAGS := -D__STDC_CONSTANT_MACROS
#include $(LOCAL_PATH)/../utils.mk

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/../include/android \
    $(LOCAL_PATH)/../include/android2.2 \
    $(LOCAL_PATH)/../include/android2.2/skia \
    $(LOCAL_PATH)/../include/android2.2/skia/core \
    $(LOCAL_PATH)/../include/android2.2/skia/images \
    $(LOCAL_PATH)/../include/android2.2/skia/graphics \
    $(LOCAL_PATH)/../libffmpeg \
    $(LOCAL_PATH)/../include/openmax \
    $(LOCAL_PATH)/../libmirrorplayer

LOCAL_SRC_FILES := \
    com_mirror_surface_jni.cpp \
    com_mirror_surface_native.cpp \
    com_mirror_rtcp.cpp \
    com_mirror_aac.cpp \
    com_mirror_h264.cpp \
    HiffmpegDecoder.cpp


LOCAL_PRELINK_MODULE := false

LOCAL_LDLIBS := -llog -lskia -landroid
LOCAL_LDLIBS += -L$(LOCAL_PATH)/../lib
LOCAL_LDLIBS += -L$(LOCAL_PATH)/../libmirrorplayer/andlib -landroid_runtime -lstagefright -lutils -lbinder -lmedia -lgui

LOCAL_STATIC_LIBRARIES := libavformat libHWAHStreaming libavcodec libavutil libpostproc libswscale libavfilter libsf libmirrorplayer

LOCAL_SHARED_LIBRARIES = libskia

LOCAL_MODULE := libmirror_jni

include $(BUILD_SHARED_LIBRARY)
