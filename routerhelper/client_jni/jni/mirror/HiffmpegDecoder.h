#ifndef _HI_FFMPEG_DECODER_H_
#define _HI_FFMPEG_DECODER_H_
#include "mediaPlayerListener.h"

extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavutil/log.h"
}

namespace android{

class HiffmpegDecoder{
public:
    HiffmpegDecoder();
    int open();
    int close();
    int decodeAudio(void *outdata, int *outdata_size, AVPacket *avpkt);
    int getAudioAttr(int* chnlCnt, int* sampleRate, int* sample_fmt);

    virtual ~HiffmpegDecoder();
protected:
    int openAudioDecoder();
    static void ffmpegNotify(void* ptr, int level, const char* fmt, va_list vl);

private:
    AVCodecContext*  mAudCodecCtx;

    bool mFirstAudioFlag;
    bool mRunning;
};
}
#endif
