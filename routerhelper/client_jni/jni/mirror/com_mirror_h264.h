#ifndef __COM_MIRROR_H264_H__
#define __COM_MIRROR_H264_H__
#include <jni.h>
#include "mirror_player.h"
#include "com_mirror_common.h"

int HI_NativeH264Surface_SetEnable(JNIEnv * env,jobject jsurface, MirrorPlayer *mp);
int HI_NativeH264Surface_DeInit(JNIEnv * env, MirrorPlayer *mp);
#endif
