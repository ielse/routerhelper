#include "com_mirror_h264.h"
#define TAG "NativeMirrorh264"
HI_S32 HI_NativeH264Surface_SetEnable(JNIEnv* env, jobject jsurface, MirrorPlayer* mp)
{
    HI_S32 s32Ret;
    if (mp == NULL )
    {
        return HI_FAILURE;
    }
    s32Ret = mp->setVideoSurface(env, jsurface);
    if (s32Ret == HI_FAILURE)
    {
        MMLOGE(TAG, "setVideoSurface  failure");
        return HI_FAILURE;
    }

    s32Ret = mp->setDataSource();
    if (s32Ret == HI_FAILURE)
    {
        MMLOGE(TAG, "setDataSource failure");
        return HI_FAILURE;
    }

    s32Ret = mp ->prepare();
    if (s32Ret == HI_FAILURE)
    {
        MMLOGE(TAG, "prepare  failure");
        return HI_FAILURE;
    }

    s32Ret = mp ->start();
    if (s32Ret == HI_FAILURE)
    {
        MMLOGE(TAG, "start failure");
        return HI_FAILURE;
    }
    return s32Ret;
}

HI_S32 HI_NativeH264Surface_DeInit(JNIEnv* env, MirrorPlayer* mp)
{
    HI_S32 s32Ret;
    if (mp == NULL )
    {
        return HI_FAILURE;
    }

    s32Ret = mp->stop();

    if (s32Ret == HI_FAILURE)
    {
        MMLOGE(TAG, "stop  failure");
    }
}
